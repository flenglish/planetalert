<?php namespace ProcessWire;
    include("./head.inc");

    $out = '';

    $link = '<a href="https://mangatar.framiq.com/en/build/" class="btn btn-primary" target="_blank">'.__('Aller sur Mangatar pour créer mon avatar').'</a>';
    $link2 = '<a href="https://framiq.com/" target="_blank">Framiq.com</a>';

    $out .= '<div class="row">';
        $out .= '<h2 class="text-center">'.__("Create your avatar !").'</h2>';

        $out .= '<p><strong>Lis bien les conseils suivants avant de créer ton avatar !</strong></p>';

        $out .= '<ul>';
        $out .= '<li>Choisis les éléments de ton personnage avec précaution car tu ne pourras plus changer sans acheter une potion de transformation.</li>';
        $out .= '<li>Les menus sur le site permettent d\'accéder à de nombreux styles d\'avatars différents. Tes choix devront être validés par ton enseignant·e avant d\'être mis en ligne sur ton profil.</li>';
        $out .= '<li>Quand tu as terminé, <strong>clique sur \'Save & Share\' pour enregistrer</strong> l\'image sur ton disque dur ou sur une clé USB.</li>';
        $out .= '<li>Indique <strong>ton nom, ton prénom et ta classe dans le nom du fichier !</strong></li>';
        if ($headTeacher->name == 'flieutaud') {
          $teacherPage = $pages->get("parent.name=teachers, template=teacherProfile, name=$headTeacher->name");
          $out .= '<li><strong>Dépose ton fichier dans le porte-documents </strong>';
          $out .= '<a role="button" class="btn btn-default" href="'.$teacherPage->filesUploadUrl.'" target="_blank" data-toggle="tooltip" title="'.__("Upload a document for your teacher").'"><span class="glyphicon glyphicon-cloud-upload"></span></a>';
          $out .= ' ou bien envoie ton fichier par mail (ou rapporte le sur en classe sur une clé USB).</li>';
        $out .= '<li>Tu peux également copier et coller l\'URL (l\'adresse du site Internet <strong>après avoir cliqué sur \'Save & Share\'</strong>) de ton avatar dans ton pad.</li>';
        } else {
          $out .= '<li><strong>Envoie ton fichier par mail</strong> ou rapporte le en classe sur une clé USB.</li>';
            $out .= '<li>Tu peux également copier et coller l\'URL (l\'adresse du site Internet <strong>après avoir cliqué sur \'Save & Share\'</strong>) de ton avatar dans ton pad.</li>';
        }
        $out .= '</ul>';

        $out .= '<p class="text-center">'.$link.'</p>';

        $out .= '<div class="text-center">';
            $out .= '<p>↓ '.__("A few examples").' ↓</p>';
            $out .= '<img class="img-rounded img-tumbnail" src="'.$page->image->url.'" />';
            $out .= '<h4>'.sprintf(__('Thanks to %s for the avatar generation tools (click on the link for more examples) !'), $link2).'</h4>';
        $out .= '</div>';
    $out.= '</div>';

    echo $out;

    include("./foot.inc");
?>
