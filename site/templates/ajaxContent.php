<?php namespace ProcessWire;
  if ($config->ajax) {
    $out = '';
    if (!$user->isSuperuser()) {
      $headTeacher = getHeadTeacher($user);
      $user->language = $headTeacher->language;
      if ($user->hasRole('player')) {
        $currentPlayer = $pages->get("parent.name=players, template=player, login=$user->name");
      }
    }
    switch ($input->get('id')) {
      case 'decision' :
        $pageId = $input->get('pageId');
        $p = $pages->get("id=$pageId");
        if ($p->is("template=player")) {
          // Possible places
          if ($p->coma == false) {
            $allPlaces = $pages->get("/places/")->find("template='place', sort='title'");
            $allPeople = $pages->find("template=people, name!=people, sort=title");
            $allEquipments = $pages->get("/shop/")->find("template=equipment|item, sort='title'");
            $possiblePlaces = $allPlaces->find("GC<=$p->GC, level<=$p->level, id!=$p->places,sort=name");
            // Possible people
            $possiblePeople = $allPeople->find("GC<=$p->GC, level<=$p->level, id!=$p->people,sort=name");
            // Possible equipments
            $nbEl = $p->places->count()+$p->people->count();
            $possibleEquipment = $allEquipments->find("GC<=$p->GC, level<=$p->level, freeActs<=$nbEl, id!=$p->equipment, parent.name!=potions, sort=-parent.name, sort=name");
            // Get rid of potions bought within the last 15 days
            $today = new \DateTime("today");
            $interval = new \DateInterval('P15D');
            $limitDate = strtotime($today->sub($interval)->format('Y-m-d'));
            $today = new \DateTime("today");
            $boughtPotions = $p->find("template=event, date>=$limitDate, refPage.name~*=potion, refPage.name!=health-potion");
            $possiblePotions = $allEquipments->find("GC<=$p->GC, level<=$p->level, freeActs<=$nbEl, parent.name=potions, sort=name")->not($boughtPotions);
            $healthPotion = $allEquipments->get("name=health-potion");
            if ($p->HP == 50) { $possiblePotions->remove($healthPotion); }
            $possibleItems = new pageArray();
            $possibleItems->add($possiblePlaces);
            if ($p->team->rank && $p->team->rank->is("index>=8")) { // Add people ONLY for 4emes/3emes
              $possibleItems->add($possiblePeople);
            }
            $possibleItems->add($possibleEquipment);
            $possibleItems->add($possiblePotions);
          }
          $team = $p->team;
          $donatorId = $p->id;
          if ($p->avatar) { $mini = '<img src="'.$p->avatar->getCrop('thumbnail')->url.'" alt="'.$p->title.'." />'; }
          $out .= '<div class="row">';
          $out .= '<div class="col-sm-4 text-center">';
          $out .= '<h3 class="thumbnail">';
            $out .= $mini.' <span class="caption">'.$p->title.'</span>';
            $out .= ' <small style="display:block;">';
              $out .= '<span class="label label-success">'.$nbEl.'el</span>';
              $out .= ' <span class="label label-danger">'.$p->GC.'GC</span>';
            $out .= '</small>';
          $out .= '</h3>';
          $out .= '</div>';
          $out .= '<div class="col-sm-8 text-left contrast">';
            $out .= '<h3>[I want to...]</h3>';
            // Organize team defense
            $nbConcerned = possibleDefense($team);
            if ($nbConcerned > 0) {
              $out .= '<h4><a href="'.$pages->get("name=quiz")->url.$team->name.'">→ Organize team defense.</a></h4>';
            }
            if ($p->is("parent.name!=groups")) { // Not for groups
              // Special discount
              if ($possibleItems->count() > 0) {
                if (rand(0,1)) { // Random special discount
                  // Pick a random item
                  $selectedItem = $possibleItems->getRandom();
                  if ($selectedItem->is("has_parent.name=places")) {
                    $details = ' in '.$selectedItem->city->title.' ('.$selectedItem->country->title.')';
                  } else if ($selectedItem->is("has_parent.name=people")) {
                    $details = ' from '.$selectedItem->country->title;
                  } else {
                    $details = ' ('.$selectedItem->category->title.')';
                  }
                  // Pick a random discount
                  $discount = $pages->find("parent=/specials")->getRandom();
                  $newPrice = round($selectedItem->GC-($selectedItem->GC*($discount->name/100))).'GC';
                  if ($newPrice == 0) { $newPrice = 'Free'; }
                  $out .= '<div id="discount">';
                    $out .= ' <h3><a href="#" class="buyBtn" data-url="'.$pages->get('name=submitforms')->url.'?form=buyForm&playerId='.$p->id.'&itemId='.$selectedItem->id.'&discount='.$discount->id.'" data-GC="'.$p->GC.'" data-item-price="'.$selectedItem->GC.'">→ Get <span class="label label-danger">'.$discount->title.'%</span> discount on <span class="label label-primary">'.$selectedItem->title.'</span> '.$details.' !</a></h3>';
                    $out .= '<div class="row">';
                      $out .= '<div class="col-sm-6 text-right">';
                        $out .= '<h3><span class="strikeText">'.$selectedItem->GC.'GC</span> → <span class="label label-success">'.$newPrice.'</span></h3>';
                      $out .= '</div>';
                      $out .= '<div class="col-sm-6 text-left">';
                      if ($selectedItem->photo) {
                        $out .= '<img class="img-thumbnail" src="'.$selectedItem->photo->eq(0)->getCrop('thumbnail')->url.'" /> ';
                      }
                      if ($selectedItem->image) {
                        $out .= '<img class="img-thumbnail" src="'.$selectedItem->image->getCrop('thumbnail')->url.'" /> ';
                      }
                      $out .= '</div>';
                    $out .= '</div>';
                  $out .= '</div>';
                } else {
                  $out .= '<div id="discount"><h3>Sorry, no discount available...</h3></div>';
                }
                // Play for a discount
                $out .= '<h4><a href="#" class="ajaxBtn" data-type="discount">→ Play for a random discount.</a></h4>';
              }
              // Go to the Marketplace
              if ($possibleItems->count() > 0) {
                $out .= '<h4>';
                $out .= '<a href="'.$pages->get("name=shop")->url.$team->name.'?playerId='.$p->id.'">→ Go to the marketplace.</a>';
                $out .= ' <span>['.sprintf(_n("%d possible element", "%d possible elements", $possibleItems->count()), $possibleItems->count()).']</span>';
                $out .= '</h4>';
              }
            }
            // Make a donation
            if ($p->GC > 5 || $p->is("parent.name=groups")) {
              $out .= '<h4><a href="'.$pages->get("name=makedonation")->url.$team->name.'/all/'.$donatorId.'">→ Make a donation.</a></h4>';
            }
            // Read about a random element
            $allElements = getAllElements($p);
            if ($allElements->count() > 0) {
              $randomId = $allElements->getRandom()->id;
              $out .= '<h4><a href="#" class="ajaxBtn" data-type="showInfo" data-id="'.$randomId.'">→ Read about 1 of my element.</a></h4>';

            }
          $out .= '</div>';
          $out .= '</div>';
        }
        if ($p->is("parent.name=groups")) {
          // Get group members
          $teamId = $input->get('teamId');
          $team = $pages->get("id=$teamId");
          $groupPlayers = $pages->find("template=player, team=$team, group=$pageId");
          $out .= '<div class="row">';
            $out .= '<h2 class="text-center"><span class="label label-primary">'.$p->title.'</span></h2>';
            $out .= '<div class="text-center">';
            $donatorId = $groupPlayers->sort('-GC')->first()->id;
            $groupPlayers->sort('-reputation');
            $out .= '<ul class="list-unstyled list-inline text-center">';
            foreach($groupPlayers as $gp) {
              if ($gp->avatar) { $mini = '<img src="'.$gp->avatar->getCrop('thumbnail')->url.'" alt="'.$gp->title.'." />'; } else { $mini = ''; }
              $out .= '<li>';
              $out .= '<div class="board panel panel-primary">';
                /* $out .= '<div class="panel-heading">'; */
                /*   $out .= '<h4 class="panel-title">'; */
                /*     $out .= $gp->title; */
                /*   $out .= '</h4>'; */
                /* $out .= '</div>'; */
                $out .= '<div class="panel-body">';
                  /* $out .= '<span class="thumbnail">'.$mini.'</span>'; */
                  $out .= '<span class="">'.$mini.'</span>';
                $out .= '</div>';
                $out .= '<div class="panel-footer text-center">';
                  $out .= $gp->title;
                $out .= '</div>';
              $out .= '</div>';
              $out .= '</li>';
            }
            $out .= '</ul>';
            $out .= '</div>';
          $out .= '</div>';
        }
        break;
      case 'ambassador' :
        $pageId = $input->get('pageId');
        $p = $pages->get("id=$pageId");
        if ($p->avatar) { $mini = '<img src="'.$p->avatar->getCrop('thumbnail')->url.'" alt="'.$p->title.'." />'; }
        $out .= '<h3 class="thumbnail">'.$mini.' <span class="caption">'.$p->title.'</span></h3>';
        unset($p);
        break;
      case 'help' :
        $playerId = $input->get('playerId');
        $player = $pages->get($playerId);
        $healingPotion = $pages->get("name=health-potion");
        $neededGC = $healingPotion->GC - $player->GC;
        $allHelpers = $pages->find("parent.name=players, team=$player->team, GC>=$neededGC, sort=name")->not($player);
        $out .= '<h2>'.sprintf(__('%d GC needed !'), $neededGC).'</h2>';
        if ($allHelpers->count() > 0) {
          $out .= '<p>'.__('Does a player want to help by making a donation ?').'</p>';
          $out .= '<ul class="col4 text-left list-unstyled">';
          foreach($allHelpers as $h) {
            $leftGC = $h->GC-$neededGC;
            $out .= '<li>';
            $out .= '<a href="'.$pages->get("name=main-office")->url.$player->team->name.'" data-href="'.$pages->get("name=submitforms")->url.'?form=quickDonation&receiver='.$player->id.'&donator='.$h->id.'&amount='.$neededGC.'" class="basicConfirm" data-reload="false" data-showFeedback="false" data-toRemove=".help-'.$player->id.'" data-toShow="#heal-'.$player->id.'" data-msg="['.$h->title.'→'.sprintf(__('%d GC left'), $leftGC).']">';
            $out .= $h->title.' ('.$h->GC.__("GC").')</a>';
            $out .= '</li>';
          }
          $out .= '</ul>';
        } else {
          $out .= '<p>'.__('No player has enough in the team. Team needs to get organized and collaborate to help the player !').'</p>';
        }
        // TODO Add Team wallet feature ?
        unset($allHelpers);
        break;
      case 'addRequest' :
        $allPlayers = $pages->find("id=$session->allPlayers, fight_request=''")->sort("title"); // Avoid pending fight requests
        if ($user->hasRole('teacher')) {
          $allMonsters = $pages->find("parent.name=monsters, template=exercise, (created_users_id=$user->id),(exerciseOwner.singleTeacher=$user,exerciseOwner.publish=1)")->sort("name");
        } else if ($user->isSuperuser()) {
          $allMonsters = $pages->find("parent.name=monsters, template=exercise, sort=name");
        }
        $out .= '<h1>'.__("Add a fight request ?").'</h1>';
        $out .= '<p>'.__("Select a player").' : ';
        $out .= '<select id="playerId">';
        foreach ($allPlayers as $p) {
          $out .= '<option value="'.$p->id.'">'.$p->title.' ['.$p->team->title.']';
        }
        $out .= '</select>';
        $out .= '</p>';
        $out .= '<p>'.__("Select a monster").' : ';
        $out .= '<select id="monsterId">';
        foreach ($allMonsters as $m) {
          $out .= '<option value="'.$m->id.'">'.$m->title.'</option>';
        }
        $out .= '</select>';
        $out .= '</p>';
        $out .= '<input type="hidden" id="submitFormUrl" value="'.$pages->get('name=submitforms')->url.'" />';
        unset($allPlayers);
        unset($allMonsters);
        break;
      case 'addLesson' :
        $allPlayers = $pages->find("id=$session->allPlayers")->sort("title");
        if ($user->hasRole('teacher')) {
          $allLessons = $pages->find("parent.name=book-knowledge-lessons, teacher=$user")->sort("title");
        } else if ($user->isSuperuser()) {
          $allLessons = $pages->find("parent.name=book-Knowledge-lessons")->sort("title");
        }
        $out .= '<h1>'.__("Add a lesson ?").'</h1>';
        $out .= '<p>'.__("Select a player").' : ';
        $out .= '<select id="playerId">';
        foreach ($allPlayers as $p) {
          $out .= '<option value="'.$p->id.'">'.$p->title.' ['.$p->team->title.']';
        }
        $out .= '</select>';
        $out .= '</p>';
        $out .= '<p>'.__("Select a lesson").' : ';
        $out .= '<select id="lessonId">';
        foreach ($allLessons as $m) {
          $out .= '<option value="'.$m->id.'">'.$m->title.'</option>';
        }
        $out .= '</select>';
        $out .= '</p>';
        $out .= '<input type="hidden" id="submitFormUrl" value="'.$pages->get('name=submitforms')->url.'" />';
        unset($allPlayers);
        unset($allLessons);
        break;
      case 'quickValidation' :
        if ($input->get("playerId")) {
          $player = $pages->get($input->get('playerId'));
          $team = $player->team;
          $showAllPlayers = false;
          $out .= '<input type="hidden" id="playerId" value="'.$player->id.'" />';
        } else {
          $showAllPlayers = true;
          $allPlayers = $pages->find("id=$session->allPlayers")->sort("title");
        }
        if ($showAllPlayers) {
          $out .= '<h3>'.__('Save quick action ?').'</h3>';
          $out .= '<label for="playerId">'.__("Choose a player").'</label> → ';
          $out .= '<select id="playerId" name="playerId">';
          foreach ($allPlayers as $p) {
            $out .= '<option value="'.$p->id.'">'.$p->title.' ['.$p->team->title.']</option>';
          }
          $out .= '</select>';
        } else {
          $out .= '<h3>'.sprintf(__('Save quick action for %1$s [%2$s] ?'), $player->title, $player->team->title).'</h3>';
        }
        if ($user->isSuperuser() || ($user->name == 'flieutaud' && $team->name == 'no-team')) {
          // Limit to selected team headTeacher
          if ($team->name == 'no-team') {
            $allTasks = $pages->find("template=task, adminOnly=0");
          } else {
            $allTasks = $pages->find("parent.name=tasks, owner.singleTeacher=$headTeacher")->sort("title");
          }
        } else { // Limit to logged in teacher
          $allTasks = $pages->find("parent.name=tasks, owner.singleTeacher=$user")->sort("title");
        }
        $out .= '<p>';
          $out .= '<label for="taskId">'.__("Choose an action").'</label> → ';
          $out .= '<select id="taskId">';
            foreach ($allTasks as $t) {
              $out .= '<option value="'.$t->id.'">'.$t->title.'</option>';
            }
          $out .= '</select>';
        $out .= '</p>';
        $out .= '<p>';
          $out .= '<input type="text" size="40" id="comment" name="comment" value="" placeholder="'.__("comment").'" />';
        $out .= '</p>';
        $out .= '<p>';
          $today = date('Y-m-d');
          $out .= '<label for="customDate">'.__("Custom date").'</label>';
          $out .= '<input id="customDate" name="customDate" type="date" size="8" value="'.$today.'" />';
        $out .= '</p>';
        $out .= '<input type="hidden" id="submitFormUrl" value="'.$pages->get('name=submitforms')->url.'" />';
        break;
      case 'game' :
        $gamePath = $input->get('gamePath');
        $gameTitle = $input->get('gameTitle');
        $out .= '<h3>'.sprintf(__('Print "%s" ?'), $gameTitle).'</h3>';
        $out .= '<div class="row">';
        $out .= '<img src="'.$gamePath.'" class="bigGame" />';
        $out .= '</div>';
        break;
      case 'audioMonster' :
        $pageId = $input->get('pageId');
        $m = $pages->get("id=$pageId");
        $exData = $m->exData;
        $out = '<h4 class="text-center"><span class="glyphicon glyphicon-volume-up"></span> '.__("Audio practice ! (experimental)").'</h4>';
        $allLines = preg_split('/$\r|\n/', $sanitizer->entitiesMarkdown($exData));
        $listWords = prepareListWords($allLines, $m->type->name, true);
        $out .= $listWords;
        break;
      case 'showInfo' :
        $pageId = $input->get('pageId');
        $input->get("more") ? $more = $input->get("more") : $more = '';
        $input->get("teamId") ? $team = $pages->get($input->get("teamId")) : $team = false;
        $input->get("playerId") ? $player = $pages->get($input->get("playerId")) : $player = false;
        $p = $pages->get("id=$pageId");
        if ($p->photo) { $mini = '<img src="'.$p->photo->eq(0)->getCrop('big')->url.'" alt="'.$p->title.'." />'; }
        if ($p->image) { $mini = '<img src="'.$p->image->getCrop('thumbnail')->url.'" alt="'.$p->title.'." />'; }
        if ($more != 'checkVoc') {
          $out .= '<h4><span class="label label-primary">'.$p->title.'</span>';
        } else {
          $title = '<span class="label label-primary">'.$p->title.'</span>';
          $out .= '<h4>'.sprintf(__("Let's check your skills on %s ! "), $title);
        }
        if ($p->is("template=place")) {
          $out .= ' (in '.$p->city->title.', '.$p->country->title.')</h3>';
        } else if ($p->is("template=people")) {
          $out .= ' (from '.$p->country->title.')</h3>';
        } else if ($p->is("template=exercise")) {
          $topics = $p->topic->implode(', ', '{title}');
          $out .= ' ('.$topics.')';
          if ($more == 'checkVoc') {
            $out .= ' <span id="numberChecked" class="pull-right badge badge-danger">0</span>';
          }
        } else if ($p->is("template=equipment|item")) {
          $out .= ' ('.$p->category->title.')';
        } else {
          $out .= 'TODO';
        }
        $out .= '</h4>';
        $out .= '<div class="row">';
        if ($more != 'checkVoc') {
          $out .= '<div class="col-sm-4 text-center">';
            $out .= '<h3 class="thumbnail">';
            $out .= $mini;
            $out .= '</h3>';
            if ($more == 'freeRate' && $team->id != '') {
              // Find element's # of owners
              $p = setElement($p, $team);
              if ($p->template == 'place') { $teamRate = $team->teamRatePlace; } else { $teamRate = $team->teamRatePeople; }
              $out .= '<p class="">'.__("Free rate").' : ['.$p->teamOwners->count().'/'.$teamRate.'] ';
              if ($p->teamOwners->count() > 0) {
                $ownersList = $p->teamOwners->implode(', ', '{title}');
                $out .= '<span class="glyphicon glyphicon-info-sign" data-toggle="tooltip"  onmouseenter="$(this).tooltip(\'show\');" title="'.$ownersList.'"></span>';
              }
              $out .= '</p> ';
              $out .= progressBar($p->teamOwners->count(), $teamRate);
              if ($p->completed == 1) { $out .= '<span class="badge">'.__("Congratulations !").'</span>'; }
            }
          $out .= '</div>';
          $out .= '<div class="col-sm-8 text-justify">';
            $out .= '<p class="lead">';
              $out .= $p->summary;
              if ($p->is("template=exercise")) {
                /* $out .= ' <button class="showInfo checkVoc btn btn-danger" data-id="'.$p->id.'" data-playerId="'.$player->id.'">'.__("Let's check !").'</button>'; */
                $out .= ' <button class="showPopup btn btn-danger" data-id="#popup-'.$p->id.'">'.__("Let's check !").'</button>';
              }
            $out .= '</p>';
            if ($p->is("template=place")) {
              $out .= '<p><a href="'.$p->url.'" target="_blank">'.__("[See details and map]").'</a></p>';
            }
            if ($p->is("template=exercise")) {
              $out .= '<h4>';
                $out .= __('Most trained player').' → ';
                if ($p->bestTrainedPlayerId != 0) {
                  $bestTrained = $pages->get($p->bestTrainedPlayerId);
                  $out .= '<span class="label label-primary">'.$p->best.' '.__('UT').' - '.$bestTrained->title.' ['.$bestTrained->team->title.']</span>';
                } else {
                  $out .= '-';
                }
              $out .= '</h4>';
              $out .= '<h4>';
                $out .= __('Master time').' → ';
                $out .= '<span class="label label-primary">';
                if ($p->bestTime) {
                  $master = $pages->get($p->bestTimePlayerId);
                  $out .= ms2string($p->bestTime).' '.__('by').' '.$master->title.' ['.$master->team->title.']';
                } else {
                  $out .= '-';
                }
                $out .= '</span>';
              $out .= '</h4>';
              if (isset($player) && $player != false) {
                if ($user->hasRole("player")) {
                  $out .= '<section class="frame">';
                  $out .= '<p class="text-center">'.sprintf(__('Your personal activity on %s'), $p->title).'</p>';
                  if ($player->avatar) { $mini = '<img class="pull-right" src="'.$player->avatar->getCrop('mini')->url.'" alt="avatar" />'; }
                  $out .= $mini;
                  $p = setMonster($player, $p);
                  $out .= '<p>';
                  $out .= '<span class="glyphicon glyphicon-time"></span> ';
                  $out .= __("Personal best time").' : ';
                  if ($p->bestTime != '') {
                    $out .= ms2string($p->bestTime);
                  } else {
                    $out .= '-';
                  }
                  $out .= '</p>';
                  // Player's Training
                  $out .= '<p>';
                  $out .= '<span class="glyphicon glyphicon-headphones"></span> ';
                  $out .= __("Personal training").' : ';
                  $out .= '<span class="label label-success">'.sprintf(__('%d UT'), $p->utGain).'</span> ';
                  $out .= __("Last training").' : ';
                  if ($p->lastTrainingInterval != '-1') {
                    $out .= $p->lastTrainingInterval;
                    $activityUrl = $pages->get("name=activity")->url;
                    $out .= ' <a href="'.$activityUrl.$p->id.'/'.$player->id.'" data-type="analyzer" class="ajaxBtn btn btn-primary btn-xs" data-url="'.$activityUrl.$p->id.'/'.$player->id.'" data-toggle="tooltip" onmouseenter="$(this).tooltip(\'show\');" title="'.__('Activity analysis').'"><span class="glyphicon glyphicon-stats"></span></a> ';
                  } else {
                    $out .= '-';
                  }
                  $out .= '</p>';
                  // Player's Fights
                  $out .= '<p>';
                  $out .= '<span class="glyphicon glyphicon-flash"></span> ';
                  $out .= __("Personal fights").' : ';
                  if ($p->lastFightInterval != '-1') {
                    $out .= '<span>'.sprintf(_n("%d fight", "%d fights", $p->fightNb), $p->fightNb);
                    $out .= '<span data-toggle="tooltip" title="Quality : '.$p->quality.'" onmouseenter="$(this).tooltip(\'show\');" data-html="true"> '.averageLabel($p->quality).'</span>';
                    $out .= '  ['.__("Last fight").' : '.$p->lastFightInterval.']</span>';
                    $out .= ' <a href="'.$activityUrl.$p->id.'/'.$player->id.'" data-type="analyzer" class="ajaxBtn btn btn-primary btn-xs" data-url="'.$activityUrl.$p->id.'/'.$player->id.'" data-toggle="tooltip" onmouseenter="$(this).tooltip(\'show\');" title="'.__('Activity analysis').'"><span class="glyphicon glyphicon-stats"></span></a> ';
                  } else {
                    $out .= '-';
                  }
                  $out .= '</p>';
                  $out .= '</section>';
                } else { // TODO : Show only % memorized estimation ?
                  $out .= '<section class="frame">';
                  $out .= '<p class="text-center">'.sprintf(__('Activity on %1$s for %2$s'), $p->title, $player->title).'</p>';
                  if ($player->avatar) { $mini = '<img class="pull-right" src="'.$player->avatar->getCrop('mini')->url.'" alt="avatar" />'; } else { $mini = '<p class="pull-right">No avatar</p>'; }
                  $out .= $mini;
                  $p = setMonster($player, $p);
                  $out .= '<p>';
                  $out .= '<span class="glyphicon glyphicon-time"></span> ';
                  $out .= __("Personal best time").' : ';
                  if ($p->bestTime != '') {
                    $out .= ms2string($p->bestTime);
                  } else {
                    $out .= '-';
                  }
                  $out .= '</p>';
                  // Player's Training
                  $out .= '<p>';
                  $out .= '<span class="glyphicon glyphicon-headphones"></span> ';
                  $out .= __("Personal training").' : ';
                  $out .= '<span class="label label-success">'.sprintf(__('%d UT'), $p->utGain).'</span> ';
                  $out .= __("Last training").' : ';
                  if ($p->lastTrainingInterval != '-1') {
                    $out .= $p->lastTrainingInterval;
                    $activityUrl = $pages->get("name=activity")->url;
                    $out .= ' <a href="'.$activityUrl.$p->id.'/'.$player->id.'" data-type="analyzer" class="ajaxBtn btn btn-primary btn-xs" data-url="'.$activityUrl.$p->id.'/'.$player->id.'" data-toggle="tooltip" onmouseenter="$(this).tooltip(\'show\');" title="'.__('Activity analysis').'"><span class="glyphicon glyphicon-stats"></span></a> ';
                  } else {
                    $out .= '-';
                  }
                  $out .= '</p>';
                  // Player's Fights
                  $out .= '<p>';
                  $out .= '<span class="glyphicon glyphicon-flash"></span> ';
                  $out .= __("Personal fights").' : ';
                  if ($p->lastFightInterval != '-1') {
                    $out .= '<span>'.sprintf(_n("%d fight", "%d fights", $p->fightNb), $p->fightNb);
                    $out .= '<span data-toggle="tooltip" title="Quality : '.$p->quality.'" onmouseenter="$(this).tooltip(\'show\');" data-html="true"> '.averageLabel($p->quality).'</span>';
                    $out .= '  ['.__("Last fight").' : '.$p->lastFightInterval.']</span>';
                    $out .= ' <a href="'.$activityUrl.$p->id.'/'.$player->id.'" data-type="analyzer" class="ajaxBtn btn btn-primary btn-xs" data-url="'.$activityUrl.$p->id.'/'.$player->id.'" data-toggle="tooltip" onmouseenter="$(this).tooltip(\'show\');" title="'.__('Activity analysis').'"><span class="glyphicon glyphicon-stats"></span></a> ';
                  } else {
                    $out .= '-';
                  }
                  $out .= '</p>';
                  $out .= '</section>';
                }
              }
            }
          $out .= '</div>';
          if ($p->is("template=exercise")) {
            $out .= '<div id="popup-'.$p->id.'" class="hidden">';
              $out .= '<h4 class="text-center">';
                $out .= ' <span id="numberChecked" class="badge badge-danger">0</span>';
              $out .= '</h4>';
              $generatedData = pickFromRawData($p->exData, $p->type->name);
              if ($p->type->is("name=image-map|quiz") && $p->imageMap->count() > 0) { // Show image if needed
                $out .= '<img src="'.$p->imageMap->first()->url.'" alt="Image" max-width="800" />';
              }
              $out .= $generatedData;
            $out .= '</div>';
          }
        } else {
          $generatedData = pickFromRawData($p->exData, $p->type->name);
          if ($p->type->name == 'image-map' && $p->imageMap->count() > 0) { // Show image if needed
            $out .= '<img src="'.$p->imageMap->first()->url.'" alt="Image" max-width="800" />';
          }
          $out .= $generatedData;
        }
        $out .= '</div>';
        unset($p);
        break;
      case 'buy' :
        $pageId = $input->get('pageId');
        $p = $pages->get("id=$pageId");
        if ($p->photo) { $mini = '<img src="'.$p->photo->eq(0)->getCrop('big')->url.'" alt="'.$p->title.'." />'; }
        if ($p->image) { $mini = '<img src="'.$p->image->getCrop('thumbnail')->url.'" alt="'.$p->title.'." />'; }
        $out .= '<h3><span class="label label-primary">'.$p->title.'</span>';
        if ($p->is("template=place")) {
          $out .= ' ('.__("in").' '.$p->city->title.', '.$p->country->title.')</h3>';
        } else if ($p->is("template=people")) {
          $out .= ' ('.__("from").' '.$p->country->title.')</h3>';
        } else {
          $out .= ' ('.$p->category->title.')</h3>';
        }
        $out .= '<div class="row">';
        $out .= '<div class="col-sm-4 text-center">';
          $out .= '<div class="">';
            $out .= $mini;
            if ($p->is("template=place|people")) {
              if ($currentPlayer->team->is("name!=no-team")) {
                // Find element's # of owners
                $p = setElement($p, $currentPlayer->team);
                if ($p->template == 'place') { $p->teamRate = $p->teamRatePlace; } else { $p->teamRate = $p->teamRatePeople; }
                $out .= '<p class="">'.__("Free rate").' : ['.$p->teamOwners->count().'/'.$p->teamRate.'] ';
                if ($p->teamOwners->count() > 0) {
                  $ownersList = $p->teamOwners->implode(', ', '{title}');
                  $out .= '<span class="glyphicon glyphicon-info-sign" data-toggle="tooltip"  onmouseenter="$(this).tooltip(\'show\');" title="'.$ownersList.'"></span>';
                }
                $out .= '</p> ';
                $out .= progressBar($p->teamOwners->count(), $p->teamRate);
                if ($p->completed == 1) { $out .= '<span class="badge">'.__("Congratulations !").'</span>'; }
              }
            }
          $out .= '</div>';
        $out .= '</div>';
        $out .= '<div class="col-sm-8 text-justify">';
          $out .= '<br/>';
          $out .= '<p>'.$p->summary.'</p>';
        $out .= '</div>';
        $out .= '</div>';
        $out .= '<br />';
        $out .= '<div class="row">';
          $out .= '<span class="alert alert-info">'.__("This item costs");
          $out .= ' '.$p->GC.__('GC');
          $out .= ' ('.sprintf(__("You have %d GC"), $currentPlayer->GC).')</span>';
        $out .= '</div>';
        if (!isset($currentPlayer->group->id) && $p->is("template=item") && $p->category->name == 'group-items') {
          $out .= '<br /><br />';
          $out .= '<span class="alert alert-warning">'.__("No groups are set. This item will be individual !").'</span>';
        }
        unset($p);
        break;
      case 'activityreport' : // Activity report
        $playerId = $input->get('playerId');
        $playerPage = $pages->get("id=$playerId");
        $activityUrl = $pages->get("name=activity")->url;
        // From tmpCache if available
        $today = new \DateTime("today");
        $tmpPage = $playerPage->child("name=tmp");
        if ($tmpPage->id && $tmpPage->tmpMonstersActivity) {
          $allConcernedMonsters = $tmpPage->tmpMonstersActivity->find("trainNb>0, monster.parent.name=monsters");
          $allConcernedMonsters->sort("lastTrainDate, date");
          if ($allConcernedMonsters->count() > 0) {
            $totalFightsNb =  array_sum(mb_split(',', $allConcernedMonsters->implode(',', '{fightNb}')));
            $allFoughtMonstersNb = $allConcernedMonsters->find("fightNb>0")->count();
            echo '<p class="label label-danger"> '.sprintf(__("You have trained on %d different monsters"), $allConcernedMonsters->count()).'</p>';
            echo ' <span class="label label-danger"> ';
            echo sprintf(_n('You have done %d fight ', 'You have done %d fights ', $totalFightsNb), $totalFightsNb);
            echo sprintf(_n('on %d monster', 'on %d different monsters', $allFoughtMonstersNb), $allFoughtMonstersNb);
            echo '</span>';
            if ($totalFightsNb < 10) {
              echo ' <span class="label label-danger">';
                echo ' <span class="glyphicon glyphicon-thumbs-down"></span> ';
                echo __("The Fighters playground is still locked !");
                echo ' <span class="glyphicon glyphicon-info-sign" datat-toggle="tooltip" title="'.__("10 fights are required to become a Fighter and gain access to the Fighters playground.").'" onmouseenter="$(this).tooltip(\'show\');"></span> ';
              echo '</span>';
            } else {
              echo ' <span class="label label-success">';
                echo ' <span class="glyphicon glyphicon-thumbs-up"></span> ';
                echo __("The Fighters playground is unlocked !");
              echo '</span>';
            }
            echo '<ul class="utReport list-group list-unstyled">';
            foreach($allConcernedMonsters as $m) {
              setMonster($playerPage, $m->monster);
              // Find # of days compared to today
              $date2 = new \DateTime(date("Y-m-d", $m->lastTrainDate));
              $interval = $today->diff($date2);
              $m->lastTrainingInterval = $interval->days;
              $date3 = new \DateTime(date("Y-m-d", $m->date));
              $availableInterval = $today->diff($date3);
              echo '<li>';
              echo '<span>';
              if ($m->monster->is("template!=megamonster")) {
                echo ' <a href="'.$activityUrl.$m->monster->id.'/'.$playerId.'" data-type="analyzer" class="ajaxBtn btn btn-primary btn-xs" data-url="'.$activityUrl.$m->monster->id.'/'.$playerId.'" data-toggle="tooltip" title="'.__('Activity analysis').'" onmouseenter="$(this).tooltip(\'show\');" onmouseleave="$(this).tooltip(\'hide\');"><span class="glyphicon glyphicon-stats"></span></a> ';
                if ($m->monster->isBestTrained) { 
                  echo '<span class="glyphicon glyphicon-thumbs-up"></span> ';
                }
                if ($m->monster->isMaster) { 
                  echo '<span class="glyphicon glyphicon-time"></span> ';
                }
                if ($m->monster->quality > 0.3 && $m->monster->fightNb >= 3) {
                  echo '<span class="glyphicon glyphicon-star"></span> ';
                }
                echo '<a href="#" class="monsterInfo" data-href="'.$m->monster->url.'?playerId='.$playerPage->id.'">'.$m->monster->title.'</a>';
              } else {
                echo __('Mega-monster training').' → '.$m->monster->title;
              }
              echo '</span> : ';
              echo '<span>'.sprintf(__('%d UT'), $m->monster->utGain).'</span>';
              if ($m->monster->fightNb > 0) {
                echo ', ';
                echo '<span>'.sprintf(_n("%d fight", "%d fights", $m->monster->fightNb), $m->monster->fightNb);
                echo ' → <span data-toggle="tooltip" title="Quality : '.$m->monster->quality.'" onmouseenter="$(this).tooltip(\'show\');" data-html="true"> '.averageLabel($m->monster->quality).'</span>';
              }
              if ($m->monster->isTrainable != 1) {
                if ($m->monster->waitForTrain == 1) { $label = __('Available tomorrow !'); } else { $label = sprintf(__("Available in %d days"), $m->monster->waitForTrain); }
                echo ' <span data-toggle="tooltip" title="'.$label.'" onmouseenter="$(this).tooltip(\'show\');">[Last training : '.$m->monster->lastTrainingInterval.']</span>';
              }
              echo '</li>';
            }
            $allMonsters = getAllMonsters($playerPage);
            $neverTrainedNb = $allMonsters->count()-$allConcernedMonsters->count();
            if ($neverTrainedNb == 0) {
              echo '<li class="label label-success">'.__("You have trained on ALL monsters !").'</li>';
            } else {
              echo '<li class="label label-danger">'.sprintf(__('You have NEVER trained on %d monsters'), $neverTrainedNb).'</li>';
            }
            if ($neverTrainedNb != $tmpPage->index) { $tmpPage->setAndSave("index", $neverTrainedNb); }
            echo '</ul>';
          } else {
            echo "<p>".__("You have never used the Memory Helmet.")."</p>";
          }
        }
        unset($allConcernedMonsters);
        unset($allMonsters);
        $pages->unCacheAll();
        break;
      case 'battlereport' : // Battle report
        $playerId = $input->get('playerId');
        $playerPage = $pages->get("id=$playerId");
        $headTeacher = getHeadTeacher($playerPage);
        $allBattles = battleReport($playerPage);
        $attacks = array();
        $allBattles->sort("-date");
        foreach ($allBattles as $p) { // Group by linkedId
          if ($p->linkedId != '') {
            $attacks["$p->linkedId"][] = $p;
          } else { // Make a unique id
            $uniqueId = mt_rand(100000, 999999); // Unique test number
            $attacks["$uniqueId"][] = $p;
          }
        }
        if (count($attacks) > 0) {
          echo '<p>';
            echo '<span class="label label-primary">'.sprintf(_n('You have faced %d monster attack during the current school year.', 'You have faced %d monster attacks during the current school year.', count($attacks)), count($attacks)).'</span> ';
            echo '<a href="'.$pages->get("name=results")->url.$playerPage->name.'" target="_blank">[<span class="glyphicon glyphicon-stats"></span> '.__('See my results analysis.').']</a>';
          echo '</p>';
          echo '<ul class="utReport list-group list-unstyled">';
          foreach($attacks as $key => $gr){
            $count = count($gr);
            if($count > 1){
              $pos = 0;
              $neg = 0;
              $vv = 0;
              $v = 0;
              $r = 0;
              $rr = 0;
              echo '<li>';
              foreach($gr as $key => $m){
                switch($m->task->name) {
                  case 'battle-vv': $vv++; $pos++; break;
                  case 'battle-v': $v++; $pos++; break;
                  case 'battle-r': $r++; $neg++; break;
                  case 'battle-rr': $rr++; $neg++; break;
                  default: break;
                }
              }
              if ($vv != 0) echo '<span class="label label-success">'.$vv.'VV</span>';
              if ($v != 0) echo ' <span class="label label-success">'.$v.'V</span>';
              if ($r != 0) echo ' <span class="label label-danger">'.$r.'R</span>';
              if ($rr != 0) echo ' <span class="label label-danger">'.$rr.'RR</span>';
              $testTitle = $gr[0]->summary;
              preg_match('/\[(.*?)\]/', $gr[0]->summary, $matches);
              echo ' : '.$matches[1].' ['.date("d/m", $gr[0]->date).']';
              echo "</li>";
            } else if ($count) {
              foreach($gr as $key => $m){
                echo '<li>';
                echo $m->result.' : '.$m->getLanguageValue($headTeacher->language, 'summary');
                echo ' ['.date("d/m", $m->date).']';
                echo "</li>";
              }
            }
            echo "</li>";
          }
          echo '</ul>';
        } else {
          echo "<p>".__("You haven't faced any monster attacks during the current school year.")."</p>";
        }
        unset($allBattles);
        $pages->unCacheAll();
        break;
      case 'history' :
        $playerId = $input->get('playerId');
        $playerPage = $pages->get($playerId);
        $headTeacher = getHeadTeacher($playerPage);
        if ($input->get->limit != 0) {
          $limitDate  = new \DateTime("@".$input->get->limit);
          $limitDate = strtotime($limitDate->format('Y-m-d'));
          $allEvents = $playerPage->child("name=history")->find("template=event, date>=$limitDate, sort=-date");
        } else {
          $allEvents = $playerPage->child("name=history")->find("template=event, sort=-date");
        }
        $allCategories = new PageArray();
        foreach ($allEvents as $e) {
          if (isset($e->task->category)) {
            $allCategories->add($e->task->category);
            $allCategories->sort("title");
          }
        }
        $out .= '<div id="Filters" data-fcolindex="2" class="text-center">';
        $out .= ' <ul class="list-inline well">';
        foreach ($allCategories as $c) {
          $out .= '<li><label for="'.$c->name.'" class="btn btn-primary btn-xs">'.$c->title.' <input type="checkbox" value="'.$c->title.'" class="categoryFilter" name="categoryFilter" id="'.$c->name.'"></label></li>';
        }
        $out .= '</ul>';
        $out .= '</div>';
        $out .= '<div class="btn-group pull-right">';
          $out .= '<button type=="button" id="allOnly" class="btn btn-default btn-xs">'.__("All").'</button>';
          $out .= '<button type=="button" id="positiveOnly" class="btn btn-default btn-xs">'.__("+ only").'</button>';
          $out .= '<button type=="button" id="negativeOnly" class="btn btn-default btn-xs">'.__("- only").'</button>';
        $out .= '</div>';
        $out .= ' <table id="historyTable" class="table table-condensed table-hover">';
        $out .= '  <thead>';
        $out .= '    <tr>';
        $out .= '    <th>Date</th>';
        $out .= '    <th>+/-</th>';
        $out .= '    <th>Category</th>';
        $out .= '    <th>Title</th>';
        $out .= '    <th>Comment</th>';
        $out .= '    <th>Hidden</th>';
        if ($user->isSuperuser() || $user->hasRole('teacher')) {
          $out .= '    <th></th>';
        }
        $out .= '  </tr>';
        $out .= '  </thead>';
        $out .= '  <tbody>';
        foreach($allEvents as $event) {
          $event->task = checkModTask($event->task, $headTeacher);
          if ($event->task->XP > 0 || ($event->task->is("name=free|buy|positive-collective-alchemy|donation|donated"))) {
            $class = '+';
            $searchString = '+++';
          } else {
            $class = '-';
            $searchString = '---';
          }
          $out .= '<tr>';
          $out .= '<td data-order='.$event->date.'>';
          if ($event->date != '') {
            $out .= date("d/m", $event->date);
          } else {
            $out .= 'Date error!';
          }
          $out .= '</td>';
          $out .= '<td>';
          $out .= $class;
          $out .= '</td>';
          $out .= '<td>';
          $out .= $event->task->category->title;
          $out .= '</td>';
          $out .= '<td>';
            if ($user->isSuperuser() || $user->hasRole('teacher')) {
              $out .= $event->task->title; // Depending on teacher's variation
              $out .= ' [';
              $out .= $event->getLanguageValue($headTeacher->language, 'title'); // Event title
              if ($event->task->teacherTitle != '') {
                $out .= ' = '.$event->task->teacherTitle.']';
              } else {
                $out .= ']';
              }
            } else {
              if ($event->task->teacherTitle != '') {
                $out .= $event->task->teacherTitle;
              } else {
                $out .= $event->task->title;
              }
            }
            if ($event->inClass == 1 && $event->task->is("name^=fight|ut-action")) {
              $out .= ' '.__('[in class]');
            }
          $out .= '</td>';
          $out .= '<td>';
            $summary = $event->getLanguageValue($headTeacher->language, 'summary');
            $summary == '' ? $summary = '-' : '';
            $out .= $summary;
          $out .= '</td>';
          $out .= '<td>';
          $out .= $searchString;
          $out .= '</td>';
          if ($user->isSuperuser()) {
            $out .= '<td>'.$event->feel().'</td>';
          }
          if ($user->hasRole('teacher')) {
            $out .= '<td><a href="'.$config->urls->admin.'page/edit/?id='.$event->id.'" target="blank">'.__("[Edit]").'</a></td>';
          }
          $out .= '</tr>';
        }
        $out .= ' </tbody>';
        $out .= '</table>';
        unset($allCategories);
        unset($allEvents);
        $pages->unCacheAll();
        break;
      case 'last10' :
        $playerId = $input->get('playerId');
        $playerPage = $pages->get("id=$playerId");
        $headTeacher = getHeadTeacher($playerPage);
        $allEvents = $playerPage->child("name=history")->find("template=event,sort=-date, limit=10");
        $out = '';
        $out .= '<ul class="list-unstyled">';
          if ($allEvents->count() > 0) {
            foreach ($allEvents as $event) {
              $event->task = checkModTask($event->task, $headTeacher);
              if ($event->task->HP < 0) {
                $className = 'negative';
                $sign = '';
                $signicon = '<span class="glyphicon glyphicon-minus-sign"></span> ';
              } else {
                $className = 'positive';
                //$className = '';
                $sign = '+';
                $signicon = '<span class="glyphicon glyphicon-plus-sign"></span> ';
              }
              $out .= '<li class="'.$className.'">';
              $out .= $signicon;
              $out .= date("d M (D)", $event->date).' : ';
              if ($className == 'negative') {
                $out .= '<span data-toggle="tooltip" title="HP" class="badge badge-warning">'.$sign.$event->task->HP.'HP</span> ';
              }
              if ($event->task->teacherTitle != '') {
                $out .= $sanitizer->markupToText($event->task->teacherTitle);
              } else {
                $out .= $sanitizer->markupToText($event->task->title);
              }
              if ($event->summary != '') {
                $out .= ' ['.$sanitizer->markupToText($event->summary).']';
              }
              $out .= '</li>';
            };
          } else {
            $out .= __('No personal history yet...');
          }
        $out .= '</ul>';
        break;
      case 'work-statistics' :
        $playerId = $input->get("playerId");
        $player = $pages->get("id=$playerId");
        $periodId = $input->get("periodId");
        $officialPeriod= $pages->get("id=$periodId");
        // Limit to official period dates
        $dateStart = $officialPeriod->dateStart;
        $dateEnd = $officialPeriod->dateEnd;
        $allEvents = $player->child("name=history")->find("template=event, date>=$dateStart, date<=$dateEnd, sort=-date");
        // Participation
        $out = '';
        setParticipation($player);
        $out .= '<p>';
        $out .= '<span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" title="Compétence SACoche : Je participe en classe." onmouseenter="$(this).tooltip(\'show\');"></span> '.__('Communication').' ';
        $out .= ' ⇒ ';
        switch ($player->participation) {
          case 'NN' : $class='primary';
            break;
          case 'VV' : $class='success';
            break;
          case 'V' : $class='success';
            break;
          case 'R' : $class='danger';
            break;
          case 'RR' : $class='danger';
            break;
          default: $class = '';
        }
        $out .=  '<span data-toggle="tooltip" data-html="true" onmouseenter="$(this).tooltip(\'show\');" title="<span class=\'label label-success\'>VV</span> <span class=\'label label-success\'>V</span> <span class=\'label label-danger\'>R</span> <span class=\'label label-danger\'>RR</span> See report below." class="label label-'.$class.'">'.$player->participation.'</span>';
        if ($player->partRatio != '-') {
          $out .= ' <span data-toggle="tooltip" onmouseenter="$(this).tooltip(\'show\');" title="Participation positive">'.$player->partPositive.' <i class="glyphicon glyphicon-thumbs-up"></i></span> <span data-toggle="tooltip" onmouseenter="$(this).tooltip(\'show\');" title="Participation négative">'.$player->partNegative.' <i class="glyphicon glyphicon-thumbs-down"></i></span>';
        }
        // Homework stats
        setHomework($player, $officialPeriod->dateStart, $officialPeriod->dateEnd);
        $out .= '<p><span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-html="true" onmouseenter="$(this).tooltip(\'show\');" title="Exercices non faits ou à moitié faits<br />Compétence SACoche : Je peux présenter mon travail fait à la maison."></span> Training problems :';
        $out .= ' <span class="">'.$player->hkPb.'</span>';
        $out .= ' [<span>'.$player->noHk->count().' Hk</span> - <span>'.$player->halfHk->count().' HalfHk</span> - <span>'.$player->notSigned->count().' notSigned</span>]';
        $out .= ' ⇒ ';
        switch ($player->homework) {
          case 'NN' : $class='primary'; break;
          case 'VV' : $class='success'; break;
          case 'V' : $class='success'; break;
          case 'R' : $class='danger'; break;
          case 'RR' : $class='danger'; break;
          default: $class = 'primary';
        }
        $out .=  '<span data-toggle="tooltip" data-html="true" onmouseenter="$(this).tooltip(\'show\');" title="<span class=\'label label-success\'>VV</span> 0/0.5 problems<br /><span class=\'label label-success\'>V</span> 1/1.5 problems<br /><span class=\'label label-danger\'>R</span> 2/2.5 problems<br /><span class=\'label label-danger\'>RR</span> 3/+ problems" class="label label-'.$class.'">'.$player->homework.'</span> ';
        // Forgotten material
        $out .= '<p><span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-html="true" onmouseenter="$(this).tooltip(\'show\');" title="Affaires oubliées<br />Compétence SACoche : J\'ai mon matériel."></span> '.__('Forgotten material').' : ';
        $out .= '<span>'.$player->noMaterial->count().'</span>';
        $out .= ' ⇒ ';
        switch ($player->materialLabel) {
          case 'NN' : $class='primary'; break;
          case 'VV' : $class='success'; break;
          case 'V' : $class='success'; break;
          case 'R' : $class='danger'; break;
          case 'RR' : $class='danger'; break;
          default: $class = 'primary';
        }
        $out .=  '<span data-toggle="tooltip" data-html="true" onmouseenter="$(this).tooltip(\'show\');" title="<span class=\'label label-success\'>VV</span> 0 oublis<br /><span class=\'label label-success\'>V</span> 1 oubli<br /><span class=\'label label-danger\'>R</span> 2 oublis<br /><span class=\'label label-danger\'>RR</span> 3 oublis (ou +)" class="label label-'.$class.'">'.$player->materialLabel.'</span>';
        $out .= '</p>';
        // Extra-hk
        $out .= '<p><span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-html="true" onmouseenter="$(this).tooltip(\'show\');" title="Travail supplémentaire HORS-CLASSE : extra-homework, personal initiative, underground training...<br />Compétence SACoche : Je prend une initiative particulière."></span> Personal motivation :';
        $out .= ' <span> ['.sprintf(__('%d extra'), $player->extraHk->count()).' - </span>';
        $out .= ' <span>'.sprintf(__('%d initiative'), $player->initiative->count()).' - </span>';
        $out .= ' <span>'.sprintf(__('%d UT/FP sessions'), $player->outClassActivity).']</span>';
        $out .= ' ⇒ ';
        $out .= '<span data-toggle="tooltip" data-html="true" onmouseenter="$(this).tooltip(\'show\');" title="<span class=\'label label-success\'>VV</span> 9xtHk/init. AND 47→49 UT/FP<br /><span class=\'label label-success\'>VV</span> 10xtHk/init. OR 50→+ UT/FP<br /><span class=\'label label-success\'>V</span> 4xtHk/init. AND 18→19 UT/FP<br /><span class=\'label label-success\'>V</span> 5xtHK/init. OR 20→49 UT/FP" class="label label-'.$class.'">'.$player->motivation.'</span> ';
        $out .= '</p>';
        
        // Attitude
        $disobedience = $allEvents->find("task.name=civil-disobedience");
        $ambush = $allEvents->find("task.name=ambush");
        $noisy = $allEvents->find("task.name=noisy-mission");
        $late = $allEvents->find("task.name=late");
        $pb = new PageArray();
        $pb->add($disobedience);
        $pb->add($ambush);
        $pb->add($noisy);
        $pb->add($late);
        $out .= '<p><span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-html="true" onmouseenter="$(this).tooltip(\'show\');" title="Soucis avec l\'attitude<br />Compétences SACoche : respect / attention en classe..."></span> '.__("Attitude problems").' :';
        $attPb = $disobedience->count()+$ambush->count()+$noisy->count();
        $out .= ' <span>['.sprintf(__('%d problems'), $attPb).' - </span>';
        $out .= ' <span>'.sprintf(__('%d slow moves'), $late->count()).']</span>';
        $out .= ' ⇒ ';
        if ($pb->count() == 0) {
          $out .= '<span data-toggle="tooltip" data-html="true" onmouseenter="$(this).tooltip(\'show\');" title="<span class=\'label label-success\'>VV</span> 0 problems<br /><span class=\'label label-success\'>V</span> <span class=\'label label-danger\'>R</span> <span class=\'label label-danger\'>RR</span> Ask your teacher" class="label label-success">VV</span>';
        } else {
          $out .= '<span>'.__("Ask your teacher.").'</span>';
        }
        $out .= '</p>';
        unset($allEvents);
        $pages->unCacheAll();
        break;
      case 'unPublish' : // Unpublish announcement
        $announcementId = $input->get("announcementId");
        $playerId = $input->get("playerId");
        if ($announcementId != '' && $playerId != '') {
          $announcement = $pages->get($announcementId);
          $player = $pages->get($playerId);
          if ($announcement->selectPlayers == 1) { // Untick player
            $announcement->playersList->remove($player);
            if ($announcement->playersList->count() == 0) { // No more ticked players, unpublish
              $announcement->publish = 0;
            }
          } else { // Team announcement, make it individual
            $announcement->selectPlayers = 1;
            $teamPlayers = $pages->find("template=player, team=$player->team")->not($player);
            foreach ($teamPlayers as $p) {
              $announcement->playersList->add($p);
            }
          }
          $announcement->of(false);
          $announcement->save();
        }
        unset($announcement);
        break;
      case 'checkHighscore' :
        $monsterId = $input->get("monsterId");
        $confirm = $input->get("confirm");
        $m = $pages->get($monsterId);
        $testPlayer = $pages->get("template=player, name=test");
        $allUt = $pages->findMany("template=event, task.name^=ut-action, refPage=$m, has_parent!=$testPlayer");
        $newBest = [];
        foreach($allUt as $e) {
          $utGain = 1;
          $pId = $e->parent("template=player")->id;
          if (!isset($newBest[$pId])) {
            $newBest[$pId] = 0;
          }
          // Test also french field
          if ($e->summary != '') {
            preg_match("/\[\+([\d]+)[UE]\.?[TS]\.?\]/", $e->summary, $matches);
          } else {
            $e->of(false);
            preg_match("/\[\+([\d]+)[UE]\.?[TS]\.?\]/", $e->summary->getLanguageValue($french), $matches);
          }
          if ($matches) {
            $utGain = $matches[1];
          }
          $newBest[$pId] += $utGain;
        }
        if (count($newBest) > 0) {
          $newBestUt = max($newBest);
          $newBestId = array_search(max($newBest),$newBest);
          $newBestPlayer = $pages->get($newBestId);
        } else {
          $newBestPlayer = false;
          $newBestId = 0;
          $newBestUt = 0;
        }
        if (!$confirm) {
          if (($newBestId == $m->bestTrainedPlayerId && $newBestUt == $m->best) || ($m->bestTrainedPlayerId == 0 && $newBestId == 0)) {
            $out = '&nbsp;<span class="label label-success">OK</span>';
          } else {
            $out = '&nbsp;<span class="label label-danger">Error</span> : '.$newBestPlayer->title.' → '.$newBestUt.'UT';
            $options = [
              'data' => [
                'id' => 'checkHighscore',
                'monsterId' => $monsterId,
                'confirm' => 'true'
              ]
            ];
            $out .= ' <button class="simpleAjax btn btn-xs btn-danger" data-href="'.$page->url($options).'" data-disable="true">Save ?</button> <span class="glyphicon glyphicon-alert"></span> ';
          }
        } else { // Saving new highscore
          $m->of(false);
          $m->bestTrainedPlayerId = $newBestId;
          $m->best = $newBestUt;
          $m->save();
          $out = '<span class="label label-success">✓ Saved !</span>';
          $out .= 'New highscore : '.$newBestUt.'UT for '.$newBestPlayer->title;
        }
        unset($allUt);
        break;
      default :
        $out = __("todo");
    }
    echo $out;
  }
?>
