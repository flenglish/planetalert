<?php namespace ProcessWire;
  include("./head.inc"); 

  if (isset($player) && $user->isLoggedin() && $player->team->name == $input->urlSegment1 || $user->isSuperuser() || $user->hasRole('teacher')) {
    if ($user->isLoggedin()) {
      if ($session->allPlayers) {
        $allPlayers = $pages->find("id=$session->allPlayers");
      } else {
        $allPlayers = getAllPlayers($user, false);
        $session->allPlayers = (string) $allPlayers;
      }
    }

    $out = '';

    $tooltip = '<p>';
      $tooltip .= __('Your team needs to fight the following mega-monsters to completely get rid of monsters all over the planet !');
    $tooltip .= '</p>';
    $tooltip .= '<p>';
    $tooltip .= __('You can practise online, but the fights will happen in class, all at once for every players in your team ! You must get an 85% success score to defeat a mega-monster.');
    $tooltip .= '</p>';
    $tooltip .= '<p>';
    $tooltip .= __('So get ready for the final fights to completely free YOUR planet !');
    $tooltip .= '</p>';

    if ($input->urlSegment1) {
      $team = $pages->get("parent.name=teams, name=$input->urlSegment1");
      $out .= '<h2 class="text-center">';
        $out .= '<span class="glyphicon glyphicon-warning-sign squeeze"></span> '.$page->title.' <span class="glyphicon glyphicon-warning-sign squeeze"></span>';
        $out .= ' <span class="pull-right glyphicon glyphicon-question-sign" data-toggle="tooltip" data-html="true" title="'.$tooltip.'"></span>';
      $out .= '</h2>';

      if ($user->isSuperuser()) {
        $headTeacher = $pages->get("template=team, name=no-team")->teacher->first();
      }
      $allMonsters = $page->children("template=megamonster, (exerciseOwner.singleTeacher=$headTeacher), (created_users_id=$headTeacher->id)")->sort("level");
      $index = 0;
      $previousVictoriesNb = $team->hqFights->find("result>=85")->count();
      $out .= '<h2 class="text-center">'.__("Your team's progression").'</h2>';
      // Build timeline
      $out .= '<ul class="timeline highlight-active">';
        foreach($allMonsters as $m) {
          if ($index == $previousVictoriesNb) {
            $out .= '<li class="active focus" data-id="'.$m->id.'">';
              $out .= '<span class="blink">'.$m->level.'</span>';
            $out .= '</li>';
          } else {
            if ($index < $previousVictoriesNb) {
              $out .= '<li data-id="'.$m->id.'">';
              if ($m->image) { 
                $out .= '<img src="'.$m->image->getCrop("mini")->url.'" alt="no-img" />';
              } else {
                $out .= '<span class="">'.$m->level.'</span>';
              }
              $out .= '</li>';
            } else {
              $out .= '<li data-id="'.$m->id.'">'.$m->level.'</li>';
            }
          }
          $index++;
        }
      $out .= '</ul>';  

      $index = 0;
      // Build megaMonsters detailled divs
      foreach($allMonsters as $m) {
        if ($index == $previousVictoriesNb) {
          $className = '';
        } else {
          $className = 'hidden';
        }
        $out .= '<div id="mega-'.$m->id.'" class="megaMonster '.$className.'">';
          $out .= '<table>';
            $lastFight = $team->hqFights->get("megaMonster=$m, sort=-date");
            if ($index == $previousVictoriesNb) {
              $out .= '<tr>';
              $out .= '<th colspan="2" width="60%" class="text-left">';
                $out .= __("Upcoming fight");
                $out .= ' → <span class="label label-danger">'.$m->title.'</span>';
              $out .= '</th>';
              $out .= '<th class="text-left">';
                $out .= '<span class="small">';
                  $out .= '<span>'.__('Last team result →').'</span> ';
                  if ($lastFight) {
                    if ($lastFight->result >= 85) {
                      $out .= '<span class="label label-success"><span class="glyphicon glyphicon-thumbs-up"></span> '.$lastFight->result.'% !</span>';
                    } else {
                      $out .= '<span class="label label-danger"><span class="glyphicon glyphicon-thumbs-down"></span> '.$lastFight->result.'%</span>';
                    }
                  } else {
                    $out .= '<span class="label label-danger">'.__('No result !').'</span>';
                  }
                $out .= '</span>';
              $out .= '</th>';
              $out .= '</tr>';
              $out .= '<tr>';
              $out .= '<td class="text-center">';
              if ($m->image) { 
                $out .= '<img src="'.$m->image->getCrop("thumbnail")->url.'" alt="no-img" />';
              } else {
                $out .= $m->title;
              }
              $out .= '</td>';
              $out .= '<td class="text-left">';
                $tooltip = __("Click to practise on this monster");
                $out .= __("Inspired by ").$m->monsters->implode(' ', '<a class="btn btn-danger btn-sm" href="{url}" data-toggle="tooltip" title="'.$tooltip.'" target="blank">{title}</a>');
                $out .= '<br />';
                $out .= '<a class="btn btn-primary" href="'.$m->url.'" target="_blank" />'.__("Practise online !").'</a>';
              $out .= '</td>';
              $submitforms = $pages->get("name=submitforms");
              $out .= '<td>';
                if ($user->hasRole("player")) {
                  if ($player->skills->has("name=captain")) {
                    $out .= '<blockquote>'.__("Captain, make sure your groupmates are ready and then enroll your group to organize a team attack in class !").'</blockquote>';
                    if ($player->group) {
                      if ($team->enrolledGroups->has($player->group)) { $checked = 'checked="checked"'; } else { $checked = ''; }
                      $out .= ' <p>→ <label class="btn btn-sm btn-default"><input type="checkbox" class="simpleAjax" data-href="'.$submitforms->url.'?form=enroll&teamId='.$team->id.'&groupId='.$player->group->id.'" data-hide-feedback="true" '.$checked.' /> Enroll '.$player->group->title.'</label> ';
                    } else {
                      $out .= '<p>→ '.__('Ask your teacher to set groups').'</p>';
                    }
                  } else {
                    $out .= '<p>'.__('Get ready, then warn your captain so he or she can enroll your group for this upcoming fight !').'</p>';
                  }
                } else {
                  if ($user->hasRole("teacher")) {
                    if ($team->is("name=test-team")) { $allPlayers = $pages->find("parent.name=players, team=$team"); }
                    $allGroups = listGroups($allPlayers->find("team=$team, sort=group.name"));
                    $out .= '<div class="text-left">';
                    $out .= '<span class="label label-primary">'.__("Captains, enroll your group ⇒").'</span> ';
                    foreach ($allGroups as $g) {
                      if ($team->enrolledGroups->has($g)) { $checked = 'checked="checked"'; } else { $checked = ''; }
                      $out .= ' <label class="btn btn-sm btn-default"><input type="checkbox" class="simpleAjax" data-href="'.$submitforms->url.'?form=enroll&teamId='.$team->id.'&groupId='.$g->id.'" data-hide-feedback="true" '.$checked.' /> '.$g->title.'</label> ';
                    }
                    if ($team->enrolledGroups->count() == $allGroups->count()) { // All groups are enrolled
                      $out .= '<h4>→ '.__("A team attack can be organized !").'</h4>';
                    }
                    $out .= '</small>';
                  }
                }
              $out .= '</td>';
              $out .= '</tr>';
            } else {
              if ($index > $previousVictoriesNb) {
                $out .= '<tr>';
                $out .= '<td>';
                $out .= '<h3 class="text-center">'.__("Surprise !").'</h3>';
                $out .= '</td>';
                $out .= '</tr>';
              } else {
                $out .= '<tr>';
                $out .= '<td class="text-center">';
                if ($m->image) { 
                  $out .= '<img src="'.$m->image->getCrop("thumbnail")->url.'" alt="no-img" />';
                } else {
                  $out .= $m->title;
                }
                $out .= '</td>';
                $out .= '<td>';
                $out .= '<h3 class="text-left">';
                  $out .= '<span class="glyphicon glyphicon-thumbs-up"></span> '.sprintf(__('%s defeated !'), $m->title);
                  $out .= ' ['.sprintf(__('Last result : %d'), $lastFight->result).'%]';
                $out .= '</h3>';
                $out .= '<h3 class="text-left">';
                switch($m->level) {
                  case '1' : $out .= __("Europe is is free !"); break;
                  case '2' : $out .= __("North and South Americas are free !"); break;
                  case '3' : $out .= __("Asia s free !"); break;
                  case '4' : $out .= __("Oceania is free !"); break;
                  case '5' : $out .= __("Africa is free !"); break;
                  case '6' : $out .= __("North and South poles are free !"); break;
                  default : break;
                }
                $out .= '</h3>';
                $out .= '</td>';
                $out .= '</tr>';
              }
            }
          $out .= '</table>';
        $out .= '</div>';
        $index++;
      }
    } else {
      $out .= '<h2>'.__("A team name as urlSegment1 is required").'</h2>';
    }

    echo $out;
  } else {
    echo $noAuthMessage;
  }

  include("./foot.inc"); 
?>
