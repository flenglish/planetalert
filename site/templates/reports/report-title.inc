<?php

namespace ProcessWire;

if (count($allPlayersIds) == 1) { // Individual report
    foreach ($allPlayersIds as $playerId) {
        $player = $pages->get($playerId);
    }
    $out .= '<h3 class="report">';
    if ($player->avatar) {
        $out .= '<img src="'.$player->avatar->getCrop("thumbnail")->url.'" height="50" alt="No avatar" />';
    } else {
        $out .= '<Avatar>';
    }
    $out .= $reportTitle;
    $out .= '</h3>';
} else {
    $out .= '<h3 class="report">'.$reportTitle.'</h3>';
}
