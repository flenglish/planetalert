<?php

namespace ProcessWire;

if (!$config->ajax) {
    include("./head.inc");
}
$out = '';
$error = '';
$feedback = false;
$numSent = false;
$adminMail = $users->get("name=admin")->email;
if ($user->hasRole('player')) {
    $player = $pages->get("parent.name=players, login=$user->name");
    $headTeacher = getHeadTeacher($user);
    if ($headTeacher && $headTeacher->email != '') {
        $emailTo = $headTeacher->email;
    } else {
        $emailTo = $adminMail;
    }
} else {
    $emailTo = $adminMail;
}

if ($input->post->submit) { // Form was submitted
    $form = array( // Sanitize form values or create empty
      'fullname' => $sanitizer->text($input->post->fullname),
      'playerClass' => $sanitizer->text($input->post->playerClass),
      'email' => $sanitizer->email($input->post->email),
      'subject' => $sanitizer->text($input->post->subject),
      'message' => $sanitizer->textarea($input->post->message, ['allowCRLF' => true])
    );
    // Form treatment
    $antispam_code =  $sanitizer->int($input->post->antispam_code);
    if ($user->isLoggedin() || ($session->get('antispam_code') == $antispam_code)) {
        $session->remove('antispam_code');
        foreach ($form as $key => $value) { // Determine if any fields were ommitted or didn't validate
            if ($key != 'email' && $key != 'playerClass') {
                if (empty($value)) {
                    $error = '<h2 class="text-center"><span class="label label-danger">'.__("An error occurred. Please check that all fields marked with * have been completed.").'</span></h2>';
                }
            }
            if ($key == 'subject') { // Adapt subject
                switch ($value) {
                    case 'extra-hk':
                        $subject = __("Extra-homework");
                        break;
                    case 'question':
                        $subject = __("Question");
                        break;
                    case 'bug':
                        $subject = __("Bug");
                        break;
                    case 'idea':
                        $subject = __("Idea");
                        break;
                    case 'contact':
                        $subject = __("Guest contact");
                        break;
                    default:
                        $subject = __("Other");
                }
                $subject .= ' ['.$form['fullname'];
                if ($form['playerClass'] != '') {
                    $subject .= ' - '.$form['playerClass'];
                }
                $subject .= ']';
            }
        }
    } else {
        $error = '<h2 class="text-center"><span class="label label-danger">'.__("An error occurred. Please check that all fields have been completed.").'</span></h2>';
    }

    if ($error == '') { // No errors, email the form results
        $message = $mail->new();
        $message->to($emailTo, "Planet Alert");
        $message->from($adminMail);
        $message->fromName($form['fullname']);
        $message->subject($subject);
        $message->body($form['message']."\r\nReply to : ".$form['email']);
        $numSent = $message->send();
        if ($numSent == 1) {
            $feedback = '<h2 class="text-center">'.__("Thank you! Your message has been sent.").'</h2>';
        } else {
            $numSent = false;
            $feedback = '<h2 class="text-center"><span class="label label-danger">'.__("An error occured ! Your message may have not been sent.").'</span></h2>';
        }
        foreach ($form as $key => $value) {
            $form[$key] = htmlentities($value, ENT_QUOTES, "UTF-8");
        }
    }
} else {
    $form = array( // Init values
      'fullname' => '',
      'playerClass' => '',
      'email' => '',
      'subject' => '',
      'message' => '',
    );
    // Basic anti-spam (for guests)
    $sendFormInteger = mt_rand(1000, 9999);
    $session->set('antispam_code', $sendFormInteger);
}

if ($error != '' || $feedback != false) {
    $out = $error;
    $out .= $feedback;
}
if ($user->isLoggedin() && $user->hasRole('player')) {
    $out .= '<section class="row">';
    $out .= '<h2>';
    $out .= '<p class="text-center">'.__('Contact my teacher').'</p>';
    $out .= '<span class="pull-right glyphicon glyphicon-question-sign" data-toggle="tooltip" title="'.__('Edit this pad,  share with your teacher and you\'ll get feedback !').'"></span>';
    $out .= '<span class="button-group" role="group">';
    if ($headTeacher->name == 'flieutaud') {
        $teacherPage = $pages->get("parent.name=teachers, template=teacherProfile, name=$headTeacher->name");
        if ($teacherPage->filesDownloadUrl != '') {
            $out .= '<a role="button" class="btn btn-default" href="'.$teacherPage->filesDownloadUrl.'" target="_blank" data-toggle="tooltip" title="'.__("Download a document from your teacher").'"><span class="glyphicon glyphicon-cloud-download"></span></a>';
        }
        if ($teacherPage->filesUploadUrl != '') {
            $out .= '<a role="button" class="btn btn-default" href="'.$teacherPage->filesUploadUrl.'" target="_blank" data-toggle="tooltip" title="'.__("Upload a document for your teacher").'"><span class="glyphicon glyphicon-cloud-upload"></span></a>';
        }
    }
    $out .= '<a role="button" class="btn btn-default" href="https://www.lelivrescolaire.fr/labo-langues" target="_blank" data-toggle="tooltip" title="'.__("Record an audio message for my teacher").'"><span class="glyphicon glyphicon-volume-up"></span></a>';
    $out .= '</span>';
    $out .= '</h2>';
    if (count($player->pads) == 0) {
        $newPad = $player->pads->getNew();
        $newPad->of(false);
        $newPad->save();
        $player->of(false);
        $player->save();
    }
    foreach ($player->pads as $pad) {
        $editLink = $pad->feel(array(
          'mode' => 'page-edit',
          'fields' => 'pad,alertBox',
          // 'data-ajax-target' => '#myPad-'.$pad->id // Commented out to reload page
        ));
        $out .= '<div id="" class="panel panel-info">';
        $out .= '<div class="panel-heading">';
        $out .= '<h5 class="panel-title">';
        $out .= '<span class="label label-primary">'.__('Personal pad').'</span> ';
        $out .= $editLink.__(" (You can double click on the pad below to start editing)");
        $out .= '</h5> ';
        $out .= '</div>';
        $out .= '<div class="panel-body">';
        if ($pad->pad == '') {
            $pad->pad = '<br /><br /><br />';
        }
        $out .= '<div id="myPad-'.$pad->id.'" class="pad">';
        $out .= $pad->edit('pad');
        $out .= '</div>';
        $out .= '</div>';
        $out .= '<div class="panel-footer">';
        $submitforms = $pages->get("name=submitforms");
        $pad->alertBox == 1 ? $status = 'checked="checked"' : $status = '';
        $out .= '<input type="checkbox" id="alertBox" name="alertBox" data-href="'.$submitforms->url.'?form=forceOption&pageId='.$pad->id.'&optionName=alertBox" class="simpleAjax" data-hide-feedback="true" '.$status.' />';
        $out .= ' <label for="alertBox">'.__("Share with my teacher for feedback !").'</label>';
        $out .= ' <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" title="'.__("If ticked, your pad is placed on your teacher's Newsboard, waiting for feedback.").'"></span>';
        $out .= '<button id="cleanPad" data-href="'.$submitforms->url.'?form=cleanPad&pageId='.$pad->id.'" class="confirm btn btn-danger btn-xs pull-right" data-hide-feedback="true">'.__('Clean my pad').'</button> ';
        $out .= '</div>';
        $out .= '</div>';
        $out .= '</div>';
        $out .= '</section>';
    }
} else {
    if (!$input->post->submit || $error != '') { // Form was not submitted or failed
        $out .= '<section class="row text-center">';
        if ($user->isLoggedin()) {
            $out .= '<span class="pull-left">';
            $teacherPage = $pages->get("parent.name=teachers, template=teacherProfile, name=$headTeacher->name");
            if ($teacherPage->filesDownloadUrl != '') {
                $out .= '<a role="button" class="btn btn-default" href="'.$teacherPage->filesDownloadUrl.'" target="_blank" data-toggle="tooltip" title="'.__("Download a document from your teacher").'"><span class="glyphicon glyphicon-cloud-download"></span></a>';
            }
            if ($teacherPage->filesUploadUrl != '') {
                $out .= '<a role="button" class="btn btn-default" href="'.$teacherPage->filesUploadUrl.'" target="_blank" data-toggle="tooltip" title="'.__("Upload a document for your teacher").'"><span class="glyphicon glyphicon-cloud-upload"></span></a>';
            }
            $out .= '<a role="button" class="btn btn-default" href="https://www.lelivrescolaire.fr/labo-langues" target="_blank" data-toggle="tooltip" title="'.__("Record an audio message for my teacher").'"><span class="glyphicon glyphicon-volume-up"></span></a>';
            $out .= '</span>';
        }
        if ($user->isGuest() || $user->hasRole('teacher')) {
            $out .= '<h1 class="text-center">'.__("Contact Planet Alert admin !").'</h1>';
        } else {
            $out .= '<h2 class="text-center">'.__("Contact my teacher !").'</h2>';
        }
        $out .= '<p><em>'.__("Please watch out your spelling and DO NOT use SMS syntax ;)").'</em>';
        $out .= __("(* Fields MUST be filled !)").'</p>';
        $out .= '<form id="contactForm" role="form" class="form-horizontal" action="'.$page->url.'" method="post">';
        if ($user->hasRole('player')) {
            $out .= '<div class="form-group">';
            $out .= '<a class="btn btn-default" href="https://cloud.ac-nancy-metz.fr/nextcloud-sso/index.php/s/ZBtis74BgEd2zQt" target="_blank" data-toggle="tooltip" title="'.__("Download a document from your teacher").'"><span class="glyphicon glyphicon-cloud-download"></span></a>';
            $out .= '<label for="subject" class="col-sm-2 control-label">'.__("Reason for your message").' *</label>';
            $out .= '<div class="col-sm-8">';
            $out .= '<select class="form-control" id="subject" name="subject">';
            $out .= '<option value="extra-hk">'.__("I did an extra-training !").'</option>';
            $out .= '<option value="question">'.__("I have a question.").'</option>';
            $out .= '<option value="bug">'.__("I've found a bug !").'</option>';
            $out .= '<option value="idea">'.__("I have an idea for the class !").'</option>';
            $out .= '<option value="other">'.__("I have something to tell you.").'</option>';
            $out .= '</select>';
            $out .= '</div>';
            $out .= '</div>';
            $out .= '<input type="hidden" id="fullname" name="fullname" value="'.$player->title.' '.$player->lastName.'" />';
            $out .= '<input type="hidden" id="playerClass" name="playerClass" value="'.$player->team->title.'" />';
        } else {
            $out .= '<input type="hidden" id="subject" name="subject" value="contact" />';
            $out .= '<div class="form-group">';
            $out .= '<label for="fullname" class="col-sm-2 control-label">'.__("First AND last name").' *</label>';
            $out .= '<div class="col-sm-8">';
            $out .= '<input type="text" class="form-control" id="fullname" name="fullname" value="'.$form['fullname'].'" placeholder="'.__("First AND last name").'" required="required" />';
            $out .= '</div>';
            $out .= '</div>';
            if (!$user->hasRole('teacher')) {
                $out .= '<div class="form-group">';
                $out .= '<label for="playerClass" class="col-sm-2 control-label">'.__("Class").'</label>';
                $out .= '<div class="col-sm-8">';
                $out .= '<input type="text" class="form-control" id="playerClass" name="playerClass" value="'.$form['playerClass'].'" placeholder="'.__("Class").'" />';
                $out .= '</div>';
                $out .= '</div>';
            }
            $out .= '<div class="form-group">';
            $out .= '<label for="email" class="col-sm-2 control-label">'.__("Your email address (if you expect a reply)").'</label>';
            $out .= '<div class="col-sm-8">';
            $out .= '<input type="email" class="form-control" name="email" id="email" value="'.$form['email'].'" placeholder="'.__("Your email address").'" />';
            $out .= '</div>';
            $out .= '</div>';
        }
        $out .= '<div class="form-group">';
        $out .= '<label for="message" class="col-sm-2 control-label">'.__("Your message").' *</label>';
        $out .= '<div class="col-sm-8">';
        $out .= '<textarea id="message" class="form-control" name="message" rows="11" required="required">'.$form['message'].'</textarea>';
        if ($user->isLoggedin()) {
            $out .= '<a class="btn btn-default" href="https://www.lelivrescolaire.fr/labo-langues" target="_blank" data-toggle="tooltip" title="'.__("Record an audio message for my teacher").'"><span class="glyphicon glyphicon-volume-up"></span></a> <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" title="'.__("Opens a webpage where you can click on the microphone and record yourself. Then, click on share and generate a URL. Copy the URL and paste it inside your message above. Send it to your teacher ! [Thank you to lelivrescolaire.fr !]").'"></span>';
            $out .= '&nbsp;&nbsp;&nbsp;';
            if ($teacherPage->filesUploadUrl != '') {
                $out .= '<a role="button" class="btn btn-default" href="'.$teacherPage->filesUploadUrl.'" target="_blank" data-toggle="tooltip" title="'.__("Upload a document for your teacher").'"><span class="glyphicon glyphicon-cloud-upload"></span></a>';
            }
        }
        $out .= '</div>';
        $out .= '</div>';
        if ($user->isGuest()) {
            $out .= '<div class="form-group">';
            $out .= '<label for="antispam_code" class="col-sm-2 control-label">Anti spam code* → <strong>'.$session->get('antispam_code').'</strong></label>';
            $out .= '<div class="col-sm-8">';
            $out .= '<input type="text" name="antispam_code" value="" class="form-control" placeholder="'.__('Copy anti-spam code here').'" required="required" />';
            $out .= '</div>';
            $out .= '</div>';
        }
        $out .= '<div class="col-sm-offset-2 col-sm-8">';
        $out .= '<input id="contactFormSubmit" type="submit" name="submit" class="btn btn-block btn-primary" value="'.__("Send message").'" />';
        $out .= '</div>';
        $out .= '</form>';
        $out .= '</section>';
    }
}
echo $out;

if (!$config->ajax) {
    include("./foot.inc");
}
