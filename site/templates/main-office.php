<?php

namespace ProcessWire;

include("./head.inc");

if (isset($player) && $player->team->is("name=test-team")) {
    $selectedTeam = $pages->get("name=test-team");
}
$rank = $selectedTeam->rank->index;
if ($user->isLoggedin()) {
    $allPlayers = getAllPlayers($user, false);
    if ($user->hasRole('teacher') || $user->isSuperuser()) {
        include("./tabList.inc");
        $pickFromList = 'pickFromList';
    } else {
        $pickFromList = '';
    }
    $out = '';
    // Decisions menu (via ajax)
    $ajaxContentUrl = $pages->get("name=ajax-content")->url;
    $out .= '<div id="ajaxDecision" data-href="'.$ajaxContentUrl.'" data-id="decision"></div>';
    $out .= '<div id="showInfo" data-href="'.$ajaxContentUrl.'"></div>';

    $out .= getScoresSummaries($headTeacher);

    if ($user->hasRole('teacher') || $user->isSuperuser()) {
        $cacheName = 'cache__'.$input->urlSegment1.'-office-teacher-'.$user->language->name;
    }
    if ($user->hasRole('player') || $user->isGuest()) {
        $cacheName = 'cache__'.$input->urlSegment1.'-office-player-'.$headTeacher->language->name;
    }
    $cachedOffice = $cache->get($cacheName, 0, function ($user, $pages, $page) use ($selectedTeam, $allPlayers, $pickFromList, $rank, $config) {
        $out = '';
        if ($allPlayers->count() > 0) {
            $dangerPlayers = $allPlayers->find("coma=1");
            $lowPlayers = $allPlayers->find("HP<=15");
            $dangerPlayers->add($lowPlayers);
            unset($lowPlayers);
            $out .= '<div class="col-sm-4">';
            // Help needed
            $out .= '<div id="" class="board panel panel-primary isFold">';
            $out .= '<div class="panel-heading">';
            $out .= '<h4 class="panel-title">';
            $out .= __("Help needed !").' ';
            $out .= __("Major alerts !");
            $out .= '</h4>';
            $out .= '</div>';
            $out .= '<div class="panel-body">';
            if ($dangerPlayers->count() != 0) {
                $healingPotion = $pages->get("name=health-potion");
                $out .= '<ul class="list list-unstyled list-inline text-center">';
                foreach ($dangerPlayers as $p) {
                    if ($p->coma == 1) {
                        $label = 'Coma';
                    } else {
                        $label = $p->HP.'HP';
                    }
                    $out .= '<li>';
                    $threshold = getLevelThreshold($p->level);
                    $out .= '<div class="thumbnail text-center" title="XP → '.$p->XP.'/'.$threshold.'" data-toggle="tooltip">';
                    if ($p->avatar) {
                        $out .= '<img class="" src="'.$p->avatar->getCrop("thumbnail")->url.'" width="50" alt="'.$p->title.'." />';
                    } else {
                        $out .= '<Avatar>';
                    }
                    $out .= '<caption class="text-center">';
                    $out .= $p->title;
                    $out .= ' <span class="badge">'.$label.'</span><br />';
                    if ($user->isSuperuser() || $user->hasRole('teacher')) {
                        $out .= '<span class="help-'.$p->id.' badge">'.sprintf(__('%d GC'), $p->GC).'</span>';
                        if ($p->GC >= $healingPotion->GC) {
                            $out .= ' <a href="#" class="btn btn-xs btn-link buyBtn" data-type="heal" data-url="'.$pages->get('name=submitforms')->url.'?form=buyForm&playerId='.$p->id.'&itemId='.$healingPotion->id.'">→ '.__("Heal ?").'</a>';
                        } else {
                            $out .= ' <a href="#" class="help-'.$p->id.' btn btn-xs btn-link ajaxBtn" data-id="'.$p->id.'" data-type="help" data-url="">→ '.__("Help ?").'</a>';
                            $out .= ' <a href="#" id="heal-'.$p->id.'" class="hidden btn btn-xs btn-link buyBtn" data-type="heal" data-url="'.$pages->get('name=submitforms')->url.'?form=buyForm&playerId='.$p->id.'&itemId='.$healingPotion->id.'">→ '.__("Heal ?").'</a>';
                        }
                    }
                    $out .= '</caption>';
                    $out .= '</div>';
                    $out .= '</li>';
                }
                $out .= '<ul>';
            } else {
                $out .= '<p>'.__("Congratulations ! No player with HP<15 !").'</p>';
            }
            $unEquippedPlayers = $pages->find("parent.name=players, team=$selectedTeam")->not("equipment.name=memory-helmet");
            if ($unEquippedPlayers->count() > 0) {
                $unEquippedPlayersList = $unEquippedPlayers->implode(', ', '{title} ({GC}GC)');
                $out .= '<hr />';
                $out .= '▶ '.__('No Memory Helmet for : ').$unEquippedPlayersList.' → ';
                $out .= "<a target='_blank' href='{$pages->get("name=makedonation")->url}{$selectedTeam->name}'>".__("Organize donations")."</a>";
            }
            $out .= '</div>';
            $out .= '<div class="panel-footer text-right">';
            $out .= '<span>';
            $out .= '<img src="'.$config->urls->templates.'img/gold_mini.png" alt="chest" /> ';
            $out .= __("Team chest : ");
            $out .= sprintf(__("%d GC"), $selectedTeam->GC);
            $out .= '</span>';
            if ($dangerPlayers->count() != 0 && $healingPotion->id) {
                $out .= ' / '.sprintf(__('Healing potion costs %d GC'), $healingPotion->GC);
            }
            $out .= '</div>';
            $out .= '</div>';

            // Groups
            if ($allPlayers->count() > 0 && $selectedTeam->name != 'no-team') {
                $allGroups = groupScores($selectedTeam);
                if (isset($allGroups)) {
                    $groupList = $allGroups->implode(', ', '{id}');
                    if ($groupList && ($user->isSuperuser() || $user->hasRole('teacher'))) {
                        $pickButton = ' <a class="btn btn-danger btn-xs pickFromList pull-right" data-list="'.$groupList.'" data-team="'.$selectedTeam.'">Pick 1!</a>';
                    } else {
                        $pickButton = '';
                    }
                } else {
                    $groupList = false;
                    $pickButton = '';
                }
                $out .= '<div id="" class="board panel panel-primary isFold">';
                $out .= '<div class="panel-heading">';
                $out .= '<h4 class="panel-title">';
                $out .= __("Most active groups").$pickButton;
                $out .= '</h4>';
                $out .= '</div>';
                $out .= '<div class="panel-body">';
                if ($groupList) {
                    $out .= '<ol class="">';
                    foreach ($allGroups as $group) {
                        $out .= '<li>';
                        $out .= '<p>';
                        if ($user->isSuperuser() || $user->hasRole('teacher')) {
                            $out .= '<span data-toggle="tooltip" data-html="true" title="'.$group->members.'">';
                        } else {
                            $out .= '<span data-toggle="tooltip" data-html="true" title="'.$group->members.'">';
                        }
                        if (isset($player) && $player->group == $group) {
                            $focus = 'focus';
                        } else {
                            $focus = '';
                        }
                        $out .= '<span class="'.$focus.' pickFromList" data-list="'.$group->id.'" data-team="'.$selectedTeam.'">'.$group->title.'</span>';
                        $out .= ' <span class="badge">'.$group->GC.__('GC').'</span>';
                        $out .= ' <span class="badge">'.$group->karma.' K</span>';
                        // Display stars for bonus (filled star = 5 empty stars, 1 star = 1 free element for each group member)
                        if ($group->nbBonus > 0) {
                            $out .= '&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-star"></span>';
                            $out .= '<span class="badge">'.$group->nbBonus.'</span>';
                        }
                        $out .= '</span>';
                        $out .= '</p>';
                        $out .= '</li>';
                    }
                    unset($allGroups);
                    $out .= '</ol>';
                } else {
                    $out .= '<p>'.__("The groups aren't ready yet.").'</p>';
                }
                $out .= '</div>';
                $out .= '</div>';

                // Team info (Fight requests, Masters...)
                $out .= '<div id="" class="board panel panel-primary isFold">';
                $out .= '<div class="panel-heading">';
                $out .= '<h4 class="panel-title">';
                $out .= __("Team info");
                $out .= '<a class="btn btn-xs btn-danger pull-right" href="'.$pages->get("name=pads")->url.'/hasContent/'.$selectedTeam->name.'" target="_blank" onclick="event.stopPropagation();"><span class="glyphicon glyphicon-edit"></span> '.__('[Team pads]').'</a>';
                $out .= '</h4>';
                $out .= '</div>';
                $out .= '<div class="panel-body">';
                $out .= '<h4>';
                $out .= '<span class="label label-primary">'.__("Fight requests").'</span>';
                $out .= '</h4>';
                $requests = $allPlayers->find("fight_request!=''");
                if ($requests->count() > 0) {
                    $out .= '<ul>';
                    foreach ($requests as $r) {
                        $out .= '<li>';
                        if (isset($player)) {
                            $focus = 'focus';
                        } else {
                            $focus = '';
                        }
                        $out .= '<span class="'.$focus.'">'.$r->title.'</span>';
                        $out .= ' <span class="badge">'.$pages->get($r->fight_request)->title.'</span>';
                        $out .= '</li>';
                    }
                    $out .= '</ul>';
                } else {
                    $out .= '<p>'.__("No pending fight requests").'</p>';
                }
                $schoolYear = $pages->get("template=period, name=school-year");
                $allFightRequests = $pages->find("has_parent=$allPlayers, template=event, task.name^=fight, summary*=request, inClass=1, date>$schoolYear->dateStart");
                $noFightRequestsNb = $allPlayers->count() - $allFightRequests->count();
                $out .= '<h5 class="text-right">';
                $out .= __('Total Fight requests').' → ';
                $out .= '<span class="label label-primary">';
                $out .= $allFightRequests->count();
                $out .= '</span>';
                $out .= ' <i class="glyphicon glyphicon-info-sign showPopup" data-id="#popup-requests-'.$page->id.'"></i>';
                $out .= '</h5>';
                if ($allFightRequests->count() > 0) {
                    $out .= '<ul class="list col3">';
                    $allPlayers->sort("name");
                    foreach ($allPlayers as $p) {
                        $fightRequestsNb = count($allFightRequests->find("has_parent=$p"));
                        if ($fightRequestsNb > 0) {
                            $out .= '<li>';
                            $out .= $p->title.':'.$fightRequestsNb;
                            $out .= '</li>';
                        }
                    }
                    $out .= '</ul>';
                    if ($noFightRequestsNb > 0) {
                        $out .= '<p class="text-right">'.__('Not concerned : ').$noFightRequestsNb.'</p>';
                    }
                    $allRequests = [];
                    foreach ($allFightRequests as $f) {
                        if (!array_key_exists($f->refPage->title, $allRequests)) {
                            $allRequests[$f->refPage->title] = 1;
                        } else {
                            $allRequests[$f->refPage->title]++;
                        }
                    }
                    // Fight Requests details accessed by clicking on total counter
                    arsort($allRequests);
                    // ksort($allRequests);
                    $out .= '<div id="popup-requests-'.$page->id.'" class="hidden">';
                    $out .= '<h4>';
                    $out .= __("Fight requests details and popularity");
                    $out .= '</h4>';
                    $out .= '<ul class="list-unstyled col5">';
                    foreach ($allRequests as $name => $nb) {
                        $out .= '<li>'.$name.' : '.$nb.'</li>';
                    }
                    $out .= '</ul>';
                    $out .= '</div>';
                }
                unset($requests);
                // Pending potions
                $unusedConcerned = $allPlayers->find("usabledItems.count>0")->sort("name");
                $out .= '<h4><span class="label label-primary">'.__("Pending potions").'</span></h4>';
                if ($unusedConcerned->count() > 0) {
                    $today = new \DateTime("today");
                    $out .= '<ul class="list-unstyled">';
                    foreach ($unusedConcerned as $p) {
                        foreach ($p->usabledItems as $item) {
                            $historyPage = $p->get("name=history")->find("refPage=$item")->last();
                            if ($historyPage) {
                                $out .= '<li>';
                                // Find # of days compared to today
                                $date2 = new \DateTime(date("Y-m-d", $historyPage->date));
                                $interval = $today->diff($date2);
                                $expireWarning = 21; // 3 weeks delay to use a potion (hence 6 weeks before blinking)
                                if ($interval->days > $expireWarning) {
                                    if ($interval->days > $expireWarning * 2) {
                                        $className = "blink";
                                    } else {
                                        $className = "";
                                    }
                                    $out .= ' <span class="badge '.$className.'">!</span> ';
                                }
                                $out .= '<span>'.$p->title.' : '.$historyPage->summary.' (bought '.$interval->days.' days ago)</span>';
                                $out .= '</li>';
                            }
                        }
                    }
                    $out .= '</ul>';
                } else {
                    $out .= '<p>'.__("No pending potions").'</p>';
                }
                unset($unusedConcerned);
                $out .= '<h4><span class="label label-primary">'.__("Masters").'</span></h4>';
                $masters = $allPlayers->find("skills.name=master");
                if ($masters->count() > 0) {
                    $out .= '<ul>';
                    foreach ($masters as $m) {
                        $bestTimeMonsters = $pages->find("parent.name=monsters, template=exercise, bestTimePlayerId=$m->id")->sort("title");
                        if ($bestTimeMonsters->count() > 0) {
                            $bestTimeMonstersList = '<ul>';
                            foreach ($bestTimeMonsters as $e) {
                                $bestTimeMonstersList .= '<li>'.$e->title.' ['.ms2string($e->bestTime).']</li>';
                            }
                            $bestTimeMonstersList .= '</ul>';
                            $out .= '<li>';
                            if (isset($player)) {
                                $focus = 'focus';
                            } else {
                                $focus = '';
                            }
                            $out .= '<span class="'.$focus.'">'.$m->title.'</span>';
                            $out .= ' <span class="badge">'.$bestTimeMonsters->count().' '._n("Master time", "Master times", $bestTimeMonsters->count()).'</span>';
                            $out .= ' : '.$bestTimeMonstersList;
                            $out .= '</li>';
                        }
                    }
                    $out .= '</ul>';
                } else {
                    $out .= '<p>'.__("No Master in the team").'</p>';
                }
                unset($Masters);
                $out .= '<h4><span class="label label-primary">'.__("Top trained").'</span></h4>';
                $topTrained = $allPlayers->find("skills.name=top-trained");
                if ($topTrained->count() > 0) {
                    $out .= '<ul>';
                    foreach ($topTrained as $m) {
                        $bestTrainedMonsters = $pages->find("parent.name=monsters, template=exercise, bestTrainedPlayerId=$m->id");
                        if ($bestTrainedMonsters->count() > 0) {
                            $bestTrainedMonstersList = $bestTrainedMonsters->implode(', ', '{title} ({best}UT)');
                            $bestTrainedMonstersList = '<ul>';
                            foreach ($bestTrainedMonsters as $e) {
                                $bestTrainedMonstersList .= '<li>'.$e->title.' ['.$e->best.__('UT').']</li>';
                            }
                            $bestTrainedMonstersList .= '</ul>';
                            $out .= '<li>';
                            if (isset($player)) {
                                $focus = 'focus';
                            } else {
                                $focus = '';
                            }
                            $out .= '<span class="'.$focus.'">'.$m->title.'</span>';
                            $out .= ' <span class="badge">'.$bestTrainedMonsters->count().' '._n("best UT", "best UT", $bestTrainedMonsters->count()).'</span>';
                            $out .= ' : '.$bestTrainedMonstersList;
                            $out .= '</li>';
                        }
                    }
                    $out .= '</ul>';
                } else {
                    $out .= '<p>'.__("No top trained players in the team").'</p>';
                }
                unset($topTrained);
                $out .= '</div>';
                $out .= '</div>';
            }

            // Most influential players
            $top = $allPlayers->sort("-yearlyKarma")->find("limit=3, yearlyKarma>0");
            if ($top->count() != 0) {
                $topList = $top->implode(', ', '{id}');
                if ($user->isSuperuser() || $user->hasRole('teacher')) {
                    $pickButton = ' <a class="btn btn-danger btn-xs pickFromList pull-right" data-list="'.$topList.'">'.__("Pick 1!").'</a>';
                } else {
                    $pickButton = '';
                }
                $out .= '<div id="" class="board panel panel-primary isFold">';
                $out .= '<div class="panel-heading">';
                $out .= '<h4 class="panel-title">';
                $out .= __("Most influential players !").$pickButton;
                $out .= '</h4>';
                $out .= '</div>';
                $out .= '<div class="panel-body text-center">';
                $out .= '<ul class="list-unstyled list-inline text-center">';
                foreach ($top as $p) {
                    $out .= '<li>';
                    $out .= '<div class="thumbnail text-center">';
                    if ($p->avatar) {
                        $out .= '<img class="'.$pickFromList.'" data-list="'.$p->id.'" src="'.$p->avatar->getCrop("thumbnail")->url.'" alt="'.$p->title.'." />';
                    } else {
                        $out .= '<Avatar>';
                    }
                    $out .= '<caption class="text-center">'.$p->title.' <span class="badge">'.$p->yearlyKarma.' Reputation</span></caption>';
                    $out .= '</div>';
                    $out .= '</li>';
                }
                $out .= '</ul>';
                $out .= '</div>';
                $out .= '<div class="panel-footer text-center">';
                /* $teamPlayersList = $allPlayers->implode(', ', '{id}'); */
                /* if ($user->isSuperuser() || $user->hasRole('teacher')) { */
                /*   $out .= ' <a id="pickTeamPlayer" class="btn btn-danger btn-xs pickFromList" data-list="'.$teamPlayersList.'">'.__("Pick a player in the whole team !").'</a>'; */
                /* } */
                $out .= '</div>';
                $out .= '</div>';
            }
            $out .= '</div>';

            $out .= '<div class="col-sm-8">';
            // Team News (Free/Buy actions during last 5 days)
            $news = new PageArray();
            $today = new \DateTime("today");
            $interval = new \DateInterval('P5D');
            $limitDate = strtotime($today->sub($interval)->format('Y-m-d'));
            $today = new \DateTime("today");
            $teamRecentUtPlayers = new PageArray();
            $teamRecentFightPlayers = new PageArray();
            $news = $pages->find("has_parent=$allPlayers, parent.name=history, template=event, date>=$limitDate, task.name~=free|buy|ut-action|fight|extra-homework|best-time|donation|donated|country, inClass=0, sort=-date");
            $already = '';
            $totalUt = 0;
            if ($news->count() > 0) {
                $news->sort("-date");
                $teamRecentUt = $news->find("task.name^=ut-action");
                foreach ($teamRecentUt as $n) {
                    $currentPlayer = $n->parent("template=player");
                    $teamRecentUtPlayers->add($currentPlayer);
                    preg_match("/\[\+([\d]+)[UE]\.?[TS]\.?\]/", $n->summary, $matches);
                    if (!$matches) {
                        $totalUt++;
                    } else {
                        $totalUt = $totalUt + $matches[1];
                    }
                }
                $utPlayersList = $teamRecentUtPlayers->implode(' ', '<span class="newsTags">{title}</span>');
            }
            if ($user->isSuperuser() || $user->hasRole('teacher')) {
                $allPlayersList = $allPlayers->implode(', ', '{id}');
                $pickButton = ' <a class="btn btn-danger btn-xs '.$pickFromList.' pull-right" data-list="'.$allPlayersList.'">'.__("Pick 1 from team").'</a>';
            } else {
                $pickButton = '';
            }
            $out .= '<div id="" class="board panel panel-primary isFold">';
            $out .= '<div class="panel-heading">';
            $out .= '<h4 class="panel-title">';
            $out .= __("Team News (during the last 5 days)");
            $out .= $pickButton;
            $out .= '</h4>';
            $out .= '</div>';
            $out .= '<div class="panel-body">';
            $out .= '<h4>';
            if (isset($teamRecentUt) && $teamRecentUt->count() > 0) {
                $out .= '<span class="wrapped"><i class="glyphicon glyphicon-headphones"></i> <span class="label label-primary">'.$totalUt.' '.__('UT').'</span> → '.$utPlayersList.' </span>';
            } else {
                $out .= '<span>'.__("No recent UT training :(").'</span>';
            }
            $out .= '</h4>';
            if ($news->count() != 0) {
                $out .= '<ul id="newsList" class="list-unstyled list-inline text-center">';
                $alreadyThere = new PageArray();
                foreach ($news as $n) {
                    if (($n->refPage && !$alreadyThere->has($n->refPage)) && $n->task->is("name!=donated|donation") && $n->task->is("!name^=ut-action")) {
                        $out .= '<li class="teamNews">';
                        $out .= '<div class="panel panel-danger board">';
                        $currentPlayer = $n->parent('template=player');
                        $out_02 = '<p class="panel-title">';
                        $out_02 .= '<span class="newsTags">'.$currentPlayer->title.'</span>';
                        if ($n->date <= $today->getTimeStamp()) {
                            $out_02 .= ' <small class="">('.sprintf(__('On %s'), date("D.", $n->date)).')</small>';
                        } else {
                            $out_02 .= ' <small>('.__('Today').')</small>';
                        }
                        $out_02 .= '</p>';
                        if ($n->task->is("name^=fight")) {
                            $out .= '<div class="panel-heading">';
                            $out .= '<p class="panel-title">';
                            $out .= '<span class=""><i class="glyphicon glyphicon-flash"></i> '.$n->refPage->title.'</span>';
                            // Show result
                            if ($n->task->is("name=fight-vv")) {
                                $out .= ' <span class="label label-success">VV</span>';
                            }
                            if ($n->task->is("name=fight-v")) {
                                $out .= ' <span class="label label-success">V</span>';
                            }
                            if ($n->task->is("name=fight-r")) {
                                $out .= ' <span class="label label-danger">R</span>';
                            }
                            if ($n->task->is("name=fight-rr")) {
                                $out .= ' <span class="label label-danger">RR</span>';
                            }
                            $out .= '</p>';
                            $out .= '</div>';
                            $out .= '<div class="panel-body">';
                            if ($n->refPage->image) {
                                $out .= '<img class="showInfo" data-id="'.$n->refPage->id.'" data-playerId="'.$currentPlayer->id.'" src="'.$n->refPage->image->getCrop("thumbnail")->url.'" alt="No image" />';
                            } else {
                                $out .= '<p>'.__('No photo').'</p>';
                            }
                            $out .= '</div>';
                            $out .= '<div class="panel-footer">';
                            $out .= $out_02;
                            $out .= '</div>';
                            continue;
                        }
                        if ($n->task->is("name=free")) {
                            $n->refPage = setElement($n->refPage, $currentPlayer->team);
                            $out .= '<div class="panel-heading">';
                            $out .= ' <p class="panel-title">'.__('Free rate').' → ['.$n->refPage->ownersNb.'/'.$n->refPage->teamRate.']</p>';
                            $out .= '</div>';
                            $out .= '<div class="panel-body">';
                            if ($n->refPage->photo) {
                                $out .= '<img class="showPopup" data-id="#popup-'.$n->refPage->id.'" src="'.$n->refPage->photo->eq(0)->getCrop("thumbnail")->url.'" alt="Photo" />';
                            } else {
                                $out .= '<p>'.__('No photo').'</p>';
                            }
                            $out .= '</div>';
                            $out .= '<div class="panel-footer">';
                            $out .= $out_02;
                            /* $out .= ' <h5><span class="">['.$n->refPage->ownersNb.'/'.$n->refPage->teamRate.']</span></h5>'; */
                            $out .= '</div>';
                            if (!stristr($already, (string) $n->refPage->id)) {
                                $already .= $n->refPage->id;
                                $cacheName = 'popup-'.$n->refPage->id;
                                $out .= createPopup($n->refPage);
                            }
                            continue;
                        }
                        if ($n->task->is("name=buy")) {
                            $out .= '<div class="panel-body">';
                            if ($n->refPage->image) {
                                $out .= '<img class="showPopup" data-id="#popup-'.$n->refPage->id.'" src="'.$n->refPage->image->getCrop("thumbnail")->url.'" alt="Photo" max-width="100" />';
                            } else {
                                $out .= '<p>'.__('No photo').'</p>';
                            }
                            $out .= '</div>';
                            $out .= '<div class="panel-footer">';
                            $out .= $out_02;
                            $out .= '</div>';
                            if (!stristr($already, (string) $n->refPage->id)) {
                                $already .= $n->refPage->id;
                                $out .= createPopup($n->refPage);
                            }
                            continue;
                        }
                        if ($n->task->is("name=best-time")) {
                            $out .= '<div class="panel-heading">';
                            $out .= '<p class="panel-title">';
                            $out .= '<i class="glyphicon glyphicon-time"></i> '.sprintf(__('Best time on %s'), $n->refPage->title);
                            $out .= '</p>';
                            $out .= '</div>';
                            $out .= '<div class="panel-body">';
                            $out .= '<img class="showInfo" data-id="'.$n->refPage->id.'" data-playerId="'.$currentPlayer->id.'" src="'.$n->refPage->image->getCrop("thumbnail")->url.'" alt="No Image" />';
                            $out .= '</div>';
                            $out .= '<div class="panel-footer">';
                            $out .= $out_02;
                            $out .= '</div>';
                            continue;
                        }
                        if ($n->task->is("name=best-time-lost")) {
                            $out .= '<div class="panel-heading">';
                            $out .= '<p class="panel-title">';
                            $out .= '<i class="glyphicon glyphicon-thumbs-down"></i> <i class="glyphicon glyphicon-time"></i> '.sprintf(__('Best time lost on %s'), $n->refPage->title);
                            $out .= '</p>';
                            $out .= '</div>';
                            $out .= '<div class="panel-body">';
                            $out .= '<img class="showInfo" data-id="'.$n->refPage->id.'" data-playerId="'.$currentPlayer->id.'" src="'.$n->refPage->image->getCrop("thumbnail")->url.'" alt="No image" />';
                            $out .= '</div>';
                            $out .= '<div class="panel-footer">';
                            $out .= $out_02;
                            $out .= '</div>';
                            continue;
                        }
                        if ($n->refPage && $n->refPage->is("template=lesson")) {
                            $out .= '<div class="panel-heading">';
                            $out .= '<p class="panel-title">';
                            $out .= '<span class="label label-success">'.__('Copied lesson').'</span> : ';
                            $out .= '<span class="">'.$n->refPage->title.'</span>';
                            $out .= '</p>';
                            $out .= '</div>';
                            $out .= '<div class="panel-body">';
                            $out .= '<img src="'.$pages->get("name=book-knowledge")->image->getCrop("thumbnail")->url.'" alt="Open book." />';
                            $out .= '</div>';
                            $out .= '<div class="panel-footer">';
                            $out .= $out_02;
                            $out .= '</div>';
                            continue;
                        }
                        if ($n->task->is("name=buy-pdf")) {
                            $out .= '<div class="panel-heading">';
                            $out .= '<p class="panel-title">';
                            $out .= '<span class="label label-success">'.__('Buy PDF').'</span> : ';
                            $out .= '<span class="">'.$n->refPage->title.'</span>';
                            $out .= '</p>';
                            $out .= '</div>';
                            $out .= '<div class="panel-body">';
                            $out .= '<img src="'.$pages->get("name=book-knowledge")->image->getCrop("thumbnail")->url.'" alt="Open book." />';
                            $out .= '</div>';
                            $out .= '<div class="panel-footer">';
                            $out .= $out_02;
                            $out .= '</div>';
                            continue;
                        }
                        if ($n->task->is("name=country-free")) {
                            $out .= '<div class="panel-heading">';
                            $out .= '<h6 class="panel-title"><span class="glyphicon glyphicon-thumbs-up"></span> '.__('Completed country !').'</h6>';
                            $out .= '</div>';
                            $out .= '<div class="panel-body">';
                            $out .= '<img src="'.$config->paths->assets.'paFiles/img/'.$n->task->name.'.png" ="globe" width="100" />';
                            $alreadyThere->add($n->refPage);
                            $out .= '</div>';
                            $out .= '<div class="panel-footer">';
                            $out .= '<h5 class="panel-title"><span class="">'.$n->refPage->title.'</span></h5>';
                            $out .= '</div>';
                            continue;
                        }
                        if ($n->task->is("name=country-lost")) {
                            $out .= '<div class="panel-heading">';
                            $out .= '<h6 class="panel-title"><span class="glyphicon glyphicon-thumbs-down"></span> '.__('Completed lost !').'</h6>';
                            $out .= '</div>';
                            $out .= '<div class="panel-body">';
                            $out .= '<img src="'.$config - paths - assets.'paFiles/img/'.$n->task->name.'.png" ="globe" width="100" />';
                            $out .= '</div>';
                            $out .= '<div class="panel-footer">';
                            $out .= '<h5 class="panel-title">'.$n->refPage->title.'</h5>';
                            $alreadyThere->add($n->refPage);
                            $out .= '</div>';
                            continue;
                        }
                        if ($n->task->is("name~=extra-homework")) {
                            $out .= '<div class="panel-heading">';
                            $out .= '<p class="panel-title">';
                            $out .= '✓ '.__('Extra-training');
                            /* $out .= '<span class="wrapped">'.$n->summary.'</span>'; */
                            $out .= '</p>';
                            $out .= '</div>';
                            $out .= '<div class="panel-body">';
                            $out .= '<h3 data-toggle="tooltip" title="'.$n->summary.'"><span class="glyphicon glyphicon-pencil"></span></h3>';
                            $out .= '</div>';
                            $out .= '<div class="panel-footer">';
                            $out .= $out_02;
                            $out .= '</div>';
                            continue;
                        }
                        $out .= '</div>';
                        $out .= '</li>';
                    }
                }
                $out .= '</ul>';
            } else {
                $out .= '<p>'.__("No recent news :(").'</p>';
            }
            $out .= '</div>';
            $out .= '<div class="panel-footer text-center">';
            $allDonations = $news->find("task.name=donation|donated");
            if ($allDonations->count() > 0) {
                $out .= '<hr />';
                $out .= '<ul>';
                $allDonationsIds = $allDonations->find("task.name=donated")->explode("linkedId");
                foreach ($allDonations as $e) {
                    if ($e->task->name == 'donation' || ($e->task->name == 'donated' && !in_array($e->linkedId, $allDonationsIds))) {
                        $currentPlayer = $e->parent('template=player');
                        $out .= '<li class="text-left">';
                        $out .= '<img src="'.$config->urls->templates.'img/heart.png" alt="heart" />';
                        $out .= ' <span>'.$currentPlayer->title.'</span>';
                        if ($e->task->is("name=donation")) {
                            $out .= ' → ';
                        } else {
                            $out .= ' ← ';
                        }
                        $out .= $e->summary.' ';
                        /* $out .= __('on').' '.date('l, M. j', $e->date); */
                        $out .= __('on').' '.date("D d M", $e->date);
                        $out .= '</li>';
                    }
                }
                $out .= '</ul>';
            }
            unset($news);
            $out .= '</div>';
            $out .= '</div>';

            // Ambassadors
            $ambassadors = $allPlayers->find("skills.name=ambassador");
            $ambassadorsList = $ambassadors->implode(', ', '{id}');
            if ($user->isSuperuser() || $user->hasRole('teacher')) {
                if ($ambassadors->count() == 0) {
                    $pickButton = '';
                } else {
                    $pickButton = ' <a class="btn btn-danger btn-xs '.$pickFromList.' pull-right" data-list="'.$ambassadorsList.'">'.__("Pick 1!").'</a>';
                }
            } else {
                $pickButton = '';
            }
            $out .= '<div id="" class="board panel panel-primary isFold">';
            $out .= '<div class="panel-heading">';
            $out .= '<h4 class="panel-title">';
            $out .= __("Ambassadors").$pickButton;
            $out .= '</h4>';
            $out .= '</div>';
            $out .= '<div class="panel-body">';
            $out .= '<ul class="list-unstyled list-inline text-center">';
            foreach ($ambassadors as $p) {
                $out .= '<li>';
                $out .= '<div class="thumbnail text-center">';
                if ($p->avatar) {
                    $out .= '<img class="'.$pickFromList.'" data-list="'.$p->id.'" src="'.$p->avatar->getCrop("thumbnail")->url.'" width="80" alt="'.$p->title.'." />';
                } else {
                    $out .= '<Avatar>';
                }
                $out .= '<caption class="text-center">'.$p->title.'</caption>';
                $out .= '</div>';
                $out .= '</li>';
            }
            if ($ambassadors->count() == 0) {
                $out .= '<p>'.__("No ambassadors today.").'</p>';
            }
            unset($ambassadors);
            $out .= '</ul>';
            $out .= '</div>';
            $out .= '</div>';

            // Captains
            $captains = $allPlayers->find("skills.name=captain");
            $captainsList = $captains->implode(', ', '{id}');
            if ($user->isSuperuser() || $user->hasRole('teacher')) {
                if ($captains->count() == 0) {
                    $pickButton = '';
                } else {
                    $pickButton = ' <a class="btn btn-danger btn-xs '.$pickFromList.' pull-right" data-list="'.$captainsList.'">'.__("Pick 1!").'</a>';
                }
            } else {
                $pickButton = '';
            }
            $out .= '<div id="" class="board panel panel-primary isFold">';
            $out .= '<div class="panel-heading">';
            $out .= '<h4 class="panel-title">';
            $out .= __("Captains").$pickButton;
            $out .= '</h4>';
            $out .= '</div>';
            $out .= '<div class="panel-body">';
            $out .= '<ul class="list-unstyled list-inline text-center">';
            foreach ($captains as $p) {
                $out .= '<li>';
                $out .= '<div class="thumbnail text-center">';
                if ($p->avatar) {
                    $out .= '<img class="'.$pickFromList.'" data-list="'.$p->id.'" src="'.$p->avatar->getCrop("thumbnail")->url.'" width="80" alt="'.$p->title.'." />';
                } else {
                    $out .= '<Avatar>';
                }
                $out .= '<caption class="text-center">'.$p->title.'</caption>';
                $out .= '</div>';
                $out .= '</li>';
            }
            if ($captains->count() == 0) {
                $out .= '<p>'.__("No captains yet.").'</p>';
            }
            unset($captains);
            $out .= '</ul>';
            $out .= '</div>';
            $out .= '</div>';

            // Most active players
            $top = $allPlayers->sort('-yearlyKarma, karma')->find("limit=5, yearlyKarma>0");
            if ($top->count() != 0) {
                $topList = $top->implode(', ', '{id}');
                if ($user->isSuperuser() || $user->hasRole('teacher')) {
                    $pickButton = ' <a class="btn btn-danger btn-xs '.$pickFromList.' pull-right" data-list="'.$topList.'">'.__("Pick 1!").'</a>';
                } else {
                    $pickButton = '';
                }
                $out .= '<div id="" class="board panel panel-primary isFold">';
                $out .= '<div class="panel-heading">';
                $out .= '<h4 class="panel-title">';
                $out .= __("Most active players !").$pickButton;
                $out .= '</h4>';
                $out .= '</div>';
                $out .= '<div class="panel-body">';
                $out .= '<ul class="list-unstyled list-inline text-center">';
                foreach ($top as $p) {
                    $out .= '<li>';
                    $out .= '<div class="thumbnail text-center">';
                    if ($p->avatar) {
                        $out .= '<img class="'.$pickFromList.'" data-list="'.$p->id.'" src="'.$p->avatar->getCrop("thumbnail")->url.'" alt="'.$p->title.'." />';
                    } else {
                        $out .= '<Avatar>';
                    }
                    $out .= '<caption class="text-center">'.$p->title.' <span class="badge">'.$p->yearlyKarma.'K</span></caption>';
                    $out .= '</div>';
                    $out .= '</li>';
                }
                $out .= '</ul>';
                $out .= '</div>';
                $out .= '</div>';
            }

            // Top players
            $topPlayers = new PageArray();
            $fpPlayer = $allPlayers->sort('-fighting_power, karma')->first();
            if ($fpPlayer->fighting_power == 0) {
                unset($fpPlayer);
            } else {
                $topPlayers->add($fpPlayer);
            }
            $donPlayer = $allPlayers->sort('-donation, karma')->first();
            if ($donPlayer->donation == 0) {
                unset($donPlayer);
            } else {
                $topPlayers->add($donPlayer);
            }
            $utPlayer = $allPlayers->sort('-underground_training, karma')->first();
            if ($utPlayer->underground_training == 0) {
                unset($utPlayer);
            } else {
                $topPlayers->add($utPlayer);
            }
            $eqPlayer = $allPlayers->sort('-equipment.count, karma')->first();
            if (count($eqPlayer->equipment) == 0) {
                unset($eqPlayer);
            } else {
                $topPlayers->add($eqPlayer);
            }
            $plaPlayer = $allPlayers->sort('-places.count, karma')->first();
            if (count($plaPlayer->places) == 0) {
                unset($plaPlayer);
            } else {
                $topPlayers->add($plaPlayer);
            }
            if ($rank >= 8) {
                $peoPlayer = $allPlayers->sort('-people.count, karma')->first();
                if (count($peoPlayer->people) == 0) {
                    unset($peoPlayer);
                } else {
                    $topPlayers->add($peoPlayer);
                }
            }
            $topPlayersList = $topPlayers->implode(', ', '{id}');
            if ($user->isSuperuser() || $user->hasRole('teacher')) {
                if ($topPlayers->count() == 0) {
                    $pickButton = '';
                } else {
                    $pickButton = ' <a class="btn btn-danger btn-xs '.$pickFromList.' pull-right" data-list="'.$topPlayersList.'">'.__("Pick 1!").'</a>';
                }
            } else {
                $pickButton = '';
            }
            $out .= '<div id="" class="board panel panel-primary isFold">';
            $out .= '<div class="panel-heading">';
            $out .= '<p class="panel-title">'.__("Top players !").$pickButton.'</p>';
            $out .= '</div>';
            $out .= '<div class="panel-body">';
            $out .= '<ul class="list-unstyled list-inline text-center">';
            if (isset($fpPlayer)) {
                $out .= '<li>';
                $out .= '<div class="fame thumbnail">'; // Best warrior
                $out .= '<span class="badge">'.__("Best warrior !").'</span>';
                if ($fpPlayer->avatar) {
                    $out .= '<img class="'.$pickFromList.'" data-list="'.$fpPlayer->id.'" src="'.$fpPlayer->avatar->getCrop("thumbnail")->url.'" width="80" alt="'.$fpPlayer->title.'." />';
                }
                $out .= '<div class="caption text-center">'.$fpPlayer->title.' <span class="badge">'.$fpPlayer->fighting_power.__("FP").'</span></div>';
                $out .= '</div>';
                $out .= '</li>';
            }
            if (isset($donPlayer)) {
                $out .= '<li>';
                $out .= '<div class="fame thumbnail">'; // Best donator
                $out .= '<span class="badge">'.__("Best donator !").'</span>';
                if ($donPlayer->avatar) {
                    $out .= '<img class="'.$pickFromList.'" data-list="'.$donPlayer->id.'" src="'.$donPlayer->avatar->getCrop("thumbnail")->url.'" width="80" alt="'.$donPlayer->title.'." />';
                }
                $out .= '<div class="caption text-center">'.$donPlayer->title.' <span class="badge">'.$donPlayer->donation.'Don.</span></div>';
                $out .= '</div>';
                $out .= '</li>';
            }
            if (isset($utPlayer)) {
                $out .= '<li>';
                $out .= '<div class="fame thumbnail">'; // Most trained
                $out .= '<span class="badge">'.__("Most trained !").'</span>';
                if ($utPlayer->avatar) {
                    $out .= '<img class="'.$pickFromList.'" data-list="'.$utPlayer->id.'" src="'.$utPlayer->avatar->getCrop("thumbnail")->url.'" width="80" alt="'.$utPlayer->title.'." />';
                }
                $out .= '<div class="caption text-center">'.$utPlayer->title.' <span class="badge">'.$utPlayer->underground_training.__("UT").'</span></div>';
                $out .= '</div>';
                $out .= '</li>';
            }
            if (isset($eqPlayer)) {
                $out .= '<li>';
                $out .= '<div class="fame thumbnail">'; // Most equipped
                $out .= '<span class="badge">'.__("Most equipped !").'</span>';
                if ($eqPlayer->avatar) {
                    $out .= '<img class="'.$pickFromList.'" data-list="'.$eqPlayer->id.'" src="'.$eqPlayer->avatar->getCrop("thumbnail")->url.'" width="80" alt="'.$eqPlayer->title.'." />';
                }
                $out .= '<div class="caption text-center">'.$eqPlayer->title.' <span class="badge">'.$eqPlayer->equipment->count().'eq.</span></div>';
                $out .= '</div>';
                $out .= '</li>';
            }
            if (isset($plaPlayer)) {
                $out .= '<li>';
                $out .= '<div class="fame thumbnail">'; // Greatest # of Places
                $out .= '<span class="badge">'.__("Greatest # of Places !").'</span>';
                if ($plaPlayer->avatar) {
                    $out .= '<img class="'.$pickFromList.'" data-list="'.$plaPlayer.'" src="'.$plaPlayer->avatar->getCrop("thumbnail")->url.'" width="80" alt="'.$plaPlayer->title.'." />';
                }
                $out .= '<div class="caption text-center">'.$plaPlayer->title.' <span class="badge">'.$plaPlayer->places->count().__("pla.").'</span></div>';
                $out .= '</div>';
                $out .= '</li>';
            }
            if ($rank >= 8) {
                if (isset($peoPlayer)) {
                    $out .= '<li>';
                    $out .= '<div class="fame thumbnail">'; // Greatest # of people
                    $out .= '<span class="badge">'.__("Greatest # of People !").'</span>';
                    if ($peoPlayer->avatar) {
                        $out .= '<img class="'.$pickFromList.'" data-list="'.$peoPlayer->id.'" src="'.$peoPlayer->avatar->getCrop("thumbnail")->url.'" width="80" alt="'.$peoPlayer->title.'." />';
                    }
                    $out .= '<div class="caption text-center">'.$peoPlayer->title.' <span class="badge">'.$peoPlayer->people->count().'peo.</span></div>';
                    $out .= '</div>';
                    $out .= '</li>';
                }
            }
            $out .= '</ul>';
            $out .= '</div>';
            $out .= '</div>';
            $out .= '</div>';
        } else {
            $out .= '<h3 class="text-center">'.__("No players in the team !").'</h3>';
        }
        return $out;
    });
    $out .= $cachedOffice;
} else {
    $out = $noAuthMessage;
}

$pages->unCacheAll();
echo $out;

include("./foot.inc");
