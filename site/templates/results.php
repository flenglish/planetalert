<?php namespace ProcessWire;

  include("./head.inc"); 

  $out = '';
  $playerId = $input->urlSegment1;
  $complete = $input->urlSegment2;
  $playerPage = $pages->get("parent.name=players, id=$playerId");

  if (isset($player) && $user->isLoggedin() && $user->name == $playerPage->name || $user->isSuperuser() || $user->hasRole('teacher')) {
    if ($complete == 'activity') { // Activity report
      $activityUrl = $pages->get("name=activity")->url;
      // From tmpCache if available
      $today = new \DateTime("today");
      $tmpPage = $playerPage->child("name=tmp");
      if ($tmpPage->id && $tmpPage->tmpMonstersActivity) {
        $allConcernedMonsters = $tmpPage->tmpMonstersActivity->find("trainNb>0, monster.parent.name=monsters");
        $allConcernedMonsters->sort("lastTrainDate, date");
        if ($allConcernedMonsters->count() > 0) {
          $totalFightsNb =  array_sum(mb_split(',', $allConcernedMonsters->implode(',', '{fightNb}')));
          $allFoughtMonstersNb = $allConcernedMonsters->find("fightNb>0")->count();
          echo '<h4>';
          echo '<p class="label label-danger"> '.sprintf(__("You have trained on %d different monsters"), $allConcernedMonsters->count()).'</p>';
          echo ' <span class="label label-danger"> ';
          echo sprintf(_n('You have done %d fight ', 'You have done %d fights ', $totalFightsNb), $totalFightsNb);
          echo sprintf(_n('on %d monster', 'on %d different monsters', $allFoughtMonstersNb), $allFoughtMonstersNb);
          echo '</span>';
          if ($totalFightsNb < 10) {
            echo ' <span class="label label-danger">';
              echo ' <span class="glyphicon glyphicon-thumbs-down"></span> ';
              echo __("The Fighters playground is still locked !");
              echo ' <span class="glyphicon glyphicon-info-sign" datat-toggle="tooltip" title="'.__("10 fights are required to become a Fighter and gain access to the Fighters playground.").'" onmouseenter="$(this).tooltip(\'show\');"></span> ';
            echo '</span>';
          } else {
            echo ' <span class="label label-success">';
              echo ' <span class="glyphicon glyphicon-thumbs-up"></span> ';
              echo __("The Fighters playground is unlocked !");
            echo '</span>';
          }
          echo '</h4>';
          echo '<ul class="utReport list-group list-unstyled">';
          foreach($allConcernedMonsters as $m) {
            setMonster($playerPage, $m->monster);
            // Find # of days compared to today
            $date2 = new \DateTime(date("Y-m-d", $m->lastTrainDate));
            $interval = $today->diff($date2);
            $m->lastTrainingInterval = $interval->days;
            $date3 = new \DateTime(date("Y-m-d", $m->date));
            $availableInterval = $today->diff($date3);
            echo '<li>';
            echo '<span>';
            if ($m->monster->is("template!=megamonster")) {
              echo ' <a href="'.$activityUrl.$m->monster->id.'/'.$playerId.'" data-type="analyzer" class="ajaxBtn btn btn-primary btn-xs" data-url="'.$activityUrl.$m->monster->id.'/'.$playerId.'" data-toggle="tooltip" title="'.__('Activity analysis').'" onmouseenter="$(this).tooltip(\'show\');" onmouseleave="$(this).tooltip(\'hide\');"><span class="glyphicon glyphicon-stats"></span></a> ';
              if ($m->monster->isBestTrained) { 
                echo '<span class="glyphicon glyphicon-thumbs-up"></span> ';
              }
              if ($m->monster->isMaster) { 
                echo '<span class="glyphicon glyphicon-time"></span> ';
              }
              if ($m->monster->quality > 0.3 && $m->monster->fightNb >= 5) {
                echo '<span class="glyphicon glyphicon-star"></span> ';
              }
              echo '<a href="#" class="monsterInfo" data-href="'.$m->monster->url.'?playerId='.$playerPage->id.'">'.$m->monster->title.'</a>';
            } else {
              echo __('Mega-monster training').' → '.$m->monster->title;
            }
            echo '</span> : ';
            echo '<span>'.sprintf(__('%d UT'), $m->monster->utGain).'</span>';
            if ($m->monster->fightNb > 0) {
              echo ', ';
              echo '<span>'.sprintf(_n("%d fight", "%d fights", $m->monster->fightNb), $m->monster->fightNb);
              echo ' → <span data-toggle="tooltip" title="Quality : '.$m->monster->quality.'" onmouseenter="$(this).tooltip(\'show\');" data-html="true"> '.averageLabel($m->monster->quality).'</span>';
            }
            if ($m->monster->isTrainable != 1) {
              if ($m->monster->waitForTrain == 1) { $label = __('Available tomorrow !'); } else { $label = sprintf(__("Available in %d days"), $m->monster->waitForTrain); }
              echo ' <span data-toggle="tooltip" title="'.$label.'" onmouseenter="$(this).tooltip(\'show\');">[Last training : '.$m->monster->lastTrainingInterval.']</span>';
            }
            echo '</li>';
          }
          $allMonsters = getAllMonsters($playerPage);
          $neverTrainedNb = $allMonsters->count()-$allConcernedMonsters->count();
          if ($neverTrainedNb == 0) {
            echo '<li class="label label-success">'.__("You have trained on ALL monsters !").'</li>';
          } else {
            echo '<li class="label label-danger">'.sprintf(__('You have NEVER trained on %d monsters'), $neverTrainedNb).'</li>';
          }
          if ($neverTrainedNb != $tmpPage->index) { $tmpPage->setAndSave("index", $neverTrainedNb); }
          echo '</ul>';
        } else {
          echo "<p>".__("You have never used the Memory Helmet.")."</p>";
        }
      }

      // Battle report
      $headTeacher = getHeadTeacher($playerPage);
      $allBattles = battleReport($playerPage);
      $attacks = array();
      $allBattles->sort("-date");
      foreach ($allBattles as $p) { // Group by linkedId
        if ($p->linkedId != '') {
          $attacks["$p->linkedId"][] = $p;
        } else { // Make a unique id
          $uniqueId = mt_rand(100000, 999999); // Unique test number
          $attacks["$uniqueId"][] = $p;
        }
      }
      if (count($attacks) > 0) {
        echo '<p>';
          echo '<span class="label label-primary">'.sprintf(_n('You have faced %d monster attack during the current school year.', 'You have faced %d monster attacks during the current school year.', count($attacks)), count($attacks)).'</span> ';
          echo '<a href="'.$pages->get("name=results")->url.$playerId.'" target="_blank">[<span class="glyphicon glyphicon-stats"></span> '.__('See my results analysis.').']</a>';
        echo '</p>';
        echo '<ul class="utReport list-group list-unstyled">';
        foreach($attacks as $key => $gr){
          $count = count($gr);
          if($count > 1){
            $pos = 0;
            $neg = 0;
            $vv = 0;
            $v = 0;
            $r = 0;
            $rr = 0;
            echo '<li>';
            foreach($gr as $key => $m){
              switch($m->task->name) {
                case 'battle-vv': $vv++; $pos++; break;
                case 'battle-v': $v++; $pos++; break;
                case 'battle-r': $r++; $neg++; break;
                case 'battle-rr': $rr++; $neg++; break;
                default: break;
              }
            }
            if ($vv != 0) echo '<span class="label label-success">'.$vv.'VV</span>';
            if ($v != 0) echo ' <span class="label label-success">'.$v.'V</span>';
            if ($r != 0) echo ' <span class="label label-danger">'.$r.'R</span>';
            if ($rr != 0) echo ' <span class="label label-danger">'.$rr.'RR</span>';
            $testTitle = $gr[0]->summary;
            preg_match('/\[(.*?)\]/', $gr[0]->summary, $matches);
            echo ' : '.$matches[1].' ['.date("d/m", $gr[0]->date).']';
            echo "</li>";
          } else if ($count) {
            foreach($gr as $key => $m){
              echo '<li>';
              echo $m->result.' : '.$m->getLanguageValue($headTeacher->language, 'summary');
              echo ' ['.date("d/m", $m->date).']';
              echo "</li>";
            }
          }
          echo "</li>";
        }
        echo '</ul>';
      } else {
        echo "<p>".__("You haven't faced any monster attacks during the current school year.")."</p>";
      }
      unset($allBattles);
      $pages->unCacheAll();

    } else { // Battle reports
      if ($complete == 'all') {
        $allBattles = battleReport($playerPage, false, true);
        $allFightRequests = $pages->find("has_parent=$playerPage, template=event, task.name~=fight, inClass=1")->sort("date");
      } else {
        $allBattles = battleReport($playerPage);
        $allFightRequests = $playerPage->get("name=history")->find("template=event, task.name~=fight, inClass=1")->sort("date");
      }
      $allBattles->sort("date");
      $allResultsNb = $allBattles->count()+$allFightRequests->count();
      $allMonstersNb = $pages->count("parent.name=monsters, template=exercise, exerciseOwner.singleTeacher=$headTeacher, exerciseOwner.publish=1");
      $items = array();

      if ($complete == 'all') {
        $out .= '<h2 class="text-center">';
          $out .= sprintf(__("COMPLETE results analysis for %s"), $playerPage->title.' ['.$playerPage->team->title.']');
          $out .= ' ['.sprintf(__('%s results'), $allResultsNb).']';
        $out .= '</h2>';
        $out .= '<h3 class="text-center"><a class="btn btn-primary" href="'.$page->url.$input->urlSegment1.'">See only current school year results.</a></h3>';
      } else {
        $out .= '<h2 class="text-center">';
          $out .= sprintf(__("Current school year results analysis for %s"), $playerPage->title.' ['.$playerPage->team->title.']');
          $out .= ' ['.sprintf(__('%s results'), $allResultsNb).']';
        $out .= '</h2>';
          $out .= '<h3 class="text-center">'.sprintf(__('You\'ve requested %1$s fight·s out of %2$s possible this year.'), $allFightRequests->count(), $allMonstersNb);
          $out .= ' <a href="'.$pages->get("name=fight-requests")->url.$playerPage->id.'">'.__("[See my fight requests hall of fame]").'</a>';
          $out .= '</h3>';
        $out .= '<h3 class="text-center"><a class="btn btn-primary" href="'.$page->url.$input->urlSegment1.'/all">See COMPLETE results.</a></h3>';
      }

      $out .= '<h5 class="text-center">';
      $sacocheLink = '<a href="https://sacoche.sesamath.net/sacoche/?sso=7970" target="_blank">SACoche</a>';
      $out .= sprintf(__("Please, go and see %s for a better analysis."), $sacocheLink);
      $out .= '</h5>';

      foreach ($allBattles as $p) { // Group by items
        preg_match('/(\[.*?\])/', $p->summary, $matches);
        $itemTitle = trim(str_replace($matches[1], '', $p->summary));
        $eDate = ' ['.date("d/m/Y", $p->date).']';
        switch($p->task->name) {
          case 'battle-vv': $result = '<span class="label label-success" data-toggle="tooltip" title="'.$eDate.' '.__('Battle : ').$matches[1].'">VV</span>'; break;
          case 'battle-v': $result = '<span class="label label-success" data-toggle="tooltip" title="'.$eDate.' '.__('Battle : ').$matches[1].'">V</span>'; break;
          case 'battle-r': $result = '<span class="label label-danger" data-toggle="tooltip" title="'.$eDate.' '.__('Battle : ').$matches[1].'">R</span>'; break;
          case 'battle-rr': $result = '<span class="label label-danger" data-toggle="tooltip" title="'.$eDate.' '.__('Battle : ').$matches[1].'">RR</span>'; break;
          default: $result = '<span class="label label-primary" data-togglt="tooltip" title="'.$eDate.' '.__('Battle : ').$matches[1].'">-</span>'; break;
        }
        $item[$itemTitle][] = $result;
      }
      foreach ($allFightRequests as $p) { // Group by Monsters' names
        $eDate = ' ['.date("d/m/Y", $p->date).']';
        switch($p->task->name) {
          case 'fight-vv': $result = '<span class="label label-success" data-toggle="tooltip" title="'.$eDate.' '.__('Fight request').'">VV</span>'; break;
          case 'fight-v': $result = '<span class="label label-success" data-toggle="tooltip" title="'.$eDate.' '.__('Fight request').'">V</span>'; break;
          case 'fight-r': $result = '<span class="label label-danger" data-toggle="tooltip" title="'.$eDate.' '.__('Fight request').'">R</span>'; break;
          case 'fight-rr': $result = '<span class="label label-danger" data-toggle="tooltip" title="'.$eDate.' '.__('Fight request').'">RR</span>'; break;
          default: $result = '<span class="label label-primary" data-togglt="tooltip" title="'.$eDate.' '.__('Fight request').'">-</span>'; break;
        }
        /* $item[$p->refPage->summary][] = $result; */
        $item[$p->refPage->title][] = $result;
      }
      $out .= ' <table id="resultsTable" class="table table-condensed table-hover">';
        $out .= '<thead>';
        $out .= ' <tr>';
        $out .= '  <th>'.__('Item').'</th>';
        $out .= '  <th>'.__('Results').'</th>';
        $out .= ' </tr>';
        $out .= '</thead>';
        $out .= '<tbody>';
      foreach ($item as $i=>$r) {
        $out .= '<tr>';
        $out .= ' <td data-order="'.$i.'">';
        $out .= $i;
        $out .= ' </td>';
        $out .= ' <td class="text-left">';
        foreach ($r as $result) {
          $out .= $result.' ';
        }
        $out .= ' </td>';
        $out .= '</tr>';
      }
      $out .= '</tbody>';
      $out .= '</table>';
    }
  } else {
    $out .= $noAuthMessage;
  }

  echo $out;

  include("./foot.inc"); 
?>
