<?php

namespace ProcessWire;

include('./my-functions.inc'); // Planet Alert PHP functions

$adminMail = $users->get("name=admin")->email;

if (!$user->isSuperuser()) {
    $headTeacher = getHeadTeacher($user);
    $user->language = $headTeacher->language;
}

if ($user->hasRole('player')) {
    // Get logged in player
    $player = $pages->get("parent.name=players, template=player, login=$user->name");
    $player->of(false);
    // Buy PDF
    if (isset($input->get->form) && $input->get->form == 'buyPdf' && $input->get->playerId != '' && $input->get->lessonId != '') {
        // Add buy-pdf action to player's history and update GC
        $player = $pages->get($input->get->playerId);
        $lesson = $pages->get($input->get->lessonId);
        $task = $pages->get("name=buy-pdf");
        $task->comment = 'Buy PDF ('.$lesson->title.')';
        $task->refPage = $lesson;
        updateScore($player, $task, true);
        // Notify teacher or admin
        $subject = _('Buy PDF ').' : ';
        $subject .= $player->title. ' ['.$player->team->title.']';
        $subject .= ' - '.$lesson->title;
        $msg = "Player : ".$player->title." [".$player->team->title."]\r\n";
        $msg .= "Buy PDF : ".$lesson->title."\r\n";
        if ($_SERVER['REMOTE_ADDR'] != '127.0.0.1') {
            $message = $mail->new();
            $message->from($adminMail, "Planel Alert");
            if ($headTeacher && $headTeacher->email != '') {
                $message->to($headTeacher->email);
            } else {
                $message->to($adminMail);
            }
            $message->subject($subject);
            $message->body($msg);
            $numSent = $message->send();
        }
    }

    // Toggle enroll group
    if (isset($input->get->form) && $input->get->form == 'enroll' && $input->get->teamId != '' && $input->get->groupId != '' && $player->skills->has("name=captain")) {
        $selectedTeam = $pages->get($input->get->teamId);
        $selectedGroup = $pages->get($input->get->groupId);
        if ($selectedTeam->enrolledGroups->has($selectedGroup)) {
            $selectedTeam->enrolledGroups->remove($selectedGroup);
        } else {
            $selectedTeam->enrolledGroups->add($selectedGroup);
        }
        $selectedTeam->of(false);
        $selectedTeam->save();
    }

    if (isset($input->get->form) && $input->get->form == 'manualTask' && $input->get->playerId != '' && $input->get->lessonId != '') { // Book of Knowledge use
        $player = $pages->get($input->get->playerId);
        $refPage = $pages->get($input->get->lessonId);
        $task = $refPage->task;
        if ($task->is("name=extra-homework|very-extra-homework")) {
            $task->comment = __("Good Copy work");
            $task->refPage = $refPage;
            $task->linkedId = false;
            // Only 1 pending lesson allowed for a player
            $already = $pages->get("name=book-knowledge-lessons, pending.player=$player");
            if (!$already || !$already->isTrash()) {
                $pendingId = savePendingLesson($player, $task);
            }
            // Notify teacher or admin
            $subject = _('Copied lesson ').' : ';
            $subject .= $player->title. ' ['.$player->team->title.']';
            $subject .= ' - '.$refPage->title;
            $msg = __("Player")." : ".$player->title." [".$player->team->title."]\r\n";
            $msg .= __("Copied lesson")." : ".$refPage->title."\r\n";
            if ($_SERVER['REMOTE_ADDR'] != '127.0.0.1') {
                $message = $mail->new();
                $message->from($adminMail, "Planel Alert");
                $message->subject($subject);
                $message->body($msg);
                if ($headTeacher && $headTeacher->email != '') {
                    $message->to($headTeacher->email);
                } else {
                    $message->to($adminMail);
                }
                $numSent = $message->send();
            }
        }
    }

    if ($input->post->buyFormSubmit) { // buyForm submitted
        $itemId = $input->post->item;
        $newItem = $pages->get($itemId);
        // Set task according to newItem's type
        if ($newItem->template == 'equipment' || $newItem->template == 'item') {
            $task = $pages->get("name=buy");
        }
        if ($newItem->template == 'place' || $newItem->template == 'people') {
            $task = $pages->get("name=free");
        }
        // Check if item is not already there
        $already = false;
        foreach ($player->equipment as $eq) {
            if ($eq->id == $newItem->id) {
                if ($newItem->parent->name !== 'potions') {
                    $already = true;
                }
            }
        }
        foreach ($player->places as $pl) {
            if ($pl->id == $newItem->id) {
                $already = true;
            }
        }
        foreach ($player->people as $pl) {
            if ($pl->id == $newItem->id) {
                $already = true;
            }
        }
        // Check if item's GC is not out of reach
        if ($newItem->GC > $player->GC) {
            $already = true;
        }
        $task->comment = $newItem->title;
        $task->refPage = $newItem;
        $task->linkedId = false;
        if ($newItem->GC <= $player->GC && $already != true) { // Final 'security' check
            updateScore($player, $task, true);
            // No need to checkDeath, Buyform can't cause death
            $msg = "Player : ". $player->title." [".$player->team->title."]\r\n";
            $msg .= "Item : ". $newItem->title;
        } else {
            // Notify admin
            $msg = "Player : ". $sanitizer->entities1($player->title)." [".$player->team->title."]\r\n";
            $msg .= "Item : ". $sanitizer->entities1($newItem->title)."\r\n";
            $msg .= "An error has occurred.";
        }
        $subject = _('Buy form ').' : ';
        $subject .= $player->title.' ['.$player->team->title.']';
        $subject .= ' - '.$newItem->title.' [Level '.$newItem->level.']';
        if ($_SERVER['REMOTE_ADDR'] != '127.0.0.1') {
            $message = $mail->new();
            if ($headTeacher && $headTeacher->email != '') {
                $message->to($headTeacher->email, "Planet Alert");
            } else {
                $message->to($adminMail, "Planet Alert");
            }
            $message->from($adminMail);
            $message->fromName("Planel Alert");
            $message->subject($subject);
            $message->body($msg);
            $numSent = $message->send();
        }
    }

    if ($input->post->marketPlaceSubmit) { // marketPlaceForm submitted
        $checkedItems = $input->post->item; // Array
        //Limit to 3 items a day
        $today = new \DateTime("today");
        $limitDate = strtotime($today->format('Y-m-d'));
        $todayItemsCount = $player->get("name=history")->find("date>=$limitDate, task.name=buy|free")->count();

        if ($todayItemsCount < 3) {
            foreach ($checkedItems as $item => $state) {
                $newItem = $pages->get($item);
                // Check if item is not already there
                $already = false;
                foreach ($player->equipment as $eq) {
                    if ($eq->id == $newItem->id) {
                        if ($newItem->parent->name !== 'potions') {
                            $already = true;
                        }
                    }
                }
                foreach ($player->places as $pl) {
                    if ($pl->id == $newItem->id) {
                        $already = true;
                    }
                }
                foreach ($player->people as $pl) {
                    if ($pl->id == $newItem->id) {
                        $already = true;
                    }
                }
                // Check if item's GC is not out of reach
                if ($newItem->GC > $player->GC) {
                    $already = true;
                }
                if ($already == false) {
                    // Get item's data
                    if ($newItem->template == 'equipment' || $newItem->template == 'item') {
                        $task = $pages->get("name=buy");
                    }
                    if ($newItem->template == 'place' || $newItem->template == 'people') {
                        $task = $pages->get("name=free");
                    }
                    // Update player's scores and save
                    $task->comment = $newItem->title;
                    $task->refPage = $newItem;
                    $task->linkedId = false;
                    updateScore($player, $task, true);
                    // No need to checkDeath, MarketPlace can't cause death
                    // Notify admin
                    $subject = _('Buy form ').' : ';
                    $subject .= $player->title. ' ['.$player->team->title.']';
                    $subject .= ' - '.$newItem->title;
                    $msg = "Player : ". $player->title."\r\n";
                    $msg .= "Team : ". $player->team->title."\r\n";
                    $msg .= "Item : ". $newItem->title;
                    if ($_SERVER['REMOTE_ADDR'] != '127.0.0.1') {
                        $message = $mail->new();
                        $message->from($adminMail, "Planel Alert");
                        $message->subject($subject);
                        $message->body($msg);
                        if ($headTeacher && $headTeacher->email != '') {
                            $message->to($headTeacher->email);
                        } else {
                            $message->to($adminMail);
                        }
                        $numSent = $message->send();
                    }
                }
            }
        } else {
            // Notify admin
            $subject = _('3 items a day limit reached !').' : ';
            $subject .= $player->title. ' ['.$player->team->title.']';
            $msg = "Player : ". $player->title."\r\n";
            $msg .= "Team : ". $player->team->title."\r\n";
            if ($_SERVER['REMOTE_ADDR'] != '127.0.0.1') {
                $message = $mail->new();
                $message->from($adminMail, "Planel Alert");
                $message->subject($subject);
                $message->body($msg);
                if ($headTeacher && $headTeacher->email != '') {
                    $message->to($headTeacher->email);
                } else {
                    $message->to($adminMail);
                }
                $numSent = $message->send();
            }
        }
    }

    if ($input->post->donateFormSubmit) { // donateForm submitted
        $amount = (int) $input->post->amount;
        $receiverId = $input->post->receiver;
        $teamChest = $input->post->chest;
        $receiver = $pages->get($receiverId);
        $receiver->of(false);

        // Save donation
        // If valid amount
        if (!$teamChest) {
            if ($player && $receiverId && $player->id != $receiverId && $amount != 0 && $amount <= $player->GC) {
                $task = $pages->get("template=task, name=donation");
                if ($receiver->is("template=team")) {
                    $task->comment = sprintf(__('%1$sGC donated to %2$s team chest.'), $amount, $receiver->title);
                } else {
                    $task->comment = sprintf(__('%1$sGC donated to %2$s [%3$s]'), $amount, $receiver->title, $receiver->team->title);
                }
                $task->refPage = $receiver;
                $task->linkedId = false;
                updateScore($player, $task, true);
                // Modify player's page
                // No need to checkDeath, Donation can't cause death
                // Notify admin
                $subject = __('Donation').' : ';
                $subject .= $amount.__("GC");
                $subject .= ' '.$player->title. ' ['.$player->team->title.']';
                if ($receiver->is("template=team")) {
                    $subject .= ' - '.sprintf(__('[%s] team chest'), $receiver->title);
                } else {
                    $subject .= ' - '.$receiver->title.' ['.$receiver->team->title.']';
                }
                $msg = __("Player")." : ".$player->title." [".$player->team->title."]\r\n";
                $msg .= __("Donation amount")." : ". $amount."\r\n";
                if ($receiver->is("template=team")) {
                    $msg .= __("Donated to")." : ".sprintf(__('[%s] team chest'), $receiver->title)."\r\n";
                    $msg .= __("Team chest amount")." : ". $receiver->GC."\r\n";
                } else {
                    $msg .= __("Donated to")." : ".$receiver->title." [".$receiver->team->title."]\r\n";
                }
                $msg .= __("Player global donation indicator")." : ". $player->donation."\r\n";
            } else {
                $subject = __('Donation Error').' : ';
                $subject .= $amount.__("GC");
                $subject .= ' '.$player->title. ' ['.$player->team->title.']';
                $subject .= ' - '.$receiver->title.' ['.$receiver->team->title.']';
                $msg = __("Player")." : ".$player->title." [".$player->team->title."]\r\n";
                $msg .= __("Donation amount")." : ". $amount."\r\n";
                if ($receiver->is("template=team")) {
                    $msg .= __("Donated to")." : ".$receiver->title." Team chest";
                } else {
                    $msg .= __("Donated to")." : ".$receiver->title." [".$receiver->team->title."]\r\n";
                }
                $msg .= __("Player global donation indicator")." : ". $player->donation."\r\n";
            }
        } else {
            if ($amount <= $player->team->GC) {
                $task = $pages->get("template=task, name=team-solidarity");
                $task->comment = sprintf(__('%1$sGC taken from your team chest.'), $amount);
                $task->refPage = $player->team;
                $task->linkedId = false;
                updateScore($player, $task, true);
                // Modify player's page
                // No need to checkDeath, Donation can't cause death
                // Notify admin
                $subject = __('Team solidarity').' : ';
                $subject .= $amount.__("GC");
                $subject .= ' '.$player->title. ' ['.$player->team->title.']';
                $msg = __("Player")." : ".$player->title." [".$player->team->title."]\r\n";
                $msg .= __("Taken amount")." : ". $amount."\r\n";
                $msg .= __("Team chest left")." : ". $player->team->GC."\r\n";
            } else {
                $subject = __('Team solidarity').' : ';
                $subject .= $amount.__("GC");
                $subject .= ' '.$player->title. ' ['.$player->team->title.']';
                $subject .= ("Error !");
                $msg = __("Player")." : ".$player->title." [".$player->team->title."]\r\n";
                $msg .= __("Taken amount")." : ". $amount."\r\n";
                $msg .= __("Team chest left")." : ". $player->team->GC."\r\n";
            }
        }
        if ($_SERVER['REMOTE_ADDR'] != '127.0.0.1') {
            $message = $mail->new();
            $message->from($adminMail, "Planel Alert");
            $message->subject($subject);
            $message->body($msg);
            if ($headTeacher && $headTeacher->email != '') {
                $message->to($headTeacher->email);
            } else {
                $message->to($adminMail);
            }
            $numSent = $message->send();
        }
        // Redirection
        $url = $pages->get('/players')->url.$player->team->name.'/'.$player->name;
        echo json_encode(array("sender" => "donateForm", "url" => $url));
    }

    if (isset($input->get->form) && $input->get->form == 'cleanPad' && $input->get->pageId != '') { // Clean pad
        $selectedPage = $pages->get($input->get->pageId);
        $selectedPage->setAndSave('alertBox', 0);
        $selectedPage->setAndSave('pad', '');
    }

    if (isset($input->get->form) && $input->get->form == 'fightRequest' && $input->get->playerId != '' && $input->get->monsterId != '') { // Fight request
        $player = $pages->get($input->get->playerId);
        $monster = $pages->get($input->get->monsterId);
        if ($monster->is("template=exercise")) {
            // Only 1 pending lesson allowed for a player
            if ($player->fight_request == 0 || $player->fight_request == '') {
                $player->setAndSave('fight_request', $monster->id);
            }
            // Notify teacher or admin
            $subject = _('Fight request ').' : ';
            $subject .= $player->title. ' ['.$player->team->title.']';
            $subject .= ' - '.$monster->title;
            $msg = __("Player")." : ". $player->title." [".$player->team->title."]\r\n";
            $msg .= __("Fight request")." : ".$monster->title."\r\n";
            if ($_SERVER['REMOTE_ADDR'] != '127.0.0.1') {
                $message = $mail->new();
                $message->from($adminMail, "Planel Alert");
                $message->subject($subject);
                $message->body($msg);
                if ($headTeacher && $headTeacher->email != '') {
                    $message->to($headTeacher->email);
                } else {
                    $message->to($adminMail);
                }
                $numSent = $message->send();
            }
        }
    }

    if ($player->team->name != 'no-team') {
        setGroupCaptain($player->id);
    }

    // Toggle option
    if (isset($input->get->form) && $input->get->form == 'forceOption' && $input->get->pageId != '' && $input->get->optionName != '') {
        $selectedPage = $pages->get($input->get->pageId);
        if ($selectedPage->parent("login=$user->name")) { // Add security : player can alert only a page having his or her login
            $optionName = $input->get->optionName;
            if ($selectedPage->$optionName == 0) { // Untick
                $selectedPage->setAndSave($optionName, 1);
            } else { // Tick
                $selectedPage->setAndSave($optionName, 0);
            }
        }
    }

    // Redirect to player's profile (in main.js, because doesn't work due to Ajax ?)
    /* $session->redirect($pages->get('/players')->url.$player->team->name.'/'.$player->name); */
    /* $url = $pages->get('/players')->url.$player->team->name.'/'.$player->name; */
    /* echo json_encode(array("sender"=>"donateForm", "url"=>$url)); */
}

if ($user->hasRole('teacher') || $user->isSuperuser()) {
    // Unpublish News from Newsboard
    if (isset($input->get->form) && $input->get->form == 'unpublish' && $input->get->newsId != '') {
        $n = $pages->get($input->get->newsId);
        if ($n->publish == 0) { // Unpublish
            $n->setAndSave('publish', 1);
        } else { // News will disappear on reload
            $n->setAndSave('publish', 0);
        }
    }
    // Toggle option
    if (isset($input->get->form) && $input->get->form == 'forceOption' && $input->get->pageId != '' && $input->get->optionName != '') {
        $selectedPage = $pages->get($input->get->pageId);
        $optionName = $input->get->optionName;
        if ($selectedPage->$optionName == 0) { // Untick
            $selectedPage->setAndSave($optionName, 1);
        } else { // Tick
            $selectedPage->setAndSave($optionName, 0);
        }
    }
    // Toggle inClass indicator
    if (isset($input->get->form) && $input->get->form == 'inClass' && $input->get->eventId != '') {
        $e = $pages->get($input->get->eventId);
        if ($e->inClass == 0) { // Untick
            $e->setAndSave('inClass', 1);
        } else { // Tick
            $e->setAndSave('inClass', 0);
        }
    }
    // Toggle enroll group
    if (isset($input->get->form) && $input->get->form == 'enroll' && $input->get->teamId != '' && $input->get->groupId != '') {
        $selectedTeam = $pages->get($input->get->teamId);
        $selectedGroup = $pages->get($input->get->groupId);
        if ($selectedTeam->enrolledGroups->has($selectedGroup)) {
            $selectedTeam->enrolledGroups->remove($selectedGroup);
        } else {
            $selectedTeam->enrolledGroups->add($selectedGroup);
        }
        $selectedTeam->of(false);
        $selectedTeam->save();
    }
    // Use item
    if (isset($input->get->form) && $input->get->form == 'unpublish' && $input->get->usedItemHistoryPageId != '') {
        $historyPage = $pages->get($input->get->usedItemHistoryPageId);
        $player = $historyPage->parent("template=player");
        $usedItem = $historyPage->refPage;
        $player->of(false);
        if ($player->usabledItems->has($usedItem)) { // 'Used today' is ticked
            $player->usabledItems->remove($usedItem); // Remove item from player's usabledItems list
        } else { // Used today is unclicked
            $player->usabledItems->add($usedItem); // Restore item in player's usabledItems list
        }
        $player->save();
    }
    // Validate Book of Knowledge
    if (isset($input->get->form) && $input->get->form == 'unpublish' && $input->get->usedPending != '') {
        $pendingId = $input->get->usedPending;
        $pending = $pages->get("id=$pendingId");
        $player = $pending->player;
        $name = $player->name.'.'.$pendingId;
        if ($pending->isTrash()) { // 'Validated' is unclicked
            $pages->restore($pending); // Restore trashed pending lesson
            // Delete linked page in player's history
            $historyPage = $player->get("name=history")->get("linkedId=$pendingId");
            if ($historyPage) {
                $historyPage->delete();
            }
            // Reset scores
            $task = $pending->task;
            $tempPlayer = $pages->get("parent.name=tmp, login=$name");
            if ($tempPlayer) {
                $player->XP = $tempPlayer->XP;
                $player->HP = $tempPlayer->HP;
                $player->GC = $tempPlayer->GC;
                $player->level = $tempPlayer->level;
                $player->reputation = $tempPlayer->reputation;
                $player->yearlyKarma = $tempPlayer->yearlyKarma;
            }
            $player->of(false);
            $player->save();
            setGroupCaptain($player->id);
            if ($tempPlayer) {
                $tempPlayer->delete();
            }
        } else { // 'Validated' is ticked
            // Store previous player's state in temp page for restore possibility
            $tmpParent = $pages->get("name=tmp");
            $tempPlayer = $pages->clone($player, $tmpParent, false);
            $tempPlayer->setAndSave('login', $name);
            // Create task in player's history
            $task = $pending->task;
            $task->date = $pending->date;
            if ($task->is("name=extra-homework|very-extra-homework")) {
                $task->comment = 'Book of Knowledge use : '.$pending->refPage->title;
                $task->refPage = $pending->refPage;
                $task->linkedId = $pending->id;
                updateScore($player, $task, true);
                if ($pending) {
                    $pending->of(false);
                    $pending->trash();
                }
                setGroupCaptain($player->id);
            }
        }
    }
    // Delete page (pending lesson, fight request, ...) without scoring
    if (isset($input->get->form) && $input->get->form == 'deleteNotification' && $input->get->pageId != '') {
        $pageToDel = $pages->get($input->get->pageId);
        $pageToDel->trash();
    }
    if (isset($input->get->form) && $input->get->form == 'deleteFightRequest' && $input->get->pageId != '') {
        $player = $pages->get($input->get->pageId);
        $player->setAndSave('fight_request', '0');
    }

    if (isset($input->get->form) && $input->get->form == 'quickValidation' && $input->get->playerId != '' && $input->get->taskId != '') { // Teacher quick validation (to bypass adminTable)
        $player = $pages->get($input->get->playerId);
        $task = $pages->get($input->get->taskId);
        $task->comment = $input->get->comment;
        if (isset($input->get->customDate)) {
            $customDate = $input->get->customDate;
            $currentTime = date('H:i:s', time());
            $task->eDate = $customDate.' '.$currentTime;
        }
        updateScore($player, $task, true);
        setGroupCaptain($player->id);
    }

    if (isset($input->get->form) && $input->get->form == 'lessonRequest' && $input->get->playerId != '' && $input->get->lessonId != '') { // Teacher manually adds copied lesson (Book of Knowledge use)
        $player = $pages->get($input->get->playerId);
        $refPage = $pages->get($input->get->lessonId);
        $task = $refPage->task;
        if ($task->is("name=extra-homework|very-extra-homework")) {
            $task->comment = __("Good Copy work");
            $task->refPage = $refPage;
            $task->linkedId = false;
            // Only 1 pending lesson allowed for a player
            $already = $pages->get("name=book-knowledge-lessons, pending.player=$player");
            if (!$already || !$already->isTrash()) {
                $pages->unCacheAll();
                $pendingId = savePendingLesson($player, $task);
                $out = '<li>';
                $out .= '<span><a href="'.$player->url.'">'.$player->title.'</a> ['.$player->team->title.'] : '.$refPage->title.'</span>';
                $out .= ' <label for="unpublish_'.$player->id.'" class="btn btn-danger btn-xs"><input type="checkbox" id="unpublish_'.$pendingId.'" class="ajaxUnpublish" value="'.$pages->get('name=submitforms')->url.'?form=unpublish&usedPending='.$pendingId.'" /> validated today</label>';
                $out .= ' <a href="'.$pages->get('name=submitforms')->url.'?form=deleteNotification&pageId='.$pendingId.'" class="del">'.__('[Delete]').'</a>';
                $out .= '</li>';
                echo $out;
            }
        }
    }

    if (isset($input->get->form) && $input->get->form == 'fightRequest' && $input->get->playerId != '' && $input->get->monsterId != '') { // Teacher manually adds fight request
        $player = $pages->get($input->get->playerId);
        $monster = $pages->get($input->get->monsterId);
        if ($monster->is("template=exercise")) {
            // Only 1 pending lesson allowed for a player
            if ($player->fight_request == 0 || $player->fight_request == '') {
                $player->setAndSave('fight_request', $monster->id);
            }
            $out = '<li>';
            $out .= '<a href="'.$player->url.'">'.$player->title.'</a> ['.$player->team->title.'] : <a href="'.$pages->get("name=monsters")->url.'?id='.$player->fight_request.'&pages2pdf=1">'.$pages->get($player->fight_request)->title.'</a>';
            $out .= ' <button class="ajaxBtn btn btn-xs btn-danger" data-type="fightRequest" data-result="rr" data-url="'.$pages->get('name=submit-fight')->url.'?form=fightRequest&playerId='.$player->id.'&result=RR&monsterId='.$player->fight_request.'">RR</button>';
            $out .= ' <button class="ajaxBtn btn btn-xs btn-danger" data-type="fightRequest" data-result="r" data-url="'.$pages->get('name=submit-fight')->url.'?form=fightRequest&playerId='.$player->id.'&result=R&monsterId='.$player->fight_request.'">R</button>';
            $out .= ' <button class="ajaxBtn btn btn-xs btn-success" data-type="fightRequest" data-result="v" data-url="'.$pages->get('name=submit-fight')->url.'?form=fightRequest&playerId='.$player->id.'&result=V&monsterId='.$player->fight_request.'">V</button>';
            $out .= ' <button class="ajaxBtn btn btn-xs btn-success" data-type="fightRequest" data-result="vv" data-url="'.$pages->get('name=submit-fight')->url.'?form=fightRequest&playerId='.$player->id.'&result=VV&monsterId='.$player->fight_request.'">VV</button>';
            $out .= ' <a href="'.$pages->get('name=submitforms')->url.'?form=deleteFightRequest&pageId='.$player->id.'" class="del">'.__('[Delete]').'</a>';
            $out .= '</li>';
            echo $out;
        }
    }

    if (isset($input->get->form) && $input->get->form == 'manualTask' && $input->get->playerId != '' && $input->get->taskId != '') { // Personal Initiative in Decisions, memory potion...
        $player = $pages->get($input->get->playerId);
        $task = $pages->get($input->get->taskId);
        if (isset($input->get->type) && $input->get->type == 'memory') {
            if (isset($input->get->historyPageId) && $input->get->historyPageId != '') {
                // Validate Memory Potion
                $historyPage = $pages->get($input->get->historyPageId);
                $usedItem = $historyPage->refPage;
                if ($player->usabledItems->has($usedItem)) {
                    // Remove item from player's usabledItems list
                    $player->of(false);
                    $player->usabledItems->remove($usedItem);
                    $player->save();
                }
                // Update player's scores and save
                $task->comment = $historyPage->summary;
                if ($task->HP < 0) { // Failed mission
                    $task->comment .= " [failed].";
                } else {
                    $task->comment .= " [Successful].";
                }
                $task->refPage = $historyPage->refPage;
                $task->linkedId = $historyPage->id;
                updateScore($player, $task, true);
                setGroupCaptain($player->id);
            }
        }
        if ($task->name == 'personal-initiative') {
            $task->comment = "Well done 'Talk about [...]'";
            $task->refPage = '';
            $task->linkedId = false;
            updateScore($player, $task, true);
            setGroupCaptain($player->id);
        }
    }

    if (isset($input->get->form) && $input->get->form == 'deleteForm' && $input->get->eventId != '') { // Delete an event
        $event = $pages->get($input->get->eventId);
        // Limit to absence (no need to recalculate scores)
        if ($event->is("task.name=absent|abs")) {
            $pages->trash($event);
            clearFileCache("adminTable");
        }
    }

    if (isset($input->get->form) && $input->get->form == 'buyForm' && $input->get->playerId != '') { // Healing potion in Main Office, Discount in Decision...
        $player = $pages->get($input->get->playerId);
        $item = $pages->get($input->get->itemId);
        if ($item->is("template=place|people")) {
            $task = $pages->get("name='free'");
        } else {
            $task = $pages->get("name='buy'");
        }
        $task->comment = $item->title;
        $task->refPage = $item;
        if ($input->get->discount && $input->get->discount != '') {
            $task->linkedId = $input->get->discount;
        } else {
            $task->linkedId = false;
        }
        updateScore($player, $task, true);
        setGroupCaptain($player->id);
    }

    if ($input->post->adminTableSubmit) { // adminTableForm submitted
        $clearAdminTableCache = false;
        // Consider checked players only
        $checkedPlayers = $input->post->player;
        $checked = array_keys($checkedPlayers);
        $allNegPlayers = new pageArray();
        if (isset($input->post->customDate)) {
            $currentTime = date('H:i:s', time());
            $customDate = $input->post->customDate.' '.$currentTime;
        } else {
            $customDate = date('m/d/Y H:i:s', time());
        }
        // Record checked task for each player
        for ($i = 0; $i < count($checked); $i++) {
            list($playerId, $taskId, $linkedId) = explode('_', $checked[$i]);
            if (isset($linkedId)) {
                $comment = 'comment_'.$playerId.'_'.$taskId.'_'.$linkedId;
            } else {
                $comment = 'comment_'.$playerId.'_'.$taskId;
            }
            $player = $pages->get($playerId);
            $player->of(false);
            $task = $pages->get($taskId);
            $task->of(false);
            $task->comment = trim($input->post->$comment);
            if (isset($linkedId)) {
                $task->linkedId = $linkedId;
            } else {
                $task->linkedId = false;
            }
            $task->eDate = $customDate;
            if (isset($input->post->customRefPage)) {
                $task->refPage = $input->post->customRefPage;
            } else {
                $task->refPage = false;
                if ($task->HP < 0) { // Negative action, keep concerned players to check death later
                }
                $allNegPlayers->add($player);
            }
            // Update player's scores and save
            updateScore($player, $task, true);
            if ($task->is("name=abs|absent")) {
                $clearAdminTableCache = true;
            }
        }
        // Check death for each players having a negative action
        $allNegPlayers = $allNegPlayers->unique();
        foreach ($allNegPlayers as $p) {
            checkDeath($p, true);
        }
        setTeamSkill($player->team, 'captain', true);

        if ($clearAdminTableCache) {
            clearFileCache("adminTable");
        }
        // Check if HQ attack (thanks to last task since all players have same task)
        if ($task->is("name~=hq-attack") && isset($input->post->lastSave)) {
            $new = $player->team->hqFights->getNew();
            $new->of(false);
            $new->megaMonster = $task->refPage;
            $new->date = $customDate;
            preg_match("/([\d]+)\%/", $task->comment, $matches);
            if (isset($matches[0])) {
                $result = $matches[0];
            } else {
                $result = 0;
            }
            $new->result = $result;
            $new->save();
        }
        // Redirect to team page (in main.js, because doesn't work due to Ajax ?)
        /* $session->redirect($pages->get('/players')->url.$player->team->name); */
        if (isset($input->post->stayOnPage)) {
            $url = $stayOnPage;
        } else {
            $url = $pages->get('/players')->url.$player->team->name;
        }
        echo json_encode(array("sender" => "adminTable", "saved" => count($checked), "url" => $url));
    }

    if ($input->post->marketPlaceSubmit) { // marketPlaceForm submitted
        $checkedItems = $input->post->item;
        $playerId = $input->post->player;

        foreach ($checkedItems as $item => $state) {
            // Modify player's page
            $player = $pages->get($playerId);
            // Get item's data
            $refPage = $pages->get($item);

            // Update player's scores and save
            if ($refPage->is("template=place|people")) {
                $task = $pages->get("name=free");
            }
            if ($refPage->is("template=equipment|item")) {
                $task = $pages->get("name=buy");
            }
            $task->comment = $refPage->title;
            $task->refPage = $refPage;
            $task->linkedId = false;
            if ($task->id) {
                updateScore($player, $task, true);
                // No need to checkDeath, Marketplace can't cause death
            }
        }
        setGroupCaptain($player->id);
        // Redirect to MarketPlace (in main.js, because doesn't work due to Ajax ?)
        /* $session->redirect($pages->get('/shop')->url.$player->team->name); */
        $url = $pages->get('/shop')->url.$player->team->name;
        echo json_encode(array("sender" => "marketPlace", "url" => $url));
    }

    if ($input->post->donateFormSubmit || (isset($input->get->form) && $input->get->form == 'quickDonation' && $input->get->receiver != '' && $input->get->donator != '' && $input->get->amount != '')) { // donateForm submitted
        if ($input->get->form && $input->get->form == 'quickDonation') {
            $playerId = $input->get->donator;
            $amount = (int) $input->get->amount;
            $receiverId = $input->get->receiver;
            $extraComment = $input->get->extraComment;
        } else {
            $playerId = $input->post->donator;
            $amount = (int) $input->post->amount;
            $receiverId = $input->post->receiver;
            $extraComment = $input->post->extraComment;
        }
        $donator = $pages->get($playerId);
        $donator->of(false);
        $receiver = $pages->get($receiverId);
        $receiver->of(false);

        // Save donation
        if ($users->get("$donator->id")->is("roles=teacher|superuser") && ($user->hasRole('teacher') || $user->isSuperuser())) {
            $task = $pages->get("template=task, name=extra-donation");
            $task->comment = sprintf(__('%1$sGC received from teacher [%2$s]'), $amount, $extraComment);
            $task->refPage = $donator;
            $task->linkedId = false;
            if ($receiver->is("template=team")) {
                addToTeamChest($receiver, $amount);
            } else {
                updateScore($receiver, $task, true);
            }
        } else {
            if ($donator && $receiverId && $amount != 0 && $amount <= $donator->GC) { // If valid amount, modify donator's page
                if ($donator->is("template=team")) { // Teacher records a donation from a team chest
                    $task = $pages->get("template=task, name=team-solidarity");
                    $task->comment = sprintf(__('%1$sGC taken from %2$s team chest.'), $amount, $receiver->title);
                    $task->linkedId = false;
                    $task->refPage = $donator;
                    updateScore($receiver, $task, true);
                    $url = $pages->get('/players')->url.$receiver->team->name;
                } else {
                    $task = $pages->get("template=task, name=donation");
                    $task->comment = sprintf(__('%1$sGC donated to %2$s [%3$s].'), $amount, $receiver->title, $receiver->team->title);
                    $task->linkedId = false;
                    $task->refPage = $receiver;
                    updateScore($donator, $task, true);
                    // No need to checkDeath, Donation can't cause death
                    setGroupCaptain($donator->id);
                    /* $url = $pages->get('/players')->url.$donator->team->name; */
                    $url = $donation->url.$donator->team->name;
                }
                // Redirection to Team page (in main.js, because doesn't work due to Ajax ?)
                /* $session->redirect($pages->get("name=players")->url.$player->team->name); */
                echo json_encode(array("sender" => "marketPlace", "url" => $url));
            }
        }
    }
}

if (isset($input->get->form) && $input->get->form == 'updateTimestamp') {
    $user->setAndSave('lastMeetingHallActivity', time());
    $out =  showOnlineUsers();
    echo $out;
}
