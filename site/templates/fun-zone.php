<?php

namespace ProcessWire; /* lesson template */

include("./head.inc");

// Record visit to log file (to see if page is used)
if ($user->hasRole('player')) {
    $logText = $user->name;
    $log->save('fun-zone', $logText);
}

// Games sources
// http://www.doodle-art-alley.com/country-coloring-pages.html
// http://www.classroomdoodles.com/
// http://www.mazegenerator.net/
// http://www.supercoloring.com/sections/coloring-pages
// https://www.free-mandalas.net/themes/animals/?image=animals__mandala-giraffe__1

$out = '<div class="row">';
$out .= '<h2 class="text-center well">'.__("Welcome to the secure Fun Zone !").'</h2>';
$out .= '<h3 class="text-center">'.__("Print your game, have fun and decorate your classroom !").'</h3>';
$out .= '</div>';

if (isset($player) && $user->isLoggedin() || $user->isSuperuser() || $user->hasRole('teacher')) {
    if ($user->isSuperuser() || $user->hasRole('teacher')) {
        $player = $pages->get("parent.name=players, name=test");
    }

    if ($user->isSuperuser()) {
        $teacherName = 'flieutaud';
    } elseif ($user->hasRole("teacher")) {
        $teacherName = $user->name;
    } else {
        $teacherName = $headTeacher->name;
    }
    bd($teacherName);
    $funZoneDir = $config->paths->assets.'fun-zone/'.$teacherName.'/';
    $funZoneIndex = $funZoneDir.'index.txt';
    $games = [];

    $handle = fopen($funZoneIndex, "r");
    if ($handle) {
        while (($line = fgets($handle)) !== false) {
            list($path, $title) = explode(',', $line);
            list($type, $fileName) = explode('/', $path);
            switch ($type) {
                case 'coloring': $type = __('Coloring page');
                    break;
                case 'maze': $type = __('Maze page');
                    break;
                case 'wordsearch': $type = __('Wordsearch page');
                    break;
                default: break;
            }
            $game['type'] = $type;
            $game['path'] = $funZoneDir.$path;
            $game['title'] = $title.' - '.$type;
            array_push($games, $game);
        }
        fclose($handle);
    } else {
        // error opening the file.
        $out .= 'Error opening '.$funZoneIndex;
    }

    if (count($games) > 0) {
        shuffle($games);
        $out .= '<div class="row text-center">';
        $out .= '<div id="showInfo" data-href="'.$pages->get('name=ajax-content')->url.'"></div>';
        $out .= '<ul class="list list-inline">';
        $maxItems = 12; // Limit to 12 activities (for faster loading)
        $itemIndex = 0;
        foreach ($games as $g) {
            if ($itemIndex < $maxItems) {
                $out .= "<li>";
                $out .= '<img class="thumbnail game" src="'.$g['path'].'" width="" height="" data-gameTitle="'.$g['title'].'" data-gamePath="'.$g['path'].'" data-pdfUrl="'.$page->url.'?pages2pdf=1&gamePath='.$g['path'].'&playerId='.$player->id.'" data-toggle="tooltip" title="'.$g['title'].'" />';
                $out .= "</li>";
                $itemIndex++;
                if ($itemIndex == round($maxItems / 2)) {
                    $out .= '<br />';
                }
            }
        }
        $out .= "</ul>";
        $out .= '<a class="btn btn-primary" href="'.$page->url.'">'.sprintf(__('Pick %d other random activities'), $maxItems).'</a>';
        $out .= '</div>';
    } else {
        $out .= '<div class="row text-center">';
        if ($user->hasRole('player')) {
            $out .= '<p class="alert alert-warning">'.__("Your Fun Zone is empty. Please contact your teacher.").'</p>';
        } else {
            $out .= '<p class="alert alert-warning">'.__("Your Fun Zone is empty. Please contact the administrator.").'</p>';
        }
        $out .= '</div>';
    }

    echo $out;
} else {
    echo $noAuthMessage;
}

include("./foot.inc");
