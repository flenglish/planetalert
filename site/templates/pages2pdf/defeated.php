<?php

namespace ProcessWire;

$logo = '<img style="float: right;" src="'.$config->paths->assets.'paFiles/logo.png" width="100" height="100" /> ';
$footprint = '<img src="'.$config->paths->assets.'paFiles/img/footprint.png" width="150" height="150" /> ';
$allMonsters = $pages->find("parent.name=monsters, template=exercise, exerciseOwner.singleTeacher=$user, exerciseOwner.publish=1")->sort("level, name");

$out = '';

$playerId = $input->get->playerid;
if ($playerId && $playerId != '' && ($user->hasRole("teacher") || $user->isSuperuser())) { // Medals PDF
    $playerPage = $pages->get("id=$playerId");
    $tmpCache = $playerPage->children()->get("name=tmp");
    $medals = $tmpCache->tmpMonstersActivity->find('fightNb>=3, quality>0.3')->sort("-lastFightDate");
    if ($playerPage->is("design=3|4")) {
        $medals->sort("-monster.level, monster.name");
    } else {
        $medals->shuffle();
    }
    $classStyle = 'style'.$playerPage->design;
    $out .= '<h2 class="text-center well banner">';
    $out .= sprintf(_n('%1$s medal for %2$s [%3$s] !', '%1$s medals for %2$s [%3$s] !', $medals->count(), $playerPage->title, $playerPage->team->title), $medals->count(), $playerPage->title, $playerPage->team->title);
    $out .= '</h2>';

    $out .= '<ul class="list list-inline defeated '.$classStyle.'">';
    foreach ($medals as $p) {
        $out .= '<li>';
        $out .= '<div class="thumbnail">';
        $out .= '<img class="" data-toggle="tooltip" data-html="true" title="'.$p->monster->title.'<br />Level '.$p->monster->level.'<br />'.$p->monster->summary.'" src="'.$p->monster->image->getCrop('thumbnail')->url.'" alt="'.$p->monster->title.'." />';
        $out .= '<caption>';
        $out .= '<p class="myPanel level-'.$p->monster->level.'">';
        $out .= $p->monster->title;
        $out .= '<span class="detailled"><br />'.__('Level').' '.$p->monster->level.'</span>';
        $out .= '</p>';
        $out .= '</caption>';
        $out .= '</div>';
        $out .= '</li>';
    }
    $out .= '</ul>';
} else { // Fight requests PDF (for copybook)
    $out .= '<h2 style="text-align: center;">';
    $out .= $logo;
    $out .= __("My Fight Requests");
    $out .= '<hr /><small>'.__("Collect monsters by making SUCCESSFUL fights !").'</small>';
    $out .= '</h2>';

    $nbMonsters = $allMonsters->count();
    $nbPages = ceil($nbMonsters / 48);

    for ($k = 0; $k < $nbPages; $k++) {
        if ($k == 0) {
            $nbRows = 7;
        } else {
            $nbRows = 8;
        }
        $out .= '<table class="" style="border: 0px;">';
        for ($i = 0; $i < $nbRows; $i++) {
            $out .= '<tr style="">';
            for ($j = 0; $j < 8; $j++) {
                $monsterIndex = ($k * 56) + (($i * 8) + $j);
                $out .= '<td style="border:0px;">';
                $out .= $footprint;
                if ($allMonsters->eq($monsterIndex)) {
                    $out .= '<span style="font-size: 12px;">(level '.$allMonsters->eq($monsterIndex)->level.')</span>';
                } else {
                    $out .= '<p>&nbsp;</p>';
                }
                $out .= '</td>';
            }
            $out .= '</tr>';
            $out .= '<tr>';
            for ($j = 0; $j < 8; $j++) {
                $out .= '<td style="border:0px;">';
                $monsterIndex = ($k * 56) + (($i * 8) + $j);
                if ($allMonsters->eq($monsterIndex)) {
                    $out .= '<span style="font-size: 16px;">'.$monsterIndex.'-'.$allMonsters->eq($monsterIndex)->title.'</span>';
                } else {
                    $out .= '<p>&nbsp;</p>';
                }
                $out .= '</td>';
            }
            $out .= '</tr>';
            if ($i < $nbRows - 1) {
                $out .= '<tr>';
                $out .= '<td style="border: 0px;">&nbsp;</td>';
                $out .= '</tr>';
            }
        }
        $out .= '</table>';
        if ($k < $nbPages - 1) {
            $out .= '<pagebreak />';
        }
    }
}

echo $out;
