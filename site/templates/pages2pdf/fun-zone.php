<?php

namespace ProcessWire;

$out = '';

$logo = '<img style="float: left;" src="'.$config->paths->assets.'paFiles/logo.png" width="100" height="100" /> ';
$gamePath = $input->get("gamePath");

// Get user's avatar
if ($user->isLoggedin() || !$user->isSuperuser()) {
    $player = $pages->get("login=$user->name");
    if ($player->avatar) {
        $avatar =  '<img style="float: right;" src="'.$player->avatar->getCrop("thumbnail")->url.'" alt="Avatar" />';
    } else {
        $avatar = '<Avatar>';
    }
}

$out .= $logo.$avatar;
$out .= '<h1>'.__("Have fun with Planet Alert !").'</h1>';
$out .= '<div style="text-align: center;">';
$out .= '<img style="max-height: 600px; max-width: 800px;" src="'.$gamePath.'" />';
$out .= '</div>';
$out .= '<br />';
$out .= '<br />';
$out .= '<h4 style="text-align: center;">'.__("Give your finished activity to your teacher to decorate your classroom and get a credit !").'</h4>';
$out .= '<hr style="margin: 10pt" />';
$out .= $logo;
$out .= '<h3 style="text-align: center;">✓ '.__("Validated mission ! Good job !").'</h3>';
$out .= '<h3 style="text-align: center"> → _________________________________________________</h3>';

echo $out;
