<?php

namespace ProcessWire;

$schoolYear = $pages->get("template=period, name=school-year");
if (!$config->ajax) {
    include("./head.inc");

    $activityUrl = $pages->get("name=activity")->url;

    if (isset($player) && $user->isLoggedin() || $user->isSuperuser() || $user->hasRole('teacher')) {
        $getPlayerId = '';
        // Test if player has unlocked Memory helmet (only training equipment for the moment)
        // or if admin has forced it in Team options
        if ($user->isSuperuser() || $user->hasRole('teacher') || $player->team->forceHelmet == 1) {
            if ($user->isSuperuser() || $user->hasRole('teacher')) { // Set player
                if ($input->get->playerId) {
                    $playerId = $input->get->playerId;
                    $player = $pages->get("id=$playerId");
                    $getPlayerId = '?playerId='.$playerId;
                } else {
                    $player = $pages->get("parent.name=players, name=test");
                }
                $headTeacher = getHeadTeacher($player);
                if ($player->team->forceHelmet == 1) {
                    $helmet = $pages->get("name=memory-helmet");
                } else {
                    $helmet = $player->equipment->get("name=memory-helmet");
                }
                if ($player->team->forceVisualizer == 1) {
                    $visualizer = $pages->get("template=item, name=visualizer");
                } else {
                    $visualizer = $player->equipment->get("name=visualizer");
                }
            } else {
                $helmet = $pages->get("template=item, name=memory-helmet");
                $visualizer = $pages->get("template=item, name=visualizer");
            }
        } else {
            $helmet = $player->equipment->get("name=memory-helmet");
            $visualizer = $pages->get("template=item, name=visualizer");
        }
        if ($helmet) { // Display training catalogue
            $out = '<div>';
            $ajaxContentUrl = $pages->get("name=ajax-content")->url;
            $out .= '<div id="showInfo" data-href="'.$ajaxContentUrl.'"></div>';
            // Set all available monsters
            // Check if player has the Visualizer (or forced by admin)
            if ($player->equipment->has("name=visualizer") || $player->team->forceVisualizer == 1) {
                $allMonsters = $pages->find("parent.name=monsters, template=exercise, exerciseOwner.singleTeacher=$headTeacher, exerciseOwner.publish=1")->sort("name");
                $allMonstersNb = $allMonsters->count();
            } else {
                $allMonsters = $pages->find("parent.name=monsters, template=exercise, exerciseOwner.singleTeacher=$headTeacher, exerciseOwner.publish=1, special=0")->sort("name");
                $hiddenMonstersNb = $pages->find("parent.name=monsters, template=exercise, (exerciseOwner.singleTeacher=$headTeacher, exerciseOwner.publish=1), special=1")->count();
            }
            // Check if fightRequest
            if ($player->fight_request == 0 || $player->fight_request == '') {
                $request = false;
            } else {
                $request = $player->fight_request;
            }
            // Load challenges
            if ($player->team->is("name!=no-team")) {
                $allChallenges = $pages->get("template=teacherProfile, name=$headTeacher->name")->teamChallenges->get("team=$player->team")->linkedMonsters;
            } else {
                $allChallenges = false;
            }
            // Prepare all monsters
            foreach ($allMonsters as $m) {
                setMonster($player, $m);
            }
            $availableNb = $allMonsters->find("isTrainable=1")->count();
            $today = new \DateTime("today");
            $newInterval = new \DateInterval('P15D');
            $limitDate = strtotime($today->sub($newInterval)->format('Y-m-d'));
            $newMonsters = $allMonsters->find("published>=$limitDate");
            // Store allMonsters in session cache on non-ajax page load
            $cache->save("monstersList_".$player->login, $allMonsters);
            $cache->save("newMonsters", $newMonsters);
            $allMonstersNb = $allMonsters->count();
            $out .= '<div class="well">';
            $out .= '<h2 class="text-center">';
            $out .= '<span class="pull-left glyphicon glyphicon-question-sign" data-toggle="tooltip" data-html="true" title="'.$page->summary.'"></span>';
            $out .= '<span class="blink">';
            $out .= __("Program your helmet !");
            $out .= "</span>";
            $out .= '<span class="avatarContainer pull-right">';
            if (isset($player) && $player->avatar) {
                $out .= '<img class="helmetAvatar" src="'.$player->avatar->getCrop("thumbnail")->url.'" alt="'.$player->title.'." />';
            }
            if ($helmet->image) {
                $out .= '<img class="helmet superpose" src="'.$helmet->image->getCrop("thumbnail")->url.'" alt="Helmet." />';
            }
            if ($player->equipment->has("name=visualizer") || $player->team->forceVisualizer == 1) {
                $out .= '<img class="helmet superpose" src="'.$visualizer->image->getCrop("mini")->url.'" alt="Visualizer" />';
            }
            $out .= '</span>';
            $out .= '</h2>';
            if ($user->isSuperuser() || $user->hasRole('teacher')) {
                $out .= '<h3 class="text-center"><span class="label label-danger">';
                $out .= sprintf(__('Teacher\'s access for player %s'), $player->title);
                $out .= '</span></h3>';
            }
            $out .= '<p class="text-center">';
            if (!isset($hiddenMonstersNb)) {
                $out .= sprintf(__('There are %1$s detected monsters thanks to your visualizer.'), $allMonstersNb);
            } else {
                $out .= sprintf(__('There are only %d detected monsters.'), $allMonstersNb);
                /* $out .= ' '.sprintf(__('%1$s monsters are absent because you don\'t have the visualizer.'), $hiddenMonstersNb); */
            }
            $out .= ' '.sprintf(__('[%d monsters are available today]'), $availableNb);
            $out .= '</p>';
            if (isset($hiddenMonstersNb)) { // Display helpAlert for Electronic visualizer
                $helpAlert = true;
                $helpTitle = __("Some monsters are absent !");
                if ($visualizer && $visualizer->image) {
                    $helpMessage = '<img src="'.$visualizer->image->getCrop("small")->url.'" alt="image" /> ';
                } else {
                    $helpMessage = '';
                }
                $helpMessage .= '<h4>'.sprintf(__('%1$s monsters are absent because you don\'t have the visualizer.'), $hiddenMonstersNb).'</h4>';
            }
            include("./helpAlert.inc.php");
            $out .= '<section class="configHelmet">';
            if ($input->urlSegment1 == '') { // No selection
                if ($player->team->is("name!=no-team")) {
                    $out .= '<div class="frame">';
                    $out .= __("Today's challenges !").' ';
                    if ($player->team->classActivity == 1) {
                        $out .= ' <span class="badge">'.__("In class activity !").'</span>';
                    } else {
                        $out .= ' <span class="badge">'.__("Recommended training !").'</span>';
                        $out .= '<span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" title="'.__("Recommended training ! Validate a solo-mission by doing the complete challenges !").'"></span>';
                    }
                    if ($allChallenges->count() > 0) {
                        $out .= '<div class="row">';
                        $nbChallenge = 0;
                        foreach ($allChallenges as $m) {
                            // Check if challenge done today !
                            list($utGain, $inClassUtGain) = utGain($m, $player, date("Y-m-d 00:00:00"), date("Y-m-d 23:59:59"));
                            $out .= '<div class="col-xs-12 col-md-4 text-center">';
                            $out .= '<p>';
                            if (($utGain > 0 && $player->team->classActivity == 0) || ($inClassUtGain > 0 && $player->team->classActivity == 1)) {
                                $nbChallenge++;
                                $out .= '<span class="label label-success">';
                                $out .= '<span class="glyphicon glyphicon-thumbs-up"></span> '.__("Done !");
                                if ($player->team->classActivity == 1) {
                                    $out .= ' '.sprintf(__('%dUT'), $inClassUtGain);
                                } else {
                                    $out .= ' '.sprintf(__('%dUT'), $utGain);
                                }
                                $out .= '</span>';
                            }
                            $out .= '</p>';
                            if (($utGain == 0 && $player->team->classActivity == 0) || ($inClassUtGain == 0 && $player->team->classActivity == 1)) {
                                $out .= '<p>';
                                if ($m->image) {
                                    $out .= '<img class="squeeze" src="'.$m->image->getCrop("small")->url.'" alt="no-img" data-toggle="tooltip" title="'.$m->summary.'" />';
                                }
                                $out .= ' <a class="btn btn-primary" href="'.$m->url.'train"><i class="glyphicon glyphicon-headphones" data-toggle="tooltip" title="'.__("Activate helmet !").'" onmouseenter="$(this).tooltip(\'show\');"></i></a>';
                                $ownerId = $m->created_users_id;
                                $owner = $users->get($ownerId);
                                if ($owner->is("name=flieutaud") && $m->type->name != 'categorize') { // Only for my exercises at the moment (which are English learning exercises)
                                    $out .= ' <a class="btn btn-xs btn-primary audioMonster" data-id="'.$m->id.'" href="#" data-toggle="tooltip" title="'.__('Use the Audio module').'" onmouseenter="$(this).tooltip(\'show\');"><span class="glyphicon glyphicon-volume-up"></span></a>';
                                }
                                if ($m->utGain > 0) {
                                    $out .= ' <a href="'.$activityUrl.$m->id.'/'.$player->id.'" data-type="analyzer" class="ajaxBtn btn btn-primary btn-xs" data-url="'.$activityUrl.$m->id.'/'.$player->id.'" data-toggle="tooltip" onmouseenter="$(this).tooltip(\'show\');" title="'.__('Activity analysis').'"><span class="glyphicon glyphicon-signal"></span></a> ';
                                }
                                $formerRequest = $pages->get("has_parent=$player, template=event, task.name=fight-vv, inClass=1, refPage=$m, date>$schoolYear->dateStart");
                                if ($formerRequest->id) {
                                    $out .= ' <span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="'.__('You have already defeated this monster this school year.').'" onmouseenter="$(this).tooltip(\'show\');"></span>';
                                } else {
                                    if ($request == 0) {
                                        $msg = sprintf(__("Fight request for %s"), $m->title);
                                        $out .= ' <span><a class="btn btn-danger btn-xs fightRequestConfirm" href="'.$page->url.'" data-href="'.$pages->get("name=submitforms")->url.'?form=fightRequest&monsterId='.$m->id.'&playerId='.$player->id.'" data-msg="'.$msg.'" data-reload="true"><i class="glyphicon glyphicon-education" data-toggle="tooltip" title="'.__("Ask teacher for an in-class Fight!").'" onmouseenter="$(this).tooltip(\'show\');"></i></a></span>';
                                    } elseif ($request == $m->id) {
                                        $out .= ' <span class="glyphicon glyphicon-ok-circle" data-toggle="tooltip" title="'.__('Your teacher has already been warned about this request.').'" onmouseenter="$(this).tooltip(\'show\');"></span>';
                                    } else {
                                        $out .= ' <span class="glyphicon glyphicon-ban-circle" data-toggle="tooltip" title="'.__('Your teacher has already been warned about a request.').'" onmouseenter="$(this).tooltip(\'show\');"></span>';
                                    }
                                }
                                $out .= '</p>';
                                $out .= '<p class="squeeze">';
                                $out .= '<span class="label label-danger">'.$m->title.'</span>';
                                $out .= '</p>';
                            } else {
                                $out .= '<p>';
                                if ($m->image) {
                                    $out .= '<img class="" src="'.$m->image->getCrop("mini")->url.'" alt="no-img" data-toggle="tooltip" data-html="true" title="'.$m->title.'<br />'.$m->summary.'" />';
                                }
                                $formerRequest = $pages->get("has_parent=$player, template=event, task.name=fight-vv, inClass=1, refPage=$m, date>$schoolYear->dateStart");
                                if ($formerRequest->id) {
                                    $out .= ' <span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="'.__('You have already defeated this monster this school year.').'" onmouseenter="$(this).tooltip(\'show\');"></span>';
                                } else {
                                    if ($request == 0) {
                                        $msg = sprintf(__("Fight request for %s"), $m->title);
                                        $out .= ' <span><a class="btn btn-danger btn-xs fightRequestConfirm" href="'.$page->url.'" data-href="'.$pages->get("name=submitforms")->url.'?form=fightRequest&monsterId='.$m->id.'&playerId='.$player->id.'" data-msg="'.$msg.'" data-reload="true"><i class="glyphicon glyphicon-education" data-toggle="tooltip" title="'.__("Ask teacher for an in-class Fight!").'" onmouseenter="$(this).tooltip(\'show\');"></i></a></span>';
                                    } elseif ($request == $m->id) {
                                        $out .= ' <span class="glyphicon glyphicon-ok-circle" data-toggle="tooltip" title="'.__('Your teacher has already been warned about this request.').'" onmouseenter="$(this).tooltip(\'show\');"></span>';
                                    } else {
                                        $out .= ' <span class="glyphicon glyphicon-ban-circle" data-toggle="tooltip" title="'.__('Your teacher has already been warned about a request.').'" onmouseenter="$(this).tooltip(\'show\');"></span>';
                                    }
                                }
                                $out .= '</p>';
                                /* $out .= '<p>'; */
                                /* $out .= '<small><span class="label label-success">'.$m->title.'</span></small>'; */
                                /* $out .= '</p>'; */
                            }
                            $out .= '</div>';
                        }
                        if ($nbChallenge == $allChallenges->count()) {
                            $out .= '<h3><span class="glyphicon glyphicon-thumbs-up"></span> ';
                            $out .= __("Congratulations ! You have completed all challenges for today !");
                            $out .= '</h3>';
                        }
                        $out .= '</div>';
                    } else {
                        $out .= ' → '.__("No challenge for today.");
                    }
                    $out .= '</div>';
                }
                $unEquippedPlayers = $pages->find("parent.name=players, team=$player->team")->not("equipment.name=memory-helmet");
                if ($unEquippedPlayers->count() > 0 && $player->team->forceHelmet == 0 && $player->team->is("name!=no-team")) {
                    $unEquippedPlayersList = $unEquippedPlayers->implode(', ', '{title}');
                    $out .= '<div class="alert alert-warning">';
                    $out .= '<h2>'.__('You are limited to today\'s challenges above because some players in your team don\'t have the Memory Helmet ! To unlock choices possibilities, ALL team players MUST be equipped with the Memory Helmet !').'</h2>';
                    $out .= '<h2>So think as a team : go and help them ! </h2>';
                    $out .= '<h3>['.__('NO Memory Helmet for : ').$unEquippedPlayersList.']</h3>';
                    $out .= '</div>';
                } else {
                    $out .= '<div class="frame">';
                    $out .= __("Quick selection ?");
                    $out .= '<div class="btn-toolbar">';
                    $out .= '<div class="btn-group btn-group-lg" role="group">';
                    /* $out .= '<a href="'.$page->url.'random" class="btn btn-primary pgHelmet">'.__("Personal recommandation").'</a>&nbsp;'; // TODO */
                    if ($availableNb > 0) {
                        $out .= '<a href="'.$page->url.'random/'.$getPlayerId.'" class="btn btn-primary pgHelmet">'.__("Random selection").'</a>&nbsp;';
                    }
                    $out .= '</div>';
                    $out .= '<div class="btn-group btn-group-lg" role="group">';
                    $out .= '<a href="'.$page->url.'never'.$getPlayerId.'" class="btn btn-primary pgHelmet">'.__("Never trained").'</a>';
                    $out .= '<a href="'.$page->url.'available'.$getPlayerId.'" class="btn btn-primary pgHelmet">'.__("Available today").'</a>';
                    $out .= '</div>';
                    $out .= '<div class="btn-group btn-group-lg" role="group">';
                    $out .= '<a href="'.$page->url.'level/1'.$getPlayerId.'" class="btn btn-primary pgHelmet">Level 1</a>';
                    $out .= '<a href="'.$page->url.'level/2'.$getPlayerId.'" class="btn btn-primary pgHelmet">Level 2</a>';
                    $out .= '<a href="'.$page->url.'level/3'.$getPlayerId.'" class="btn btn-primary pgHelmet">Level 3</a>';
                    $out .= '</div>';
                    $out .= '<div class="btn-group btn-group-lg" role="group">';
                    $out .= '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                    $out .= __("Name selection").' <span class="caret"></span>';
                    $out .= '</button>';
                    $out .= '<ul class="dropdown-menu monsterSelection">';
                    $allCat = new pageArray();
                    foreach ($allMonsters as $m) {
                        $out .= '<li><a class="pgHelmet" href="'.$page->url.'monster/'.$m->id.'/'.$getPlayerId.'">'.$m->title.'</a></li>';
                        foreach ($m->topic as $c) {
                            $allCat->add($c);
                        }
                        $allCat->sort("name");
                    }
                    $out .= '</ul>';
                    $out .= '</div>';
                    $out .= '<div class="btn-group btn-group-md" role="group">';
                    $out .= '<a href="'.$page->url.'notAvailable'.$getPlayerId.'" class="btn btn-primary pgHelmet">'.__("Not available today").'</a>';
                    $out .= '</div>';
                    if ($newMonsters->count() > 0) {
                        $out .= '<div class="btn-group btn-group-md" role="group">';
                        $out .= '<a href="'.$page->url.'new'.$getPlayerId.'" class="blink btn btn-primary pgHelmet">'.__("New monsters !").'</a>';
                        $out .= '</div>';
                    }
                    $out .= '</div>';
                    $out .= '</div>';
                    $out .= '<div class="frame">';
                    $out .= __("Topic selection ?");
                    $out .= '<ul class="list list-unstyled col6">';
                    foreach ($allCat as $c) {
                        $out .= '<li class="text-left"><a href="'.$c->id.$getPlayerId.'" class="label label-danger pgHelmet topics">'.$c->title.'</a></li>';
                    }
                    $out .= '</ul>';
                    $out .= '</div>';
                }
            }
            $out .= '</section>';

            $out .= '<section id="trainingList" class="row" data-href="'.$page->url.'"></section>';

            echo $out;

            echo '</div>';
            echo '</div>';
        }
    } else {
        echo $noAuthMessage;
    }
    $pages->unCacheAll();
    include("./foot.inc");
} else { // Load selected training possibilities
    if (!$user->isSuperuser()) {
        if ($user->hasRole('player')) { // Get logged in player
            $headTeacher = getHeadTeacher($user);
            $user->language = $headTeacher->language;
            $player = $pages->get("parent.name=players,template=player, login=$user->name");
            $allCachedMonsters = $cache->get("monstersList_".$user->name); // Read from cache
        } else {
            if ($input->get->playerId) {
                $playerId = $input->get->playerId;
                $player = $pages->get("id=$playerId");
                $allCachedMonsters = $cache->get("monstersList_".$player->login); // Read from cache
            } else {
                $player = $pages->get("parent.name=players,template=player, name=test");
                $allCachedMonsters = $cache->get("monstersList_".$player->login); // Read from cache
            }
            $headTeacher = getHeadTeacher($player);
        }
    } else {
        $player = $pages->get("template=player, name=test");
    }
    foreach ($allCachedMonsters as $m) {
        setMonster($player, $m);
    }
    if (!$user->isSuperuser()) {
        $availableNb = $allCachedMonsters->find("isTrainable=1")->count();
        switch ($input->urlSegment1) {
            case 'new':
                $allMonsters = $cache->get("newMonsters"); // Read from cache
                $title = '<h3>'.__("Newly published monsters").' → ';
                $title .= sprintf(_n('%d result', '%d results', $allMonsters->count()), $allMonsters->count()).'</h3>';
                break;
            case 'random':
                if ($availableNb > 3) {
                    $nbSelected = 3;
                } else {
                    $nbSelected = $availableNb;
                }
                $allMonsters = $allCachedMonsters->find("isTrainable=1")->findRandom($nbSelected)->sort("level");
                $title = '';
                break;
            case 'available':
                $selector = 'isTrainable=1';
                $allMonsters = $allCachedMonsters->find($selector)->sort("name, level");
                $title = '<h3>';
                $title .= sprintf(_n('%d result', '%d results', $allMonsters->count()), $allMonsters->count());
                $title .= ' <button class="btn-xs btn-primary limitLevel" data-level="0">'.__('All results').'</button> ';
                $title .= ' <button class="btn-xs btn-primary limitLevel" data-level="1">'.__('Level 1 only').'</button> ';
                $title .= '<button class="btn-xs btn-primary limitLevel" data-level="2">'.__('Level 2 only').'</button> ';
                $title .= '<button class="btn-xs btn-primary limitLevel" data-level="3">'.__('Level 3 only').'</button> ';
                $title .= '</h3>';
                break;
            case 'notAvailable':
                $selector = 'isTrainable=0';
                $allMonsters = $allCachedMonsters->find($selector)->sort("waitForTrain, name, level");
                $title = '<h3>'.sprintf(_n('%d result', '%d results', $allMonsters->count()), $allMonsters->count()).'</h3>';
                break;
            case 'level':
                $level = $input->urlSegment2;
                $selector = 'level='.$level;
                $allMonsters = $allCachedMonsters->find($selector)->sort("title");
                $title = '<h3>'.sprintf(_n('%d result', '%d results', $allMonsters->count()), $allMonsters->count()).'</h3>';
                break;
            case 'monster':
                $monsterId = $input->urlSegment2;
                $selector = 'id='.$monsterId;
                $allMonsters = $allCachedMonsters->find($selector);
                $title = '';
                break;
            case 'never':
                $tmpCache = $player->children()->get("name=tmp");
                $allTrainedIds = [];
                foreach ($tmpCache->tmpMonstersActivity as $p) {
                    array_push($allTrainedIds, $p->monster->id);
                }
                $allTrainedIds = implode('|', $allTrainedIds);
                $selector = 'id!='.$allTrainedIds;
                $allMonsters = $allCachedMonsters->find($selector)->sort("level");
                $title = '<h3>';
                $title .= sprintf(_n('%d result', '%d results', $allMonsters->count()), $allMonsters->count());
                $title .= ' <button class="btn-xs btn-primary limitLevel" data-level="0">'.__('All results').'</button> ';
                $title .= ' <button class="btn-xs btn-primary limitLevel" data-level="1">'.__('Level 1 only').'</button> ';
                $title .= '<button class="btn-xs btn-primary limitLevel" data-level="2">'.__('Level 2 only').'</button> ';
                $title .= '<button class="btn-xs btn-primary limitLevel" data-level="3">'.__('Level 3 only').'</button> ';
                $title .= '</h3>';
                break;
            default:
                $selector = 'topic.id='.$input->urlSegment1;
                $allMonsters = $allCachedMonsters->find($selector)->sort("level");
                $title = '<h3>';
                $title .= sprintf(_n('%d result', '%d results', $allMonsters->count()), $allMonsters->count());
                $title .= ' <button class="btn-xs btn-primary limitLevel" data-level="0">'.__('All results').'</button> ';
                $title .= ' <button class="btn-xs btn-primary limitLevel" data-level="1">'.__('Level 1 only').'</button> ';
                $title .= '<button class="btn-xs btn-primary limitLevel" data-level="2">'.__('Level 2 only').'</button> ';
                $title .= '<button class="btn-xs btn-primary limitLevel" data-level="3">'.__('Level 3 only').'</button> ';
                $title .= '</h3>';
        }

        $activityUrl = $pages->get("name=activity")->url;
        $out = '';
        $out .= '<p class="text-center"><button id="configHelmetBtn" class="btn btn-danger btn-block"><span class="glyphicon glyphicon-cog"></span> '.__("Show/Hide programming options").'</button></p>';
        if ($player->fight_request == 0 || $player->fight_request == '') {
            $request = false;
        } else {
            $request = $player->fight_request;
        } // Check if fightRequest
        $notAvailable = new pageArray();
        $out .= '<div class="container">';
        $out .= $title;
        $out .= '<section class="row display-flex">';
        $today = new \DateTime("today");
        foreach ($allMonsters as $m) {
            $topics = $m->topic->implode(', ', '{title}');
            if (($user->hasRole("teacher") || $user->isSuperuser()) && !isset($input->get->playerId)) { // Never trained (for admin or teachers) if not visiting a player's training zone
                $m->isTrainable = 1;
                $m->lastTrainingInterval = -1;
                $m->waitForTrain = 0;
            }
            if ($m->bestTrainedPlayerId != 0) {
                $bestTrained = $pages->get($m->bestTrainedPlayerId);
                $m->bestTrainedTitle = $bestTrained->title;
                $m->bestTrainedTeam = $bestTrained->team->title;
                if ($m->bestTrainedPlayerId == $player->id) {
                    $m->isBestTrained = true;
                } else {
                    $m->isBestTrained = false;
                }
            }
            if ($m->bestTimePlayerId != 0) {
                $master = $pages->get($m->bestTimePlayerId);
                $m->bestTimePlayerTitle = $master->title;
                $m->bestTimeTeam = $master->team->title;
                if ($m->bestTrainedPlayerId == $player->id) {
                    $m->isMaster = true;
                } else {
                    $m->isMaster = false;
                }
            }
            switch ($m->level) {
                case '1': $class = 'success';
                    break;
                case '2': $class = 'warning';
                    break;
                case '3': $class = 'danger';
                    break;
                default: $class = 'primary';
            }
            if ($m->isTrainable == 1) {
                $out .= '<div class="col-xs-12 col-md-4">';
                $out .= '<div class="monsterPanel level'.$m->level.'">';
                $out .= '<div class="actionBar lead col-xs-12 col-md-12">';
                $out .= '<span class="badge pull-left">'.__("Level").' '.$m->level.'</span>';
                // Find # of days compared to today to set 'New' indicator
                $date2 = new \DateTime(date("Y-m-d", $m->published));
                $interval = $today->diff($date2);
                if ($interval->days <= 15) {
                    $out .= ' <span class="badge">'.__("New").'</span>';
                }
                if ($m->special) {
                    $out .= ' <span class="badge">'.__("Detected").' !</span>';
                }
                $out .= ' <span>'.$m->title.'</span>';
                $out .= '<span class="pull-right">';
                $out .= ' <a class="btn btn-primary btn-md" href="'.$m->url.'train"><i class="glyphicon glyphicon-headphones" data-toggle="tooltip" title="'.__("Activate helmet !").'" onmouseenter="$(this).tooltip(\'show\');"></i></a>';
                $ownerId = $m->created_users_id;
                $owner = $users->get($ownerId);
                if ($owner->is("name=flieutaud") && $m->type->name != 'categorize') { // Only for my exercises at the moment (which are English learning exercises)
                    $out .= ' <a class="btn btn-md btn-primary audioMonster" data-id="'.$m->id.'" href="#" data-toggle="tooltip" title="'.__('Use the Audio module').'" onmouseenter="$(this).tooltip(\'show\');"><span class="glyphicon glyphicon-volume-up"></span></a>';
                }
                $formerRequest = $pages->get("has_parent=$player, template=event, task.name=fight-vv, inClass=1, refPage=$m, date>$schoolYear->dateStart");
                if ($formerRequest->id) {
                    $out .= ' <span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="'.__('You have already defeated this monster this school year.').'" onmouseenter="$(this).tooltip(\'show\');"></span>';
                } else {
                    if ($request == 0) {
                        $msg = sprintf(__("Fight request for %s"), $m->title);
                        $out .= ' <span><a class="btn btn-danger btn-md fightRequestConfirm" href="'.$page->url.'" data-href="'.$pages->get("name=submitforms")->url.'?form=fightRequest&monsterId='.$m->id.'&playerId='.$player->id.'" data-msg="'.$msg.'" data-reload="true"><i class="glyphicon glyphicon-education" data-toggle="tooltip" title="'.__("Ask teacher for an in-class Fight!").'" onmouseenter="$(this).tooltip(\'show\');"></i></a></span>';
                    } elseif ($request == $m->id) {
                        $out .= ' <span class="glyphicon glyphicon-ok-circle" data-toggle="tooltip" title="'.__('Your teacher has already been warned about this request.').'" onmouseenter="$(this).tooltip(\'show\');"></span>';
                    } else {
                        $out .= ' <span class="glyphicon glyphicon-ban-circle" data-toggle="tooltip" title="'.__('Your teacher has already been warned about a request.').'" onmouseenter="$(this).tooltip(\'show\');"></span>';
                    }
                }
                $out .= '</span>';
                $out .= '</div>';
                $out .= '<div class="row">';
                $out .= '<div class="col-xs-12 col-md-4">';
                if ($m->image) {
                    $out .= '<img class="img-thumbnail" src="'.$m->image->getCrop("thumbnail")->url.'" title="" alt="no-img" />';
                }
                $out .= '</div>';
                $out .= '<div class="col-xs-12 col-md-8 monsterPanelBody">';
                $out .= '<p>';
                $out .= __("UT gained").' : ';
                if (isset($player)) {
                    if ($m->utGain > 0) {
                        $out .= '<span class="label label-success"><span class="glyphicon glyphicon-thumbs-up"></span> +'.$m->utGain.'</span> ';
                    } else {
                        $out .= '<span class="label label-danger"><span class="glyphicon glyphicon-thumbs-down"></span> 0</span> ';
                    }
                }
                $out .= '</p>';
                $out .= '<p>';
                $out .= __("Last training session").' : ';
                if (isset($player)) {
                    if ($m->lastTrainingInterval != '-1') {
                        $out .= $m->lastTrainingInterval;
                        $out .= ' <a href="'.$activityUrl.$m->id.'/'.$player->id.'" data-type="analyzer" class="ajaxBtn btn btn-primary btn-xs" data-url="'.$activityUrl.$m->id.'/'.$player->id.'" data-toggle="tooltip" onmouseenter="$(this).tooltip(\'show\');" title="'.__('Activity analysis').'"><span class="glyphicon glyphicon-signal"></span></a> ';
                    } else {
                        $out .= '-';
                    }
                }
                $out .= '</p>';
                $out .= '<hr />';
                $out .= '<p>';
                $out .= '<span>'.__("Most trained").' : ';
                if ($m->bestTrainedPlayerId != 0) {
                    if ($m->isBestTrained) {
                        $class = 'success';
                    } else {
                        $class = 'primary';
                    }
                    $out .= '<span class="label label-'.$class.'">'.$m->best.' '.__("UT").' - '.$m->bestTrainedTitle.' ['.$m->bestTrainedTeam.']</span>';
                } else {
                    $out .= __("Nobody yet.");
                }
                $out .= '</span>';
                $out .= '</p>';
                $out .= '<p>';
                $out .= '<span>'.__("Master time").' : ';
                if ($m->bestTimePlayerTitle) {
                    if ($m->isMaster) {
                        $class = 'success';
                    } else {
                        $class = 'primary';
                    }
                    $out .= '<span class="label label-'.$class.'">'.ms2string($m->masterTime).' '.__('by').' '.$m->bestTimePlayerTitle.' ['.$m->bestTimeTeam.']</span>';
                } else {
                    $out .= __("Nobody yet.");
                }
                $out .= '</span>';
                $out .= '</p>';
                $out .= '<hr />';
                $out .= '<p>';
                $out .= __("Exercise type").' → '.$m->type->title;
                if ($m->type->summary != '') {
                    $tooltip = $m->type->summary;
                } else {
                    if ($m->type->getLanguageValue($french, 'summary') != '') {
                        $tooltip = $m->type->getLanguageValue($french, 'summary');
                    }
                }
                $out .= ' <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" title="'.$tooltip.'" onmouseenter="$(this).tooltip(\'show\');"></span>';
                $out .= '</p>';
                $out .= '</div>'; // col-md-9
                $out .= '</div>'; // row
                $out .= '<div class="footerBar">';
                if ($m->summary != '') {
                    $out .= ' → <span>'.$m->summary.'</span> ';
                } else {
                    if ($m->getLanguageValue($french, 'summary') != '') {
                        $out .= ' → <span>'.$m->getLanguageValue($french, 'summary').'</span> ';
                    }
                }
                // Data preview
                $exData = $m->exData;
                $allLines = preg_split('/$\r|\n/', $sanitizer->entitiesMarkdown($exData));
                $listWords = prepareListWords($allLines, $m->type->name);
                $out .= ' <span class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-html="true" title="'.$listWords.'" onmouseenter="$(this).tooltip(\'show\');"></span>';
                $out .= '</div>';
                $out .= '</div>'; // monsterDiv
                $out .= '<div class="clearfix visible-md-block"></div>';
                $out .= '</div>'; // col-md-4
            } else {
                $notAvailable->add($m);
            }
            $out .= '<div class="clearfix visible-md-block"></div>';
        }
        $out .= '</section>'; // row
        if ($notAvailable->count() > 0) {
            $out .= '<section class="row">';
            $out .= '<h3>'.__("Not available").' ('.$notAvailable->count().') :</h3>';
            $out .= '<ul>';
            foreach ($notAvailable as $m) {
                $out .= '<li>';
                if ($m->waitForTrain == 1) {
                    if ($m->image) {
                        $out .= '<img class="" src="'.$m->image->getCrop("small")->url.'" title="" alt="no-img" /> ';
                    }
                    $out .= $m->title;
                    $out .= ' → <span class="label label-success">'.__("Available tomorrow !").'</span>';
                } else {
                    if ($m->image) {
                        $out .= '<img class="" src="'.$m->image->getCrop("small")->url.'" title="" alt="no-img" /> ';
                    }
                    $out .= $m->title;
                    $out .= ' → <span class="label label-danger">'.sprintf(__("Available in %d days"), $m->waitForTrain).'</span>';
                }
                $formerRequest = $pages->get("has_parent=$player, template=event, task.name=fight-vv, inClass=1, refPage=$m, date>$schoolYear->dateStart");
                if ($formerRequest->id) {
                    $out .= ' <span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="'.__('You have already defeated this monster this school year.').'" onmouseenter="$(this).tooltip(\'show\');"></span>';
                } else {
                    if ($request == 0) {
                        $msg = sprintf(__("Fight request for %s"), $m->title);
                        $out .= ' <span><a class="btn btn-danger btn-lg fightRequestConfirm" href="'.$page->url.'" data-href="'.$pages->get("name=submitforms")->url.'?form=fightRequest&monsterId='.$m->id.'&playerId='.$player->id.'" data-msg="'.$msg.'" data-reload="true"><i class="glyphicon glyphicon-education" data-toggle="tooltip" title="'.__("Ask teacher for an in-class Fight!").'" onmouseenter="$(this).tooltip(\'show\');"></i></a></span>';
                    } elseif ($request == $m->id) {
                        $out .= ' <span class="glyphicon glyphicon-ok-circle" data-toggle="tooltip" title="'.__('Your teacher has already been warned about this request.').'" onmouseenter="$(this).tooltip(\'show\');"></span>';
                    } else {
                        $out .= ' <span class="glyphicon glyphicon-ban-circle" data-toggle="tooltip" title="'.__('Your teacher has already been warned about a request.').'" onmouseenter="$(this).tooltip(\'show\');"></span>';
                    }
                }
                $out .= '</li>';
            }
            $out .= '</ul>';
            $out .= '</section>';
        }
        $out .= '</div>'; // container
    } else {
        $out .= 'Not Available for superuser !';
    }
    $pages->unCacheAll();

    echo $out;
}
