<?php namespace ProcessWire;
  include("./head.inc"); 

  // Nav tabs
  $selectedTeam = $page->team;
  $team = $selectedTeam;
  if ($user->isSuperuser()) {
    $headTeacher = $team->teacher->first();
  }
  include("./tabList.inc"); 

  $today = mktime(0,0,0, date("m"), date("d"), date("Y"));

  // TODO Set access role only for teachers
  
  $allPlayers->filter("team=$team")->sort("name");
  $allCategories = $pages->find("parent.name=categories, name=participation|reading|speaking|testing|revising|writing|listening,adminOnly=1,sort=name");
  $allCheckpoints = $pages->find("parent.name=tasks, category.name=participation");
  $allCheckpoints->setDuplicateChecking(false);
  foreach ($page->checkpoints as $p) { 
    $positiveTask = clone $p->positiveTask;
    $negativeTask = clone $p->negativeTask;
    if ($positiveTask) { 
      $positiveTask->title = $p->title.' ✓';
      $positiveTask->category = clone $p->category;
      $positiveTask->comment = $p->title;
      $positiveTask->checkpointId = $p->id;
    }
    if ($negativeTask) {
      $negativeTask->title = $p->title.' ✗';
      $negativeTask->category = clone $p->category;
      $negativeTask->comment = $p->title;
      $negativeTask->checkpointId = $p->id;
    }
    $allCheckpoints->add($positiveTask);
    $allCheckpoints->add($negativeTask);
  }
  $allCheckpoints->sort("-category.name,HP,XP");

  if ($allCheckpoints->count() > 0) {
    if ($input->urlSegment1 == 'edit') { // Editing mode
      echo '<p class="text-center">';
        echo '<a href="'.$page->url.'" class="btn btn-primary">'.__('Viewing mode').'</a>';
        echo '<a href="'.$page->url.'edit" class="btn btn-primary" disabled="disabled">'.__('Editing mode').'</a>';
      echo '</p>';
  ?>
    <ul class="list-inline text-center">
      <ul class="list-inline">
        <li><button class="btn btn-primary toggle-vis" data-category=""><?php echo __('All Categories'); ?></button></li>
        <?php
          foreach ($allCategories as $cat) {
            echo '<li><button class="btn btn-primary toggle-vis" data-category="'.$cat->name.'">'.$cat->title.'</button></li>';
          }
        ?>
      </ul>
    </ul>

    <form id="adminTableForm" name="adminTableForm" action="<?php echo $pages->get('name=submitforms')->url; ?>" method="post" class="" role="form">

    <input type="hidden" name="adminTableSubmit" value="Save" />
    <input type="submit" name="adminTableSubmit" value="<?php echo __("Save"); ?>" class="btn btn-block btn-primary" disabled="disabled" />

    <table id="adminTable" class="adminTable roadmapTable">
        <colgroup></colgroup>
        <?php
        foreach ($allCheckpoints as $task) {
          echo '<colgroup></colgroup>';
        }
        ?>
      <thead>
        <tr class="dark">
          <th style="min-width:80px"><?php echo __('Players'); ?></th>
          <?php 
          $colIndex = 0;
          foreach ($allCheckpoints as $task) { 
            if ($task->is("template=task")) {
              $task = checkModTask($task, $headTeacher);
            }
          ?>
          <th class="task" id="th_<?php echo $colIndex; ?>" data-category="<?php echo $task->category->name; ?>" data-order="<?php echo $task->name; ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo $task->summary; ?>" data-keepVisible="">
            <div class="vertical-text">
              <div class="vertical-text__inner">
                <?php if ($task->teacherTitle != '') { $task->title = $task->teacherTitle; }
                  echo $task->title;
                ?>
              </div>
            </div>
          <?php $colIndex++; ?>
          </th>
          <?php } ?>
        </tr>
        <tr>
          <td><?php echo __("Select all"); ?></td>
          <?php 
            $colIndex = 0;
            foreach ($allCheckpoints as $task) {
          ?>
            <td data-toggle="tooltip" title="<?php echo __("Select all"); ?>"><input type="checkbox" id="csat_<?php echo $colIndex; ?>" class="selectAll" onclick="selectAll(<?php echo $colIndex; ?>)" /></td>
          <?php 
              $colIndex++;
            }
          ?>
        </tr>
      </thead>
      <tbody>
      <?php
        foreach ($allPlayers as $player) { 
          $id = $player->id; 
          // See if absence already recorded (last event)
          $abs = $player->get("name=history")->children("date>$today")->get("task.name=abs|absent");
          // TODO Check if checkpoint has already been completed, then disable checkbox ???
      ?>
      <tr class="<?php if ($abs) { $disabled = 'disabled'; echo 'negative'; } else { $disabled = ''; } ?>">
      <td class="dark"><a class="negative" href="<?php echo $player->url; ?>" target="_blank"><?php echo $player->title; ?></a></td>
        <?php 
        $colIndex = 0;
        foreach ($allCheckpoints as $task) {
          if ($task->is("category.name=participation")) { $task->comment = $page->title; $task->checkpointId = $page->id; }
          if ($task->is("HP<0")) { $type = 'negative'; } else { $type=''; }
          if ($task->is("category.name=participation")) {$type .= " strong"; }
          $taskId = $task->id;
        ?>
        <td class="<?php echo $type; ?>" data-toggle="tooltip" title="<?php echo $player->title.' - '.$task->title; ?>">
          <input type="checkbox" <?php echo $disabled; ?> class="ctPlayer ct_<?php echo $colIndex; ?>" id="" data-customId="<?php echo $id.'_'.$taskId.'_'.$task->checkpointId; ?>" name="player[<?php echo $id.'_'.$taskId.'_'.$task->checkpointId; ?>]" onChange="onCheck(<?php echo $colIndex; ?>)" />
          <input style="display: none;" disabled="disabled" type="text" data-customId="<?php echo $id.'_'.$taskId.'_'.$task->checkpointId; ?>" class="cc_<?php echo $taskId; ?>" name="comment_<?php echo $id.'_'.$taskId.'_'.$task->checkpointId; ?>" value="<?php echo $task->comment; ?>" placeholder="<?php echo __("comment"); ?>" />
<!-- 
        // TODO Use linkedId for checkpoint ID ?? [For now, uneditable summary is used to relate events ]
        // <input type="hidden" name="linkedId" value="'.$task->linkedIdd.'" />
-->
          <?php 
            if ($abs && $task->is("name=absent|abs")) { 
              echo "<a href='#' class='removeAbs' data-type='removeAbs' data-url='".$pages->get('name=submitforms')->url."?form=deleteForm&eventId=".$abs->id."'>[✗]</a>"; 
            } else {
              if ($disabled == 'disabled') { echo "<a href='#' class='toggleEnabled'>[◑]</a>"; } 
            }
          ?>
        </td>
        <?php $colIndex++;} ?>
      </tr>
      <?php } ?>
      </tbody>
    </table>
    <input type="submit" name="adminTableSubmit" value="<?php echo __("Save"); ?>" class="btn btn-block btn-primary" disabled="disabled" />
    <fieldset>
      <?php
        $today = date('Y-m-d');
        echo '<legend>';
        echo __("More options").' - ';
        echo '<span class="glyphicon glyphicon-warning-sign"></span> '.__("Applied to ALL selected players and tasks !");
        echo '</legend>';
        echo '<label for="customDate">'.__("Custom date").'</label>';
        echo '<input id="customDate" name="customDate" type="date" size="8" value="'.$today.'" />';
        echo '&nbsp;&nbsp;';
        echo '<select id="customRefPage" name="customRefPage">';
          echo '<option value="'.$page->id.'">';
          echo $page->title;
          echo '</option>';
        echo '</select>';
        echo '<input type="checkbox" id="stayOnPage" name="stayOnPage" data-url="'.$page->url.$input->urlSegment1.'" />';
        echo '&nbsp;';
        echo '<label for="stayOnPage">'.__("Stay on this page after saving").'</label>';
      ?>
    </fieldset>
    </form>

  <?php
  } else { // Viewing mode
      echo '<p class="text-center">';
        echo '<a href="'.$page->url.'" class="btn btn-primary" disabled="disabled">'.__('Viewing mode').'</a>';
        echo '<a href="'.$page->url.'edit" class="btn btn-primary">'.__('Editing mode').'</a>';
      echo '</p>';
      echo '<h4 class="text-center">'.$page->title.'</h4>';
    ?>
      <ul class="list-inline text-center">
        <ul class="list-inline">
          <li><button class="btn btn-primary toggle-vis" data-category=""><?php echo __('All Categories'); ?></button></li>
          <?php
            foreach ($allCategories as $cat) {
              if ($cat->name != "participation") {
                echo '<li><button class="btn btn-primary toggle-vis" data-category="'.$cat->name.'">'.$cat->title.'</button></li>';
              }
            }
          ?>
        </ul>
      </ul>
    <?php
      // TODO Use categoryFilter to hide columns (so no .task and sortable columns would work)
      // TODO Make a #roadMap based on #adminTable to dissociate both ?
      echo '<table id="adminTable" class="adminTable">';
        echo '<colgroup></colgroup>';
        echo '<colgroup></colgroup>';
        foreach ($allCheckpoints as $task) {
          echo '<colgroup></colgroup>';
        }
        echo '<thead>';
          echo '<tr class="dark">';
                $colIndex = 0;
                echo '<th>'.__("Involvement").'</th>';
                echo '<th style="min-width:80px">&nbsp;</th>';
                foreach ($page->checkpoints as $checkpoint) {
                  echo '<th class="task" id="th_'.$colIndex.'" data-category="'.$checkpoint->category->name.'" data-order="'.$checkpoint->name.'" data-toggle="tooltip" data-placement="top" title="'.$checkpoint->title.'" data-keepVisible="">';
                  echo '<div class="vertical-text">';
                    echo '<div class="vertical-text__inner">';
                      echo $checkpoint->title;
                    echo '</div>';
                  echo '</div>';
                  echo '</th>';
                  $colIndex++;
                }
          echo '</tr>';
        echo '</thead>';
        echo '<tbody>';
        foreach ($allPlayers as $player) { 
          echo '<tr>';
          $allParticipation = $player->child("name=history")->find("template=event, refPage=$page, task.category.name=participation, sort=date");
            // for ($i=0; $i<19; $i++) {
              echo '<td class="participation">';
            foreach ($allParticipation as $p) {
              $result = '';
              switch($p->task->name) {
              // if ($allParticipation[$i] != '') {
                // switch($allParticipation[$i]->task->name) {
                  case 'communication-rr' : $result = 'RR'; $className = "label label-danger"; break; 
                  case 'communication-r' : $result = 'R'; $className = "label label-danger"; break; 
                  case 'communication-v' : $result = 'V'; $className = "label label-success"; break; 
                  case 'communication-vv' : $result = 'VV'; $className = "label label-success"; break; 
                  case 'absent':
                  case 'abs' : $result = '-'; $className = "label label-primary"; break; 
                }
                // echo '<span class="'.$className.'" data-toggle="tooltip" title="'.date("d/m", $allParticipation[$i]->date).'">'.$result.'</span>';
              // } else { echo '&nbsp;'; }
              echo '<span style="display:inline-block; width:20px;" class="'.$className.'" data-toggle="tooltip" title="'.date("d/m", $p->date).'">'.$result.'</span>';
            }
            echo '</td>';
          // echo '<span class="badge badge-danger pull-left">'.$nbResult.'</span>';
          echo '<td class="dark"><a class="negative" href="'.$player->url.'" target="_blank">'.$player->title.'</a></td>';
          foreach ($page->checkpoints as $checkpoint) {
            $allResults = $player->child("name=history")->find("template=event, refPage=$page, summary=$checkpoint->title");
            echo '<td>';
            foreach ($allResults as $r) {
              if ($r->task->HP < 0) { $result = '✗'; $className = "label label-danger"; } else { $result = '✓'; $className = "label label-success"; }
              echo '<span class="'.$className.'" data-toggle="tooltip" title="'.date("d/m", $r->date).'">'.$result.'</span>';
            }
            unset($allResults);
            echo '</td>';
          }
          echo '</tr>';
        }
        echo '</tbody>';
      echo '</table>';
    }
  } else { // No tasks
    echo '<p class="">'.__("You have no tasks set yet.").'</p>';
  }

  $pages->unCacheAll();

  include("./foot.inc"); 
?>
