<?php namespace ProcessWire;
  include("./head.inc"); 

  if ($user->hasRole('player') || $user->hasRole('teacher')) {
    $headTeacher = getHeadTeacher($user);
  }

  $out = '';

  if (isset($player) && $user->isLoggedin() || $user->isSuperuser() || $user->hasRole('teacher')) {
    if ($user->isLoggedin()) {
      $user->setAndSave('lastMeetingHallActivity', time());
    }
    if ($headTeacher) {
      $teacherPage = $pages->get("parent.name=teachers, template=teacherProfile, name=$headTeacher->name");
      $out .= '<h4 class="text-center">';
        if ($teacherPage->bbbMeetingHallUrl != '') {
          $out .= '<a class="btn btn-primary" href="'.$teacherPage->bbbMeetingHallUrl.'" target="_blank">';
          $out .= '<span class="glyphicon glyphicon-tent"></span> ';
          $out .= __("FLEnglish Classroom");
          $out .= '</a>';
          if ($headTeacher->name == 'flieutaud') {
            $out .= ' | ';
            $out .= '<a class="btn btn-primary" href="https://web.mumble.framatalk.org/" target="_blank">';
              $out .= '<span class="glyphicon glyphicon-earphone"></span> ';
              $out .= __('Audio Room');
              $help = __("Choose a username and then double-click on FLEnglish room. Shitf+Control to start speaking. Look at FLEnglish TV to see the complete guide.");
              $out .= ' <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" title="'.$help.'"></span> ';
            $out .= '</a>';
          }
        }
      $out .= '</h4>';
      if ($teacherPage->meetingHallUrl != '') {
        $help = __("Click on the top-right icon and type your name, then you can type in the main central zone. EVERYBODY will see what you type !");
        $out .= '<section id="meetingHall" class="row" data-url="'.$pages->get("name=submitForms")->url.'" data-userId="'.$user->id.'">';
        $out .= '<h2 class="text-center">';
          $out .= __('Planet Alert Meeting hall (live)');
          $out .= '<span class="pull-right glyphicon glyphicon-question-sign" data-toggle="tooltip" title="'.$help.'"></span>';
        $out .= '</h2>';
          $out .= '<p class="text-center"><span class="glyphicon glyphicon-warning-sign"></span> <em>'.__('Be respectful ! EVERYTHING is recorded by your teacher in this room.').'</em></p>';
          $out .= '<iframe name="embed_readwrite" src="'.$teacherPage->meetingHallUrl.'" width="100%" height=600></iframe>';
        $out .= '</section>';
      }
    }
  } else {
    $out .= $noAuthMessage;
  }

  echo $out;

  include("./foot.inc"); 
?>
