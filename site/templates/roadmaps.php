<?php namespace ProcessWire;
  include("./head.inc"); 

  // Nav tabs
  $team = $selectedTeam;
  if ($user->isSuperuser()) {
    $headTeacher = $team->teacher->first();
  }
  include("./tabList.inc"); 

  $today = mktime(0,0,0, date("m"), date("d"), date("Y"));

  // TODO Set access role only for teachers
  
  if ($user->hasRole('teacher') || $user->isSuperuser()) {
    $allRoadmaps = $pages->find("parent.name=roadmaps,template=roadmap,team=$team,sort=title");

    foreach ($allRoadmaps as $p) { 
      echo '<ul>';
      echo '<li><a href="'.$p->url.'">'.$p->title.'</a></li>'; 
      echo '</ul>';
    }
  }

  $pages->unCacheAll();

  include("./foot.inc"); 
?>
