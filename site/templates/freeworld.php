<?php namespace ProcessWire;

include("./head.inc"); 

$selectedTeam = $pages->get("template=team, name=$input->urlSegment1");
if ($selectedTeam) {
  $rank = $selectedTeam->rank->index;
  if ($user->isLoggedin() && $user->hasRole('teacher') || $user->isSuperuser()) {
    include("./tabList.inc");
  }

  $out = '';
  $ajaxContentUrl = $pages->get("name=ajax-content")->url;
  $out .= '<div id="showInfo" data-href="'.$ajaxContentUrl.'"></div>';

  $out .= '<h2 class="text-center">';
    $out .= '<span id="teamTitle" class="label label-primary">';
    $out .= sprintf(__('%1$s has completed %2$s%% of Planet Alert world !'), $selectedTeam->title, $selectedTeam->freeworld);
    $allElements = teamFreeworld($selectedTeam);
    $allCompleted = $allElements->find("completed=1");
    $notCompleted = $allElements->find("completed!=1");
    $out .= ' '.sprintf(__('(%1$s/%2$s elements)'),$selectedTeam->freeelements->count(), $allElements->count());
    $out .= '</span>';
    if (isset($player)) {
      $out .= '<span id="individualTitle" class="label label-primary hidden">';
      $out .= sprintf(__('You have freed %1$s/%2$s elements of Planet Alert world !'), ($player->places->count()+$player->people->count()), $allElements->count());
      $out .= '</span>';
    }
    $out .= '<div class="btn-group pull-right">';
      $out .= '<button class="btn btn-success btn-xs" id="showMapButton" autofocus="autofocus"><span class="glyphicon glyphicon-globe" data-toggle="tooltip" title="['.__('Show free world map').']"></span></button>';
      $out .= '<button id="teamView" class="btn btn-success btn-xs" data-toggle="tooltip" title="'.__('Team view').'"><span class="glyphicon glyphicon-user"></span><span class="glyphicon glyphicon-user"></span></button>';
      if (isset($player)) {
        $out .= '<button id="individualView" class="btn btn-success btn-xs" data-toggle="collapse" title="'.__('Individual view').'"><span class="glyphicon glyphicon-user"></span></button>';
      }
      $out .= '<button class="btn btn-success btn-xs" id="thumbsGallery"><span class="glyphicon glyphicon-th" data-toggle="tooltip" title="['.__('See free world thumbnails').']"></span></button>';
      $out .= '<button class="btn btn-success btn-xs" id="visitGallery"><span class="glyphicon glyphicon-eye-open" data-toggle="tooltip" title="['.__('Visit free world gallery').']"></span></button>';
    $out .= '</div>';
  $out .= '</h2>';

  $out .= '<div id="showMap" class="">';
    $map = $modules->get('MarkupLeafletMap');
    $out .= $map->getLeafletMapHeaderLines();
    $options = array(
      'markerIcon' => 'star',
      'markerColour' => 'green',
      'markerFormatter' => function($page, $marker_options) {
        if ($page->completed == 1) { 
          $page->marker_icon = 'star';
          $page->marker_colour = 'green';
          $page->marker_spin = true;
        } else if ($page->completed == 0.5) { 
          $page->marker_icon = 'android';
          $page->marker_colour = 'orange';
          $page->marker_spin = false;
        } else {
          $page->marker_icon = 'android';
          $page->marker_colour = 'red';
          $page->marker_spin = false;
        }
        $marker_options['icon'] = $page->marker_icon; // Override the default icon for this marker.
        $marker_options['spin'] = $page->marker_spin; // Override the default icon for this marker.
        $marker_options['markerColor'] = $page->marker_colour; // Override the default icon for this marker.
        return $marker_options;
      },
      'popupFormatter' => function($page) {
        if ($page->is("template=place")) {
          $out[] = '<strong>'.sprintf(__('In %1$s, in %2$s'), $page->city->title, $page->country->title).'</strong>';
        } else {
          if ($page->city->id) {
            $out[] = '<strong>'.sprintf(__('Born in %1$s, in %2$s (%3$s)'), $page->city->title, $page->country->title, $page->nationality).'</strong>';
          } else {
            $out[] = '<strong>'.sprintf(__('From %1$s (%2$s)'), $page->country->title, $page->nationality).'</strong>';
          }
        }
        $out[] = '['.$page->ownersNb.'/'.$page->teamRate.']';
        /* $out[] = "<p class=\"text-center\"><img src=\"{$page->photo->eq(0)->getCrop('thumbnail')->url}\" /></p>"; */
        $out[] = '<a href="'.$page->url.'" target="_blank">'.__('[See details]').'</a>]';
        return implode('<br/>', $out);
      },
      'height' => '500px',
      'class' => 'mapBox',
      'provider' => 'Esri.NatGeoWorldMap',
    );
    $out .= $map->render($allElements, 'map', $options); 
  $out .= '</div>';

  $freeWorldList = $cache->get('cache__'.$selectedTeam->name.'-freeworld-countryList', 86400, function($page, $pages) use($allElements, $allCompleted, $selectedTeam) {
    $out = '';
    $out .= '<div id="freeWorldList" class="well hidden">';
      $allCountries = $pages->find("template=country")->sort("title");
      $out .= '<ul class="freeworld col3">';
      $completedCountry = 0;
      foreach ($allCountries as $c) {
        $allCountryElements = $allElements->find("country=$c");
        $allCompletedCountryElements = $allCompleted->find("country=$c");
        if ($allCountryElements->count() > 0) {
          if ($allCountryElements->count() == $allCompletedCountryElements->count()) {
            $class = 'completedCountry';
            $completedCountry++;
          } else {
            $class = 'incompleteCountry';
          }
          $out .= '<li class="'.$class.'">';
          $out .= '<span class="'.$class.'"><a href="'.$pages->get("name=places")->url.'country/'.$c->name.'">'.$c->title.'</a></span>';
          $out .= '<span class="pushRight">';
          foreach ($allCountryElements as $el) {
            if ($el->completed == 1) {
              $class = 'completed';
            } else if ($el->completed == 0.5) {
              $class = 'almost';
            } else {
              $class = '';
            }
            $tooltip = $el->title. ' ['.$el->teamOwners->count().'/'.$el->teamRate.']';
            if (isset($player) && $player->name == $user->name) {
              if ($player->places->has($el) || $player->people->has($el)) {
                $class.= ' userCompleted';
              }
            }
            $out .= '<span class="showInfo freeRate elementSymbol '.$class.'" data-completed="'.$class.'" data-toggle="tooltip" title="'.$tooltip.'" data-id="'.$el->id.'" data-teamId="'.$selectedTeam->id.'">&nbsp;</span>';
          }
          $out .= '</span>';
          $out .= '</li>';
        }
      }
      $out .= '</ul>';
      $out .= '<h4 class="text-right"><span class="glyphicon glyphicon-thumbs-up"></span> '.sprintf(_n('%d country is completely free !', '%d countries are completely free !', $completedCountry), $completedCountry).'</h4>';
    $out .= '</div>';
    return $out;
  });
  $out .= $freeWorldList;

  $freeWorldCarousel = $cache->get('cache__'.$selectedTeam->name.'-freeWorld-carousel-'.$user->language->name, 86400, function() use($selectedTeam) {
    // Free world carousel
    $out = '';
    if ($selectedTeam->freeelements->count() > 0) {
      $selectedTeam->freeelements->shuffle();
      $out .= '<div id="galleryContainer" class="row hidden">';
        $out .= '<div class="col-sm-4">';
        $out .= '</div>';
        $out .= '<div class="col-sm-4">';
          $out .= '<div id="myGallery" class="carousel slide" data-ride="carousel" data-interval="5000">';
            $out .= '<div class="carousel-inner">';
            $out .= '<ol class="carousel-indicators">';
              $index = 0;
              $active = 'active';
              foreach($selectedTeam->freeelements as $el) {
                $out .= '<li data-target="#myGallery" data-slide-to="'.$index.'" class="'.$active.'"></li>';
                $index++;
                $active = '';
              }
            $out .= '</ol>';
              $active = 'active';
              foreach($selectedTeam->freeelements as $el) {
                /* $thumb = $el->photo->eq(0); */
                /* $thumb = $image->size(500, 500); */
                $img = $el->photo->eq(0);
                if ($img->width() > $img->height()) {
                  $thumb = $img->width(800);
                } else {
                  $thumb = $img->height(400);
                }
                $out .= '<div class="item '.$active.'">';
                  $out .= '<img class="" src="'.$thumb->url.'" alt="'.$el->name.'" />';
                  $out .= '<div class="carousel-caption">';
                    $out .= '<h3>'.$el->title.'</h3>';
                    $out .= '<p>';
                      if ($el->is("template=place")) {
                        $out .= $el->city->title.', '.$el->country->title;
                      }
                      if ($el->is("template=people")) {
                        $out .= $el->nationality;
                      }
                    $out .= '</p>';
                  $out .= '</div>';
                $out .= '</div>';
                $active = '';
              }
            $out .= '</div>';
              $out .= '<a class="left carousel-control" href="#myGallery" role="button" data-slide="prev">';
                $out .= '<span class="glyphicon glyphicon-chevron-left"></span>';
                $out .= '<span class="sr-only">'.__('Previous').'</span>';
              $out .= '</a>';
              $out .= '<a class="right carousel-control" href="#myGallery" role="button" data-slide="next">';
                $out .= '<span class="glyphicon glyphicon-chevron-right"></span>';
                $out .= '<span class="sr-only">'.__('Next').'</span>';
              $out .= '</a>';
          $out .= '</div>';
        $out .= '</div>';
        $out .= '<div class="col-sm-4">';
        $out .= '</div>';
      $out .= '</div>';
    }
    return $out;
  });
  $out .= $freeWorldCarousel;

  $thumbsContainer = $cache->get('cache__'.$selectedTeam->name.'-freeworld-thumbsContainer-'.$user->language->name, 86400, function($page, $pages) use($allElements, $allCompleted, $selectedTeam) {
    $out = '';
    $out .= '<div id="thumbsContainer" class="well hidden">';
      $out .= '<h2>'.__("Completely free elements").' ['.$allCompleted->count().']</h2>';
      $out .= '<ul class="list list-unstyled list-inline">';
      foreach ($allCompleted as $c) {
        if ($c->template == 'place')  { $teamRate = $selectedTeam->teamRatePlace; } else { $teamRate = $selectedTeam->teamRatePeople; }
        $tooltip = '<strong>'.$c->title.'</strong> ['.$c->ownersNb.'/'.$teamRate.']';
        if ($c->is("template=place")) {
          $tooltip .= '<p>'.sprintf(__('In %1$s, in %2$s'), $c->city->title, $c->country->title).'</p>';
        } else {
          if ($c->city->id) {
            $tooltip .= '<p>'.sprintf(__('Born in %1$s, in %2$s (%3$s)'), $c->city->title, $c->country->title, $c->nationality).'</p>';
          } else {
            $tooltip .= '<p>'.sprintf(__('From %1$s (%2$s)'), $c->country->title, $c->nationality).'</p>';
          }
        }
        $out .= '<li class="" data-toggle="tooltip" data-html="true" title="'.$tooltip.'">';
          $out .= '<img class="showInfo img img-thumbnail freeRate" src="'.$c->photo->eq(0)->getCrop('thumbnail')->url.'" data-id="'.$c->id.'" data-teamId="'.$selectedTeam->id.'" />';
        $out .= '</li>';
      }
      $out .= '</ul>';
      // In progress elements
      $allInProgress = $allElements->find("ownersNb>0, completed!=1")->sort("-ownersNb");
      $out .= '<h2>'.__("Elements in progress").' ['.$allInProgress->count().']</h2>';
      $out .= '<ul class="list list-unstyled list-inline">';
      foreach ($allInProgress as $c) {
        if ($c->template == 'place')  { $teamRate = $selectedTeam->teamRatePlace; } else { $teamRate = $selectedTeam->teamRatePeople; }
        $tooltip = '<strong>'.$c->title.'</strong> ['.$c->ownersNb.'/'.$teamRate.']';
        if ($c->is("template=place")) {
          $tooltip .= '<p>'.sprintf(__('In %1$s, in %2$s'), $c->city->title, $c->country->title).'</p>';
        } else {
          if ($c->city->id) {
            $tooltip .= '<p>'.sprintf(__('Born in %1$s, in %2$s (%3$s)'), $c->city->title, $c->country->title, $c->nationality).'</p>';
          } else {
            $tooltip .= '<p>'.sprintf(__('From %1$s (%2$s)'), $c->country->title, $c->nationality).'</p>';
          }
        }
        $out .= '<li class="" data-toggle="tooltip" data-html="true" title="'.$tooltip.'">';
          $out .= '<img class="showInfo img img-thumbnail freeRate" src="'.$c->photo->eq(0)->getCrop('thumbnail')->url.'" data-id="'.$c->id.'" data-teamId="'.$selectedTeam->id.'" />';
        $out .= '</li>';
      }
      $out .= '</ul>';
    $out .= '</div>';
    return $out;
  });
  $out .= $thumbsContainer;
}
echo $out;

include("./foot.inc"); 

?>
