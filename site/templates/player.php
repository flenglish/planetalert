<?php

namespace ProcessWire;

include("./head.inc");

/* $playerPage = $pages->get("template=player,name=".$input->urlSegment2); */
$playerPage = $page;
$tmpCache = $playerPage->children()->get("name=tmp");
$playersTotalNb = $pages->count("template=player,team=$playerPage->team");
$playerPage->places ? $playerPlacesNb = $playerPage->places->count() : $playerPlacesNb = 0;
$playerPage->people ? $playerPeopleNb = $playerPage->people->count() : $playerPeopleNb = 0;
$playerNbEl = $playerPlacesNb + $playerPeopleNb;
$rightInvasions = $pages->find("has_parent=$playerPage, parent.name=history, task.name=right-invasion")->count();
$wrongInvasions = $pages->find("has_parent=$playerPage, parent.name=history, task.name=wrong-invasion")->count();
$submitforms = $pages->get("name=submitforms");

$ajaxContentUrl = $pages->get("name=ajax-content")->url;
$out = '<div id="showInfo" data-href="'.$ajaxContentUrl.'"></div>';

$karma = $playerPage->yearlyKarma;
if (!$karma) {
    $karma = 0;
}
if ($karma > 0 && $playerPage->team->name != 'no-team') { // Team Position
    // Number of players having a better karma than current player
    $playerPos = $pages->count("template=player,team=$playerPage->team,yearlyKarma>$karma") + 1;
} else {
    $playerPos = $playersTotalNb;
}

// Set details according to user profile
if ($user->isSuperuser() || ($user->hasRole('teacher') && $playerPage->team->teacher->has("id=$user->id")) || ($user->isLoggedin() && $user->name == $playerPage->login)) {
    $showDetails = true;
} else {
    $showDetails = false;
}
// Set no hk counter
if ($showDetails) {
    if ($playerPage->hkcount > 0) {
        // See hk details on tooltip
        $hkTasks = getHkCountDetails($playerPage, $playerPage->team->periods->dateStart, $playerPage->team->periods->dateEnd);
        $hkList = '<ul>';
        foreach ($hkTasks as $e) {
            $hkList .= '<li>';
            $hkList .= date("d/m", $e->date).' : ';
            $hkList .= $e->title;
            $hkList .= '</li>';
        }
        $hkList .= '</ul>';
        if ($hkTasks->count() > 0) {
            $hkCount = '<span class="label label-danger" data-toggle="tooltip" data-html="true" title="'.$hkList.'">'.$playerPage->hkcount.'</span>';
        } else {
            $hkCount = '<span class="label label-danger" data-toggle="tooltip" title="'.__("Error ? Ask your teacher.").'">'.$playerPage->hkcount.'?</span>';
        }
    } else {
        $hkCount = '<span class="label label-danger">0</span>';
    }
} else {
    $hkCount = '<span class="label label-danger">Private!</span>';
}
// Get last activity # of days (for no-team players)
$twoWeeksOld = time() - (14 * 24 * 60 * 60);
if ($playerPage->modified <= $twoWeeksOld) {
    $helpAlert = true;
    $helpTitle = __("Watch out for inactivity !");
    $helpMessage = '<h4>'.__("You will lose all your GC if you don't do anything (UT, fight, donation, ...) soon !").'</h4>';
}

// helpAlert
if ($user->hasRole('teacher') || $user->isSuperuser()) {
    echo '<div class="row">';
    if ($tmpCache) {
        echo '<a class="pdfLink btn btn-danger" href="'.$tmpCache->url.'">'.__("See tmpCache").'</a>';
    } else {
        createTmpCache($playerPage);
        $tmpCache = $playerPage->children()->get("name=tmp");
        echo '<a class="pdfLink btn btn-danger" href="'.$tmpCache->url.'">'.__("See tmpCache").'</a>';
    }
    // Check nb of pages according to nb of freed items
    echo '<a class="pdfLink btn btn-info" href="'.$playerPage->url.'?index=-1&pages2pdf=1">'.__("Empty PDF").'</a>';
    echo '<a class="pdfLink btn btn-info" href="'.$playerPage->url.'?index=0&pages2pdf=1">PDF 1</a>';
    $nbPages = ceil($playerNbEl / 6);
    for ($i = 0; $i < $nbPages; $i++) {
        echo '<a class="pdfLink btn btn-info" href="'.$playerPage->url.'?index='.($i + 1).'&total='.$nbPages.'&pages2pdf=1">PDF '.($i + 2).'</a>';
    }
    echo '</div>';
}
?>
<div class="row">
  <div class="col-lg-6 panel panel-success panel-player">
    <div class="panel-heading">
      <h1 class="panel-title">
        <span class="">
          <?php
            echo $playerPage->title.' ['.$playerPage->team->title.']';
if ($user->isSuperuser() || ($user->hasRole('teacher') && $playerPage->team->teacher->has("id=$user->id"))) {
    echo $playerPage->feel();
}
?>
        </span>
        <?php
$showSkills = '';
foreach ($playerPage->skills as $s) {
    $showSkills .= '&nbsp;<span class="label label-primary">'.$s->title.'</span>';
}
echo $showSkills;
?>
      </h1>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="text-center col-sm-6">
          <?php
    if ($playerPage->avatar) {
        echo '<img src="'.$playerPage->avatar->url.'" alt="'.$playerPage->title.'." height="200" />';
    } else {
        echo 'No avatar';
        echo '<br /><a href="'.$pages->get("name=avatar")->url.'" title="'.__("Click to create your avatar").'">'.__("[Create an avatar]").'</a>';
    }
if ($user->isSuperuser() || $user->hasRole('teacher')) {
    echo '<br /><a href="'.$pages->get("name=avatar")->url.'" title="'.__("Click to create your avatar").'">'.__("[Create an avatar]").'</a>';
}
?>
        </div>
        <div class="col-sm-6">
          <ul class="player-details">
          <li><?php echo __("Karma"); ?> : <span class="label label-default"><?php echo $karma; ?></span> <?php if ($playerPage->team->name != 'no-team') {?><span data-toggle="tooltip" title="Team position">(<?php echo $playerPos; ?>/<?php echo $playersTotalNb; ?>)</span><?php } ?></li>
          <li><?php echo __("Reputation"); ?> : <span class="label label-default"><?php echo $playerPage->reputation; ?></span></li>
          <li><?php echo __("Level"); ?> : <?php echo $playerPage->level; ?></li>
          <li><img src="<?php  echo $config->urls->templates?>img/gold_mini.png" alt="gold coins." /> : <span class="label label-default" data-toggle="tooltip" data-html="true" title="<?php echo __('Gold Coins'); ?>">
            <?php echo $playerPage->GC.' '.__("GC"); ?>
          </span></li>
          <?php
  if ($playerPage->rank && $playerPage->rank->is("index>=6") || $playerPage->team->rank->is("index>=6")) {
      echo '<li><span class="glyphicon glyphicon-exclamation-sign"></span> '. __("Hk count") .' : '.$hkCount.'</li>';
  }
if ($playerPage->inactive == 0) {
    if ($playerPage->modified <= $twoWeeksOld) {
        echo '<li>';
        echo '<span class="glyphicon glyphicon-exclamation-sign"></span> Almost inactive player !';
        echo '</li>';
    } else {
        echo '<li><span class="glyphicon glyphicon-thumbs-up"></span> '.__("Active player !").'</li>';
    }
} else {
    echo '<li><span class="glyphicon glyphicon-exclamation-sign"></span> '.__("Long inactivity period");
    echo ' → '.__("All GC have been lost !").'</li>';
}
?>
          </ul>
        </div>
      </div>
      <div class="row">
        <?php if ($playerPage->coma == 0) { ?>
          <div class="col-sm-2 text-right">
            <span class="badge"><img src="<?php  echo $config->urls->templates?>img/heart.png" alt="heart." /> <?php echo $playerPage->HP; ?>/50</span>
          </div>
          <div class="col-sm-10">
            <div class="progress progress-striped progress-lg" data-toggle="tooltip" title="<?php echo __("Health points"); ?>">
              <div class="progress-bar progress-bar-danger" role="progressbar" style="width:<?php echo 2 * $playerPage->HP; ?>%">
              </div>
            </div>
          </div>
          <div class="col-sm-2 text-right">
            <?php
  if ($playerPage->level <= 4) {
      $delta = 40 + ($playerPage->level * 10);
  } else {
      $delta = 90;
  }
            $threshold = ($playerPage->level * 10) + $delta;
            ?>
            <span class="badge"><img src="<?php echo $config->urls->templates; ?>/img/star.png" alt="star." /> <?php echo $playerPage->XP; ?>/<?php echo $threshold; ?></span>
          </div>
          <div class="col-sm-10">
            <div class="progress progress-striped progress-lg" data-toggle="tooltip" title="<?php echo sprintf(__("Experience (Level %d)"), $playerPage->level); ?>">
              <?php if ($playerPage->XP == false) {
                  $playerPage->XP = 0;
              } ?>
              <div class="progress-bar progress-bar-success" role="progressbar" style="width:<?php echo round((100 * $playerPage->XP) / $threshold); ?>%">
              </div>
            </div>
          </div>
        <?php } else { ?> // Coma state 
          <h4 class="text-center"><span class="label label-danger"><?php echo __("You're in a COMA !"); ?></span></h4>
          <h4 class="text-center"><span><?php echo __("Buy a healing potion to go back to normal state !"); ?></span></h4>
        <?php } ?>
      </div>
    </div>
  </div>

  <div class="col-lg-5 panel panel-success">
    <div class="panel-heading">
      <h4 class="panel-title"><?php echo __("Equipment"); ?></h4>
    </div>
    <div class="panel-body text-center">
      <ul class="list-inline">
        <?php
          if ($playerPage->equipment->count > 0) {
              foreach ($playerPage->equipment as $equipment) {
                  if ($equipment->image) {
                      $thumb = $equipment->image->getCrop('small')->url;
                      echo "<li data-toggle='tooltip' data-html='true' title='{$equipment->title}<br />{$equipment->summary}'>";
                      if ($equipment->name == "memory-helmet") { // Direct link to training zone
                          if ($user->isSuperuser() || $user->hasRole('teacher')) {
                              echo '<a href="'.$trainingZone->url.'?playerId='.$playerPage->id.'" title="Go to the Training Zone"><img class="img-thumbnail" src="'.$thumb.'" /></a>';
                          } else {
                              echo '<a href="'.$trainingZone->url.'" title="Go to the Training Zone"><img class="img-thumbnail" src="'.$thumb.'" /></a>';
                          }
                      } elseif ($equipment->name == "book-knowledge") { // Direct link to Visualizer page
                          echo '<a href="'.$pages->get("name=book-knowledge")->url.'" title="Use the '.$equipment->title.'"><img class="img-thumbnail" src="'.$thumb.'" /></a>';
                      } else {
                          echo '<img class="img-thumbnail" src="'.$thumb.'" />';
                      }
                      echo "</li>";
                  } else {
                      echo '<li data-toggle="tooltip" data-html="true" title="'.$equipment->title.'<br />'.$equipment->summary.'">'.$equipment->title.'</li>';
                  }
                  echo "</li>";
              }
          } else {
              echo '<p>'.__("No equipment").'</p>';
          }
?>
      </ul>
      <hr />
      <p class="label label-danger"><?php echo __("Items you need to use in class ↓"); ?></p>
      <ul class="list-inline">
      <?php
if (count($playerPage->usabledItems)) {
    foreach ($playerPage->usabledItems as $i) {
        if ($i->image) {
            if ($i->is("name^=transformation")) {
                echo '<li data-toggle="tooltip" title="'.$i->title.' : '.__("Click to create a new avatar").'"><a target="_blank" href="'.$pages->get("name=avatar")->url.'"><img src="'.$i->image->getCrop("small")->url.'" alt="'.$i->title.'." /></a></li>';
            } else {
                echo '<li data-toggle="tooltip" title="'.$i->title.'"><img src="'.$i->image->getCrop("small")->url.'" alt="'.$i->title.'." /></li>';
            }
        } else {
            echo '<li>'.$i->title.'</li>';
        }
    }
} else {
    echo '<li>'.__("No items").'</li>';
}
?>
      </ul>
      <?php
  echo '<p><span class="label label-danger">'.__("Fight request").'</span> → ';
if ($playerPage->fight_request != 0) {
    $monster = $pages->get($playerPage->fight_request);
    echo $monster->title;
} else {
    echo __('No request');
}
echo ' <a class="btn btn-sm btn-primary" href="'.$pages->get("name=fight-requests")->url.$page->id.'">'.__("See my fight requests page").'</a>';
echo '</p>';
?>
    </div>
    <?php
if ($showDetails) {
    ?>
    <div class="panel-footer">
      <?php
        echo '<p><a href="'.$pages->get('name=shop_generator')->url.$playerPage->id.'">→ '.__("Go to the Marketplace").'</a>.</p>';
    ?>
    </div>
    <?php
}
?>
  </div>
</div>

<div class="row">
  <?php
$out .= '<div class="panel panel-success">';
$out .= '<div class="panel-heading">';
$out .= '<h4 class="panel-title">';
$out .= '<span class=""><span class="glyphicon glyphicon-thumbs-up"></span> '.__("Free elements").' : '.$playerNbEl.'</span>';
if ($playerNbEl > 0 && $showDetails) {
    $out .= ' <a href="#" class="" data-toggle="modal" data-target="#myModal" id="launchCarousel">[<span class="glyphicon glyphicon-eye-open"></span> '.__("Visit the elements").']</a>';
}
$out .= '</h4>';
$out .= '</div>';
$out .= '<div class="panel-body">';
$cachedElements = $cache->get("cache__elements-".$playerPage->id, 2678400, function () use ($playerPage) {
    $out = '';
    $out .= '<h4 class="badge badge-info"><span class=""><span class="glyphicon glyphicon-thumbs-up"></span> '.__("Free places").' </span></h4>';
    $out .= '<ul class="playerPlaces list-inline">';
    foreach ($playerPage->places as $place) {
        $thumbImage = $place->photo->eq(0)->getCrop('thumbnail')->url;
        $out .= '<li><a href="'.$place->url.'"><img class="img-thumbnail" src="'.$thumbImage.'" alt="'.$place->title.'." data-toggle="tooltip" data-html="true" title="'.$place->title.'<br />['.$place->city->title.', '.$place->country->title.']" /></a></li>';
    }
    $out .= '</ul>';
    if ($playerPage->rank && $playerPage->rank->is("index>=8") || $playerPage->team->rank->is("index>=8")) {
        $out .= '<h4 class="badge badge-info"><span class=""><span class="glyphicon glyphicon-thumbs-up"></span> '.__("Free people").'</span></h4>';
        $out .= '<ul class="playerPlaces list-inline">';
        foreach ($playerPage->people as $p) {
            $thumbImage = $p->photo->eq(0)->getCrop('thumbnail')->url;
            $out .= '<li><a href="'.$p->url.'"><img class="img-thumbnail" src="'.$thumbImage.'" alt="'.$p->title.'." data-toggle="tooltip" data-html="true" title="'.$p->title.'<br />'.$p->summary.'" /></a></li>';
        }
        $out .= '</ul>';
    } else {
        $out .= '<p class="badge badge-danger">'.__("People are available for 4emes and 3emes only.").'</p>';
    }
    return $out;
});
$out .= $cachedElements;
$out .= '</div>';
// Carousel
if ($playerNbEl > 0) {
    $allElements = new PageArray();
    $allElements->add($playerPage->places);
    $allElements->add($playerPage->people);
    $allElements->shuffle();
    $out .= '<div class="modal fade" id="myModal">';
    $out .= '<div class="modal-dialog">';
    $out .= '<div class="modal-content">';
    $out .= '<div class="modal-body">';
    $out .= '<div id="myGallery" class="carousel slide" data-interval="4000">';
    $out .= '<div class="carousel-inner">';
    $out .= '<ol class="carousel-indicators">';
    $index = 0;
    $active = 'active';
    foreach ($allElements as $el) {
        $out .= '<li data-target="#myGallery" data-slide-to="'.$index.'" class="'.$active.'"></li>';
        $index++;
        $active = '';
    }
    $out .= '</ol>';
    $active = 'active';
    foreach ($allElements as $el) {
        $img = $el->photo->eq(0);
        if ($img->width() > $img->height()) {
            $thumb = $img->width(800);
        } else {
            $thumb = $img->height(550);
        }
        $out .= '<div class="item '.$active.' text-center">';
        $out .= '<img class="" src="'.$thumb->url.'" alt="'.$el->name.'" />';
        $out .= '<div class="carousel-caption">';
        $out .= '<h3>'.$el->title.'</h3>';
        $out .= '<p>';
        if ($el->is("template=place")) {
            $out .= $el->city->title.', '.$el->country->title;
        }
        if ($el->is("template=people")) {
            $out .= $el->nationality;
        }
        if ($playerPage->team->is("name!=no-team")) {
            setElement($el, $playerPage->team);
            $out .= ' <span>['.$el->owners.'/'.$el->teamRate.']</span>';
        }
        $out .= '</p>';
        $out .= '</div>';
        $out .= '</div>';
        $active = '';
    }
    $out .= '</div>';
    $out .= '<a class="left carousel-control" href="#myGallery" role="button" data-slide="prev">';
    $out .= '<span class="glyphicon glyphicon-chevron-left"></span>';
    $out .= '<span class="sr-only">'.__('Previous').'</span>';
    $out .= '</a>';
    $out .= '<a class="right carousel-control" href="#myGallery" role="button" data-slide="next">';
    $out .= '<span class="glyphicon glyphicon-chevron-right"></span>';
    $out .= '<span class="sr-only">'.__('Next').'</span>';
    $out .= '</a>';
    $out .= '</div>';
    $out .= '</div>';
    $out .= '</div>';
    $out .= '</div>';
    $out .= '</div>';
}


if ($user->hasRole('teacher') || $user->isSuperuser() || ($user->hasRole('player') && $user->name === $playerPage->login)) { // Admin is logged or user
    $out .= '<div class="panel-footer">';
    $out .= '<span class="glyphicon glyphicon-star"></span>';
    if ($rightInvasions > 0 || $wrongInvasions > 0) {
        $out .= __('Defensive power').' : <span>'.round(($rightInvasions * 100) / ($wrongInvasions + $rightInvasions)).'%</span>';
        $out .= '('.sprintf(__('You have perfectly repelled %1$s out of %2$s monster invasions'), $rightInvasions, ($rightInvasions + $wrongInvasions)).')';
    } else {
        $out .= __('You have not faced any monster invasions yet.');
    }
    $out .= '</div>';
}
$out .= '</div>';
echo $out;
?>

  <div class="panel panel-success">
    <div class="panel-heading">
    <h4 class="panel-title"><span class=""><span class="glyphicon glyphicon-list"></span> <?php echo __("The Scoreboards"); ?></span></h4>
    </div>
    <div class="panel-body">
      <ul class="list-unstyled col2">
      <?php
      // Scoreboards positions
      $scoreboardRawLink = $pages->get('name=scoreboard')->url;
$totalPlayers = $pages->count("parent.name=players, name!=test");
$totalTeamPlayers = $pages->count("parent.name=players, name!=test, team=$playerPage->team");

// Most active (yearlyKarma)
list($playerGlobalPos, $totalGlobalPlayersNb, $playerTeamPos, $totalTeamPlayersNb) = setScoreboardNew($playerPage, 'yearlyKarma', 'all', true);
$scoreboardLink = $scoreboardRawLink.'?field=yearlyKarma';
echo displayPlayerPos($scoreboardLink, __("Most active"), $playerGlobalPos, $playerTeamPos, $totalGlobalPlayersNb, $totalTeamPlayersNb);

// Most influential (reputation)
list($playerGlobalPos, $totalGlobalPlayersNb, $playerTeamPos, $totalTeamPlayersNb) = setScoreboardNew($playerPage, 'reputation', 'all', true);
$scoreboardLink = $scoreboardRawLink.'?field=reputation';
echo displayPlayerPos($scoreboardLink, __("Most influential"), $playerGlobalPos, $playerTeamPos, $totalGlobalPlayersNb, $totalTeamPlayersNb);

// Greatest # of places (places)
list($playerGlobalPos, $totalGlobalPlayersNb, $playerTeamPos, $totalTeamPlayersNb) = setScoreboardNew($playerPage, 'places', 'all', true);
$scoreboardLink = $scoreboardRawLink.'?field=places';
echo displayPlayerPos($scoreboardLink, __("Greatest # of places"), $playerGlobalPos, $playerTeamPos, $totalGlobalPlayersNb, $totalTeamPlayersNb);

// Greatest # of people (people)
if ($playerPage->rank && $playerPage->rank->is("index>=8")) {
    list($playerGlobalPos, $totalGlobalPlayersNb, $playerTeamPos, $totalTeamPlayersNb) = setScoreboardNew($playerPage, 'people', 'all', true);
    $scoreboardLink = $scoreboardRawLink.'?field=people';
    echo displayPlayerPos($scoreboardLink, __("Greatest # of people"), $playerGlobalPos, $playerTeamPos, $totalGlobalPlayersNb, $totalTeamPlayersNb);
}

// Best warrior (fighting_power)
list($playerGlobalPos, $totalGlobalPlayersNb, $playerTeamPos, $totalTeamPlayersNb) = setScoreboardNew($playerPage, 'fighting_power', 'all', true);
$scoreboardLink = $scoreboardRawLink.'?field=fighting_power';
echo displayPlayerPos($scoreboardLink, __("Best warriors"), $playerGlobalPos, $playerTeamPos, $totalGlobalPlayersNb, $totalTeamPlayersNb);

// Best donators (donation)
list($playerGlobalPos, $totalGlobalPlayersNb, $playerTeamPos, $totalTeamPlayersNb) = setScoreboardNew($playerPage, 'donation', 'all', true);
$scoreboardLink = $scoreboardRawLink.'?field=donation';
echo displayPlayerPos($scoreboardLink, __("Best donators"), $playerGlobalPos, $playerTeamPos, $totalGlobalPlayersNb, $totalTeamPlayersNb);

// Most trained (underground_training)
list($playerGlobalPos, $totalGlobalPlayersNb, $playerTeamPos, $totalTeamPlayersNb) = setScoreboardNew($playerPage, 'underground_training', 'all', true);
$scoreboardLink = $scoreboardRawLink.'?field=underground_training';
echo displayPlayerPos($scoreboardLink, __("Most trained"), $playerGlobalPos, $playerTeamPos, $totalGlobalPlayersNb, $totalTeamPlayersNb);

// Most active groups
/* list($topPlayers, $prevPlayers, $playerPos, $totalPlayers) = getScoreboard($playerPage, 'group'); */
/* if ($playerPos) { */
/*   if ($playerPos === 1) { $star = '<span class="glyphicon glyphicon-star"></span>'; } else { $star=''; } */
/*   echo '<li>'; */
/*   echo '<p>'; */
/*   echo '<a href="'.$pages->get('name=scoreboard')->url.'?field=group"><span class="glyphicon glyphicon-list" data-toggle="tooltip" title="'.__("See the complete scoreboard").'"></span></a> '; */
/*   echo __('Most active groups').' : '.$playerPos.'/'.$totalPlayers.' '.$star; */
/*   echo '</p></li>'; */
/* } else { */
/*   echo '<li>'; */
/*   echo '<p>'; */
/*   echo '<a href="'.$pages->get('name=scoreboard')->url.'?field=underground_training"><span class="glyphicon glyphicon-list" data-toggle="tooltip" title="'.__("See the complete scoreboard").'"></span></a> '; */
/*   echo __('Most active groups').' : '; */
/*   echo __('No ranking.'); */
/*   echo '</p></li>'; */
/* } */
?>
      </ul>
    </div>
  </div>

  <div class="panel panel-success">
    <div class="panel-heading">
      <h4 class="panel-title">
        <span class="glyphicon glyphicon-headphones"></span> <span class=""><?php echo __("My activity"); ?></span>
        <?php
    if ($showDetails) {
        echo '<a href="'.$pages->get("name=results")->url.$playerPage->id.'/activity" class="btn btn-sm btn-danger">'.__("See complete details").'</a>'; // TODO
    }
?>
      </h4>
    </div>
    <?php
      if ($showDetails) {
          ?>
    <div class="panel-body">
    <?php
              $allConcernedMonstersNb = $tmpCache->tmpMonstersActivity->count("trainNb>0, monster.parent.name=monsters");
          $mostTrained = $tmpCache->tmpMonstersActivity->sort("-totalUt")->first();
          $allMonsters = getAllMonsters($playerPage);
          $totalFightsNb =  array_sum(mb_split(',', $tmpCache->tmpMonstersActivity->implode(',', '{fightNb}')));
          $allFoughtMonstersNb = $tmpCache->tmpMonstersActivity->find("fightNb>0")->count();
          $allBattles = battleReport($playerPage);
          bd($allBattles);
          $allFightRequests = $playerPage->get("name=history")->find("template=event, task.name~=fight, inClass=1")->sort("date");
          $attacks = [];
          foreach ($allBattles as $p) { // Group by linkedId
              if ($p->linkedId != '') {
                  $attacks["$p->linkedId"][] = $p;
              } else { // Make a unique id
                  $uniqueId = mt_rand(100000, 999999); // Unique test number
                  $attacks["$uniqueId"][] = $p;
              }
          }
          echo '<table class="table">';
          echo '<tr>';
          echo '<th class="text-left">';
          echo '<span class="glyphicon glyphicon-headphones"></span> '.sprintf(__("Underground Training : %d UT"), $playerPage->underground_training);
          echo '</th>';
          echo '<th class="text-left">';
          echo '<span class="glyphicon glyphicon-flash"></span> '.sprintf(__("Fighting Power : %d FP"), $playerPage->fighting_power);
          echo '</th>';
          echo '<th class="text-left">';
          echo '<span class="glyphicon glyphicon-education"></span> '.sprintf(__("Current school year Monster Attacks : %d "), count($attacks));
          echo '</th>';
          echo '</tr>';
          echo '<tr>';
          echo '<td class="text-left">';
          echo '<p> → '.sprintf(_n('You have trained on 1 monster only (out of %2$s !).', 'You have trained on %1$s different monsters (out of %2$s)', $allConcernedMonstersNb, $allMonsters->count()), $allConcernedMonstersNb, $allMonsters->count()).'</p>';
          $topTrainedNb = $pages->count("parent.name=monsters, bestTrainedPlayerId=$playerPage->id");
          echo '<p> → '.sprintf(_n('You are the top trained player on 1 monster', 'You are the top trained player on %d monsters', $topTrainedNb), $topTrainedNb).'</p>';
          echo '</td>';
          echo '<td class="text-left">';
          echo '<p> → ';
          echo sprintf(_n('You have done %d fight ', 'You have done %d fights ', $totalFightsNb), $totalFightsNb);
          echo sprintf(_n('on %d monster', 'on %d different monsters', $allFoughtMonstersNb), $allFoughtMonstersNb);
          if ($totalFightsNb < 10) {
              echo ' <span class="label label-danger">';
              echo ' <span class="glyphicon glyphicon-thumbs-down"></span> ';
              echo __("The Fighters playground is still locked !");
              echo ' <span class="glyphicon glyphicon-info-sign" datat-toggle="tooltip" title="'.__("10 fights are required to become a Fighter and gain access to the Fighters playground.").'" onmouseenter="$(this).tooltip(\'show\');"></span> ';
              echo '</span>';
          } else {
              echo ' <span class="label label-success">';
              echo ' <span class="glyphicon glyphicon-thumbs-up"></span> ';
              echo __("The Fighters playground is unlocked !");
              echo '</span>';
          }
          echo '</p>';
          echo '<p> → ';
          $medals = $tmpCache->tmpMonstersActivity->find('fightNb>=5, quality>0.3')->count();
          if ($medals > 0) {
              echo __("You have won");
              echo ' <span class="label label-success"><span class="glyphicon glyphicon-certificate"></span> '.sprintf(_n('%d medal', '%d medals', $medals), $medals);
              echo ' <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" title="'.__('You need at least 5 fights on a monster with a positive average.').'"></span>';
              echo '</span>';
              echo ' [<a href="'.$pages->get("name=defeated")->url.$playerPage->id.'">'.__('See defeated monsters gallery').'</a>]';
          } else {
              echo ' <span class="label label-danger"><span class="glyphicon glyphicon-thumbs-down"></span> '.sprintf(__('No medals'));
              echo ' <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" title="'.__('You need at least 5 fights on a monster with a positive average.').'"></span>';
              echo '</span>';
          }
          echo '</p>';
          echo '<p> → ';
          echo '<span class="glyphicon glyphicon-time"></span> ';
          $bestTimeNb = $pages->count("parent.name=monsters, bestTimePlayerId=$playerPage->id");
          echo sprintf(_n('You have the best time on 1 monster.', 'You have the best time on %d monsters.', $bestTimeNb), $bestTimeNb);
          echo '</p>';
          echo '</td>';
          echo '<td class="text-left">';
          if (count($attacks) > 0) {
              $pos = 0;
              $neg = 0;
              $vv = 0;
              $v = 0;
              $r = 0;
              $rr = 0;
              foreach ($attacks as $key => $gr) {
                  $count = count($gr);
                  if ($count > 1) {
                      foreach ($gr as $key => $m) {
                          switch ($m->task->name) {
                              case 'battle-vv': $vv++;
                                  $pos++;
                                  break;
                              case 'battle-v': $v++;
                                  $pos++;
                                  break;
                              case 'battle-r': $r++;
                                  $neg++;
                                  break;
                              case 'battle-rr': $rr++;
                                  $neg++;
                                  break;
                              default: break;
                          }
                      }
                  }
              }
              echo '<p> → ';
              echo __('Global results : ');
              if ($vv != 0) {
                  echo '<span class="label label-success">'.$vv.'VV</span>';
              }
              if ($v != 0) {
                  echo ' <span class="label label-success">'.$v.'V</span>';
              }
              if ($r != 0) {
                  echo ' <span class="label label-danger">'.$r.'R</span>';
              }
              if ($rr != 0) {
                  echo ' <span class="label label-danger">'.$rr.'RR</span>';
              }
              echo '</p>';
              echo '<p> → ';
              echo '<a href="'.$pages->get("name=results")->url.$playerPage->id.'" target="_blank">[<span class="glyphicon glyphicon-stats"></span> '.__('See my results analysis.').']</a>';
              echo '</p>';
          } else {
              echo "<p>".__("You haven't faced any monster attacks during the current school year.")."</p>";
              echo '<p> → ';
              echo '<a href="'.$pages->get("name=results")->url.$playerPage->id.'" target="_blank">[<span class="glyphicon glyphicon-stats"></span> '.__('See my old results analysis.').']</a>';
              echo '</p>';
          }
          echo '<p> → ';
          echo sprintf(_n('You have done 1 fight request.', 'You have done %d fight requests.', $allFightRequests->count()), $allFightRequests->count());
          echo '</p>';
          echo '</td>';
          echo '</tr>';
          echo '</table>';
          ?>
      <div class="panel-footer">
        <?php
              if ($user->isSuperuser() || $user->hasRole('teacher')) {
                  echo '→ <span class="glyphicon glyphicon-headphones"></span> <a href="'.$trainingZone->url.'?playerId='.$playerPage->id.'">'.__("Use the Memory Helmet (Training Zone)").'</a>&nbsp;&nbsp;&nbsp;';
                  echo '→ <span class="glyphicon glyphicon-flash"></span> <a href="'.$fightingZone->url.$playerPage->id.'?playerId='.$playerPage->id.'">'.__("Go to the Fighting Zone").'</a>&nbsp;&nbsp;&nbsp;';
                  echo '→ <span class="glyphicon glyphicon-time"></span> <a href="'.$playground->url.$playerPage->id.'?playerId='.$playerPage->id.'">'.__("Go to the Fighters playground").'</a>';
              }
          ?>
      </div>
    </div>
    </div>
    <?php
      } else {
          echo '<div class="panel-body"><p>'.__("Details are private.").'</p></div>';
      }

if ($showDetails) {
    ?>
      <div class="panel panel-success">
        <div class="panel-heading">
          <h4>
            <?php
                echo __('History').' ';
    $currentPeriod = $playerPage->team->periods;
    if ($currentPeriod != false) {
        $mod = $currentPeriod->periodOwner->get("singleTeacher=$playerPage->team->teacher->eq(0)"); // Get personalized infos if needed
        if ($mod) {
            $mod->dateStart != '' ? $dateStart = $mod->dateStart : '';
            $mod->dateEnd != '' ? $dateEnd = $mod->dateEnd : '';
        } else {
            $dateStart = $currentPeriod->dateStart;
            $dateEnd = $currentPeriod->dateEnd;
        }
        echo __('[from beginning of period').' : '.date('d M', $dateStart).']'.' ';
    } else {
        $limitDate  = new \DateTime("-30 days");
        $dateStart = $limitDate->format('U');
        echo __('[for the last 30 days]').' ';
    }
    if ($user->name == $playerPage->name || $user->isSuperuser() || ($user->hasRole('teacher') && $playerPage->team->teacher->has("id=$user->id"))) {
        // Ajax reload of complete history
        echo '<a href="#" data-href="'.$pages->get('name=ajax-content')->url.'?id=history&limit=0&playerId='.$playerPage->id.'" data-targetId="historyPanel" data-hide-feedback="true" class="simpleAjax btn btn-xs btn-danger">'.__("Reload complete history").'</a>';
    }
    if ($user->isSuperuser() || ($user->hasRole('teacher') && $playerPage->team->teacher->has("id=$user->id"))) {
        echo ' <a target="blank" href="'.$adminActions->url.'recalculate/'.$playerPage->id.'">'.__("[Edit history]").'</a>';
    }
    ?>
          </h4>
        </div>
        <div id="historyPanel" class="panel-body ajaxContent" data-priority="1" data-href="<?php echo $pages->get('name=ajax-content')->url; ?>" data-id="history&limit=<?php echo $dateStart; ?>&playerId=<?php echo $playerPage->id; ?>">
          <p class="text-center"><img src="<?php echo $config->urls->templates; ?>img/hourglass.gif"></p>
        </div>
      </div>
      <?php if ($user->hasRole('teacher') || $user->isSuperuser()) { ?>
        <div class="panel panel-success">
          <div class="panel-heading">
            <?php
    foreach ($playerPage->pads as $pad) {
        echo '<h5 class="panel-title">';
        echo '<span class="label label-primary">'.__('Personal pad').'</span> ';
        echo '<label for="editedBox" class="btn btn-sm">';
        $pad->editedBox == 1 ? $status = 'checked="checked"' : $status = '';
        echo '<input type="checkbox" id="editedBox" name="editedBox" data-href="'.$submitforms->url.'?form=forceOption&pageId='.$pad->id.'&optionName=editedBox" class="simpleAjax" data-hide-feedback="true" '.$status.' /> ';
        echo __("Edited");
        echo '</label> ';
        echo '<label for="alertBox" class="btn btn-sm">';
        $playerPage->pads->first->alertBox == 1 ? $status = 'checked="checked"' : $status = '';
        echo '<input type="checkbox" id="alertBox" name="alertBox" data-href="'.$submitforms->url.'?form=forceOption&pageId='.$pad->id.'&optionName=alertBox" class="simpleAjax" data-hide-feedback="true" '.$status.' /> ';
        echo __("Published on Newsboard");
        echo '</label> ';
        echo ' <a href="#" class="btn btn-danger btn-sm ajaxBtn" data-type="quickValidation" data-playerId="'.$playerPage->id.'">['.__("Quick validation").'] </a>';
        $lastModDate = date("d M Y, H:i:s", $pad->modified);
        echo ' <span>['.sprintf(__("Last modified on %s"), $lastModDate).']</span>';
        echo '</h5>';
        ?>
            </div>
            <div id="padPanel" class="panel-body">
              <div class="pad">
                <?php if ($pad->pad == '') {
                    $pad->pad = '<br /><br />';
                } echo $pad->edit('pad'); ?>
              </div>
            </div>
          </div>
        <?php }
    }
} ?>
</div>
<?php include("./foot.inc"); ?>
