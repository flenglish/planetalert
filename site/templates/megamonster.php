<?php namespace ProcessWire;
  include("./head.inc"); 
  
  $ajaxContentUrl = $pages->get("name=ajax-content")->url;
  echo '<div id="showInfo" data-href="'.$ajaxContentUrl.'"></div>';

  // check for login before outputting markup
  if($input->post->username && $input->post->pass) {
    $userName = $sanitizer->pageName($input->post->username);
    $pass = $input->post->pass; 
    if($session->login($userName, $pass)) {
      $session->redirect($page->url); // Redirect logged user to page to set up all variable
    }
  }

  if (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident') !== FALSE || strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE) { // IE detected
    echo $wrongBrowserMessage;
  } else {
    if (!$user->isLoggedin()) {
      echo '<div class="row">';
        echo '<p class="well text-center">'.__("You MUST log in to access this page !").'</p>';
          echo '<div class="col-md-10 text-center">';
          if($input->post->username || $input->post->pass) echo "<h3><span class='label label-danger'>Login failed... (check user name or password)</span></h3>";
          echo '<form class="form-horizontal loginForm" action="'.$page->url.'" method="post">';
            echo '<div class="form-group">';
              echo '<label for="username" class="col-sm-4 control-label">User :</label>';
              echo '<div class="col-sm-6">';
                echo '<input class="form-control" type="text" name="username" id="username" placeholder="Username" />';
              echo '</div>';
            echo '</div>';
            echo '<div class="form-group">';
              echo '<label for="pass" class="col-sm-4 control-label">Password :</label>';
              echo '<div class="col-sm-6">';
                echo '<input class="form-control" type="password" name="pass" id="pass" placeholder="Password" /></label></p>';
              echo '</div>';
            echo '</div>';
          echo '<input type="submit" class="btn btn-info" name="submit" value="Connect" />';
          echo '</form>';
        echo '</div>';
        echo '</div>';
    } else {
      $helmet = $pages->get("name=memory-helmet");
      // Check for publish state from exerciseOwner or created_users_id
      if ($user->isSuperuser() || $user->hasRole('teacher') || ($user->hasRole('player') && $page->exerciseOwner->get("singleTeacher=$headTeacher") != NULL && $page->exerciseOwner->get("singleTeacher=$headTeacher")->publish == 1) || $player->team->is("name=test-team")) {
        if ($user->isSuperuser() || $user->hasRole('teacher')) {
          $player = $pages->get("parent.name=players, name=test");
          $teacherView = true;
          $access = true;
        } else {
          $teacherView = false;
          if ($player->team->skills->has("name=elite")) { // Team must have freed 100% of the world
            $access = true;
          } else {
            $access = false;
          }
        }
        if ($access || $teacherView) { // Training session
          $out = '';
          $redirectUrl = $pages->get('name=headquarters')->url.$player->team->name;
          $monster = $page;
          if ($teacherView) { $out .= '<h3 class="text-center"><span class="label label-danger text-uppercase"><i class="glyphicon glyphicon-warning-sign"></i> '.__("Teacher access !").'</span></h3>'; }
          $out .= '<div ng-app="exerciseApp">';
            $out .= '<div class="row" ng-controller="TrainingCtrl" ng-init="init(\''.$pages->get("name=service-pages")->url.'\', \'megamonster\', \''.$monster->id.'\', \''.$redirectUrl.'\', \''.$player->id.'\', \''.$pages->get("name=submit-fight")->url.'\')">';
            if ($monster->id) { // Training session starts
              $out .= '<div class="col-sm-12 text-center">';
                $out .= ' <div class="pull-right text-center alert alert-info">';
                  $out .= '<p><small>'.__("Helmet programmed for").' :</small></p>';
                  if ($monster->image) { $mini = '<img src="'.$monster->image->getCrop('small')->url.'" alt="'.$page->title.'." />'; }
                  $out .= '<p>'.$mini.'</p>';
                  $out .= '<small>';
                    $out .= $monster->title;
                    $out .= ' <span class="glyphicon glyphicon-eye-open" data-toggle="tooltip" title="'.$monster->summary.'"></span>';
                  $out .= '</small>';
                $out .= '</div>';
              $out .= '<h2>';
              $out .= '<span ng-class="{label:true, \'label-success\':true}">'.__("Training session");
              $out .= ' <span class="blink">'.__("started").'</span></span> ';
              $out .= ' → ';
              $out .= ' <span ng-class="{label:true, \'label-default\':true, \'blink\':correct}">'.__("Current counter").': {{counter}}</span>';
              $out .= ' → ';
              $out .= ' <span class="label label-primary">+{{result}}'.__("UT").'</span>';
              $out .= ' <small><span class="glyphicon glyphicon-info-sign" data-toggle="tooltip" title="'.__("10 words/sentences = +1UT").'"></span></small>';
              $out .= '</h2>';
              $out .= '<div class="well trainingBoard" ng-show="waitForStart">'.__("Please wait while loading data...").'</div>';
              $out .= '<div class="well trainingBoard" ng-hide="waitForStart">';
                if ($monster->type->is("name=image-map|quiz")) {
                  if ($monster->imageMap && $monster->imageMap->count() > 0) {
                    $out .= '<div><img src="'.$monster->imageMap->first()->url.'" max-width="800" alt="numbered vocabulary." /></div>';
                  }
                }
                if ($monster->instructions != '') {
                  $out .= '<span class="pull-right glyphicon glyphicon-question-sign" data-toggle="tooltip" data-html="true" title="'.$monster->instructions.'"></span>';
                }
                $out .= '<div class="bubble-right">';
                  if ($monster->type->name == 'jumble') {
                    $out .= '<div class="text-center">';
                    $out .= '<h2 class="jumbleW inline" ng-repeat="w in word track by $index">';
                    $out .= '<span ng-class="{\'label\':true, \'label-primary\':selectedItems.indexOf($index) === -1, \'label-warning\':selectedItems.indexOf($index) !== -1}" ng-click="pickWord(w, $index)" ng-bind-html="w|paTags"></span>';
                    $out .= '</h2>';
                    $out .= '</div>';
                    $out .= ' <h3><span ng-show="wrong"><span class="glyphicon glyphicon-arrow-right" ng-show="wrong"></span> {{showCorrection}} {{feedback|paTags}}</span></h3> ';
                    $out .= '<button class="btn btn-danger btn-xs" ng-click="clear()">'.__("Try again").'</button> ';
                    $out .= '<span class="lead pull-right" data-toggle="tooltip" data-html="true" data-original-title="{{mixedWord}}" ng-show="showClue"><span class="glyphicon glyphicon-sunglasses"></span></span>';
                    $out .= '<br /><br />';
                    $out .= '<h3 id="" ng-bind="playerAnswer"></h3>';
                    $out .= '<p class="text-right">';
                      $out .= '<button ng-click="attack()" class="actionBtn btn btn-success">'.__("Stimulate !").'</button>';
                      $out .= '<button ng-click="dodge()" class="actionBtn btn btn-danger">'.__("I don't know").'</button>';
                    $out .= '</p>';
                  } else if ($monster->type->name == 'categorize') {
                    $out .= '<div class="text-center">';
                    $out .= '<h2 class="inline" ng-bind-html="word|paTags"></h2>   ';
                    $out .= '<span class="lead pull-right" data-toggle="tooltip" data-html="true" data-original-title="{{mixedWord}}" ng-show="showClue"><span class="glyphicon glyphicon-sunglasses"></span></span>';
                    $out .= ' <h3><span ng-show="wrong"><span class="glyphicon glyphicon-arrow-right" ng-show="wrong"></span> {{showCorrection}}</span></h3> ';
                    $out .= '</div>';
                    $out .= '<br />';
                    $out .= '<h2 class="category inline" ng-repeat="c in categories">';
                    $out .= '<span ng-click="pickCategory(c)" ng-bind-html="c|paTags"></span>';
                    $out .= '</h2>';
                  } else {
                    $out .= '<div class="text-center">';
                    $out .= '<h2 class="inline" ng-bind-html="word|paTags"></h2>   ';
                    $out .= '<span class="lead pull-right" data-toggle="tooltip" data-html="true" data-original-title="{{mixedWord}}" ng-show="showClue"><span class="glyphicon glyphicon-sunglasses"></span></span>';
                    $out .= ' <h3 class="inline"><span ng-show="wrong"><span class="glyphicon glyphicon-arrow-right" ng-show="wrong"></span> <span ng-bind-html="showCorrection|underline"></span> {{feedback}}</span></h3> ';
                    $out .= '</div>';
                    $out .= '<br />';
                    $out .= '<input type="text" class="input-lg" ng-model="playerAnswer" size="50" placeholder="'.__("Type your answer").'" autocomplete="off" my-enter="attack()" sync-focus-with="isFocused" />';
                    $out .= '<p class="text-right">';
                      $out .= '<button ng-click="attack()" class="actionBtn btn btn-success">'.__("Stimulate !").'</button>';
                      $out .= '<button ng-click="dodge()" class="actionBtn btn btn-danger">'.__("I don't know").'</button>';
                    $out .= '</p>';
                  }
              $out .= '</div>';
              $out .= '<span class="avatarContainer">';
                if (isset($player) && $player->avatar) {
                  $out .= '<img class="" src="'.$player->avatar->getCrop("thumbnail")->url.'" alt="'.$player->title.'." />';
                }
                if ($helmet->image) {
                  $out .= '<img class="helmet superpose squeeze" src="'.$helmet->image->url.'" alt="memory helmet." />';
                }
              $out .= '</span>';
              $out .= '<h4>';
              if (!$user->isSuperuser() && !$user->hasRole('teacher')) {
                list($utGain, $inClassUtGain) = utGain($monster, $player);
                $out .= ' ('.__("Your global UT for this monster").': '.($utGain+$inClassUtGain).')';
              }
              $out .= '</h4>';
              $out .= '</div>';
              $out .= '<button ng-click="stopSession()" class="btn btn-danger" ng-hide="waitForStart" ng-disabled="">'.__("Quit").'</button>';
              $out .= '</div>';
              $out .= '</div>';
            } else {
              $out .= __("Sorry, but a problem occured. Please try again. If the problem still exists, contact the administrator.");
            }
          $out .= '</div>';
          
          echo $out;
        } else {
          echo __("A problem has occurred. Please tell the administrator.");
        }
      } else {
        echo $noAuthMessage;
      }
    }
  }
  include("./foot.inc"); 
?>
