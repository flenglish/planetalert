<?php namespace ProcessWire;

include("./head.inc"); 

$minLevel = 15;
$allPlayers = $pages->find("template=player, parent.name=players, level>=$minLevel")->shuffle();

$out = '';

$out .= '<section class="row">';
$out .= '<h1 class="well text-center">';
$out .= $page->title;
$out .= ' <small>'.sprintf(__('%1$s players of level %2$s and higher'), $allPlayers->count(), $minLevel).'</small>';
$out .= '<span class="pull-right" data-toggle="tooltip" title="'.__("The higher your level is, the bigger your avatar is ;)").'"><span class="glyphicon glyphicon-question-sign"></span></span>';
$out .= '</h1>';

$out .= '<div class="grid">';
foreach ($allPlayers as $p) {
  if ($p->team->name != 'no-team') {
    $team = ' ['.$p->team->title.']';
  } else {
    $team = '';
  }
  if ($p->avatar) {
    if ($p->level == $minLevel ) { $class = 'grid-item'; }
    if ($p->level > $minLevel && $p->level<=$minLevel+5) { $class = 'grid-item--width2'; }
    if ($p->level > $minLevel+5 && $p->level<=$minLevel+10) { $class = 'grid-item--width3'; }
    if ($p->level > $minLevel+10 && $p->level<=$minLevel+15) { $class = 'grid-item--width4'; }
    if ($p->level > $minLevel+15) { $class = 'grid-item--width5'; }
    $out .= '<div class="'.$class.' playerDiv">';
    $out .= '<a href="'.$p->url.'"><img class="img-thumbnail" data-toggle="tooltip" data-html="true" title="'.$p->title.$team.'<br />'.__("Level").' '.$p->level.'" src="'.$p->avatar->url.'" alt="'.$p->title.'." /></a>';
    $out .= '</div>';
  }
}
$out .= '</div>';

$out .= '</section>';

$pages->unCacheAll();

echo $out;

include("./foot.inc"); 
?>
