<?php namespace ProcessWire;
  include("./head.inc"); 

  if ($user->hasRole('teacher') || $user->isSuperuser()) {
    $team = $selectedTeam;
    include("./tabList.inc");
  }
  if ($user->hasRole('player')) {
    $team = $player->team;
  }

  $out = '';

  if ($user->isLoggedin()) {
    $showAll = true;
    if ($user->isSuperuser() || $user->hasRole('teacher')) {
      $showAll = false;
      if (isset($input->urlSegment2) && $input->urlSegment2 == 'all') {
        $showAll = true;
      }
      // Reload to include 'no-team' players
      if ($user->isSuperuser()) {
        $globalPlayers = $pages->find("parent.name=players, template=player, name!=test")->sort('team.name, title');
      } else {
        if ($showAll) {
          $globalPlayers = $pages->find("parent.name=players, template=player, team.teacher=$headTeacher")->sort('title');
        } else {
          $globalPlayers = $pages->find("parent.name=players, template=player, team=$selectedTeam")->sort('title');
        }
      }
      if (isset($input->urlSegment3)) {
        $playerId = $input->urlSegment3;
        $selectedPlayer = $pages->get($playerId);
        $maxAmount = $selectedPlayer->GC;
        $hidden = '';
      } else {
        $playerId = 0;
        $hidden = 'hidden';
        $maxAmount = 1000;
      }
    } else {
      $playerId = $player->id;
      $maxAmount= $player->GC;
      $globalPlayers = $pages->find("parent.name=players, template=player, team.teacher=$headTeacher")->sort('title');
    }
    if ($input->get->type == 'team') {
      $maxAmount= $player->team->GC;
      if ($player->team->GC > 0) {
        $out .= '<h3 class="well text-center">';
          $out .= '<img src="'.$config->urls->templates.'img/gold_mini.png" alt="chest" /> ';
          $out .= sprintf(__('Get gold coins from your team chest : %1$sGC available !'), $player->team->GC);
          $out .= ' <span class="glyphicon glyphicon-question-sign pull-right" data-toggle="tooltip" title="'.__("This is a solidarity space. If you have lots of gold coins, you can donate some of them to your team chest so other players can use them if they need.").'"></span>';
        $out .= '</h3>';
        $out .= "<section id='' class='well text-center'>";
          $out .= '<form id="donateForm" name="donateForm" action="'.$pages->get("name=submitforms")->url.'" method="post" class="form-horizontal" role="form">';
          $out .= '<div class="form-group has-warning has-feedback">';
            $out .= '<label for="amount" class="col-sm-5 control-label">'.__('Amount').' (<span id="maxAmount">'.$maxAmount.'</span>GC max.) : </label>';
            $out .= '<div class="col-sm-5">';
            $out .= '<input id="amount" name="amount" type="number" min="0" max="'.$maxAmount.'" data-max="'.$maxAmount.'" size="5" placeholder="0" class="form-control" />';
            $out .= '<span class="glyphicon glyphicon-warning-sign form-control-feedback" aria-hidden="true"></span>';
            $out .= '</div>';
          $out .= '</div>';
          $out .= '<span class="glyphicon glyphicon-hand-up"></span> '.__('Take only what you need and don\'t forget other players ');
          $out .= '<input type="hidden" id="chest" name="chest" value="1" />';
          $out .= ' <input id="donateFormSubmit" name="donateFormSubmit" type="submit" class="form-control btn btn-primary btn-sm" value="'.__('Validate !').'" disabled="true" />';
          $out .= '</form>';
        $out .= '</section>';
      } else {
        $out .= '<h3 class="well text-center">';
          $out .= __('Your team chest is empty (0GC)');
          $out .= ' <span class="glyphicon glyphicon-question-sign pull-right" data-toggle="tooltip" title="'.__("This is a solidarity space. If you have lots of gold coins, you can donate some of them to your team chest so other players can use them if they need.").'"></span>';
        $out .= '</h3>';
      }
    } else {
      if ($user->hasRole("player")) {
        $out .= '<h3 class="well text-center">'.sprintf(__('Make a donation : %sGC available !'), $maxAmount).'</h3>';
      } else {
        $out .= '<h3 class="well text-center">'.__('Make a donation !');
        if ($input->urlSegment1) {
          if (isset($input->urlSegment2) && $input->urlSegment2 == 'all') {
            $out .= '<a class="pull-right btn btn-primary btn-xs" href="'.$page->url.$selectedTeam->name.'">'.__("Team selection").'</a>';
          } else {
            $out .= '<a class="pull-right btn btn-primary btn-xs" href="'.$page->url.$selectedTeam->name.'/all">'.__("All players selection").'</a>';
          }
        }
        $out .= '</h3>';
      }
      $out .= "<section id='donationDiv' class='well text-center'>";
      $out .= '<form id="donateForm" name="donateForm" action="'.$pages->get("name=submitforms")->url.'" method="post" class="form-horizontal" role="form">';
      if ($user->isSuperuser() || $user->hasRole('teacher')) { // Donator selection
        $out .= '<div class="form-group has-warning has-feedback">';
        $out .= '<label for="donator" class="col-sm-5 control-label">'.__("Donation from").' : </label>';
        $out .= '<div class="col-sm-5">';
        $out .= '<select class="form-control" id="donator" name="donator">';
          $out .= '<option value="0" data-gc="0">'.__("Select a donator").'</option>';
          $out .= '<option value="'.$user->id.'" data-gc="100">'.__("Teacher").'</option>';
          $emptyPlyr = 0;
          if ($showAll) {
            foreach($allTeams as $t) {
              if ($t->is("name!=no-team")) {
                if ($t->GC == '') { $t->GC = 0; }
                if ($t->GC > 0) {
                  $out .= '<option value="'.$t->id.'">'.sprintf(__("%s Team chest"), $t->title).' ('.$t->GC.' GC)</option>';
                } else {
                  $emptyPlyr++;
                }
              }
            }
          } else {
            if ($selectedTeam->GC == '') { $selectedTeam->GC = 0; }
            if ($selectedTeam->GC > 0) {
              $out .= '<option value="'.$selectedTeam->id.'">'.sprintf(__("%s Team chest"), $selectedTeam->title).' ('.$selectedTeam->GC.' GC)</option>';
            } else {
              $emptyPlyr++;
            }
          }
          foreach ($globalPlayers as $plyr) {
            if ($plyr->GC > 0) {
              if ($plyr->id == $playerId) {
                $selected = ' selected="selected"';
              } else {
                $selected = '';
              }
              if ($plyr->team->name != 'no-team') {
                $team = ' ['.$plyr->team->title.']';
              } else {
                $team = '';
              }
              $out .= '<option value="'.$plyr->id.'" data-gc="'.$plyr->GC.'" '.$selected.'>'.$plyr->title.$team.' ('.$plyr->GC.' GC)</option>';
            } else {
              $emptyPlyr++;
            }
          }
        $out .= '</select>';
        $out .= '<span class="glyphicon glyphicon-warning-sign form-control-feedback '.$hidden.'" aria-hidden="true"></span> ';
        $out .= sprintf(__('[%d player⋅s/team chest⋅s with 0GC]'), $emptyPlyr);
        $out .= '</div>';
        $out .= '</div>';
      } else {
        $out .= '<input type="hidden" id="donator" name="donator" value="'.$playerId.'" />';
      }
      $out .= '<div class="form-group has-warning has-feedback">';
      $out .= '<label for="receiver" class="col-sm-5 control-label">'.__("Donate to").' : </label>';
      $out .= '<div class="col-sm-5">';
      $out .= '<select class="form-control" id="receiver" name="receiver">';
        $out .= '<option value="0">'.__('Select a receiver').'</option>';
        if ($showAll) {
          foreach($allTeams as $t) {
            if ($t->is("name!=no-team")) {
              if ($t->GC == '') { $t->GC = 0; }
              $out .= '<option value="'.$t->id.'">'.sprintf(__("%s Team chest"), $t->title).' ('.$t->GC.' GC)</option>';
            }
          }
        } else {
          if ($selectedTeam->GC == '') { $selectedTeam->GC = 0; }
          $out .= '<option value="'.$selectedTeam->id.'">'.sprintf(__("%s Team chest"), $selectedTeam->title).' ('.$selectedTeam->GC.' GC)</option>';
        }
        foreach ($globalPlayers as $plyr) {
          if ($plyr->id != $playerId) {
            if ($plyr->team->name != 'no-team') { 
              if ($showAll) {
                $teamTitle = ' ['.$plyr->team->title.']';
              } else {
                $teamTitle = '';
              }
              $out .= '<option value="'.$plyr->id.'">'.$plyr->title.' '.$teamTitle.' ('.$plyr->GC.' GC)</option>';
            } else { 
              $teamTitle = '';
              $out .= '<option value="'.$plyr->id.'">'.$plyr->title.' '.substr($plyr->lastName, 0, 1).$teamTitle.' ('.$plyr->GC.' GC)</option>';
            }
          }
        }
      $out .= '</select>';
      $out .= '<span class="glyphicon glyphicon-warning-sign form-control-feedback" aria-hidden="true"></span>';
      $out .= '</div>';
      $out .= '</div>';
      $out .= '<div class="form-group has-warning has-feedback">';
      if ($user->isSuperuser() || $user->hasRole('teacher')) {
        $out .= '<label for="amount" class="col-sm-5 control-label">'.__("Amount").' (<span class="glyphicon glyphicon-warning-sign"></span>&nbsp;<span id="maxAmount">'.$maxAmount.'</span>GC max.) : </label>';
      } else {
        $out .= '<label for="amount" class="col-sm-5 control-label">'.__('Amount').' (<span id="maxAmount">'.$maxAmount.'</span>GC max.) : </label>';
      }
      $out .= '<div class="col-sm-5">';
      $out .= '<input id="amount" name="amount" type="number" min="0" max="'.$maxAmount.'" data-max="'.$maxAmount.'" size="5" placeholder="0" class="form-control" />';
      $out .= '<span class="glyphicon glyphicon-warning-sign form-control-feedback" aria-hidden="true"></span>';
      $out .= '</div>';
      $out .= '</div>';
      $out .= '<div class="form-group has-warning has-feedback hidden">';
        $out .= '<label for="extraComment" class="col-sm-5 control-label">'.__("Extra comment").' : </label>';
      $out .= '<div class="col-sm-5">';
      $out .= '<input id="extraComment" name="extraComment" type="text" size="5" placeholder="'.__('Comment').'" class="form-control" />';
      $out .= '</div>';
      $out .= '</div>';
      $out .= ' <input id="donateFormSubmit" name="donateFormSubmit" type="submit" class="form-control btn btn-primary btn-sm" value="'.__('Donate !').'" disabled="true" />';
      $out .= '</form>';
      $out .= '</section>';
    }

  } else {
    $out .= '<p>'.__("You don't have permission to make a donation. If you think this is an error, please contact the Administrator.").'</p>';
  }

  echo $out;

  include("./foot.inc"); 
?>
