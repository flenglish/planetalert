<?php namespace ProcessWire;
  include("./head.inc");

  $out = '';

  // Test if a player is connected
  if ($user->hasRole('player')) { // Show player's mini-profile
    echo '<div class="row well lead text-center">';
      include("./miniProfile.inc"); 
      echo $out;
      $item = possibleElement($player, $page);
      // helpAlerts
      switch($item->pb) {
        case 'possible' : 
          $helpAlert = true;
          $helpTitle = __("You can buy this item !");
          break;
        case 'helmet' : 
          $helpAlert = true;
          $helpTitle = __("Memory helmet required !");
          $link = '<a href="'.$pages->get("name=memory-helmet")->url.'">Memory Helmet</a>';
          $helpMessage = sprintf(__('You must buy the %s first before buying this item'), $link);
          break;
        case 'already' : 
          $helpAlert = true;
          $helpTitle = __("You already own this item !");
          break;
        case 'freeActs' : 
          $nbEl = $player->places->count()+$player->people->count();
          $helpAlert = true;
          $helpTitle = __("More free elements required !");
          $helpMessage = sprintf(__('This item requires %1$s free elements ! You have only %2$s free elements.'), $item->freeActs, $nbEl);
          break;
        case 'GC' : 
          $helpAlert = true;
          $helpTitle = __("Not enough GC !");
          $helpMessage =  sprintf(__('This item requires %1$s GC ! You have only %2$sGC !'), $item->GC, $player->GC);
          break;
        case 'level' : 
          $helpAlert = true;
          $helpTitle = __("Low Level !");
          $helpMessage =  sprintf(__('This item requires a level %1$s ! You are only at level %2$s !'), $item->level, $player->level);
          break;
        default: 
          $helpAlert = true;
          $helpTitle = __("You can't buy this item for the moment. Sorry.");
      }
    echo '</div>';
    include("./helpAlert.inc.php");
  }

  $thumbImage = $page->photo->eq(0)->url;
  $imageLink = $page->photo->eq(0)->description;
  $nationality = $page->nationality;
  $country = $page->country;

  if ($user->isSuperuser() || $user->hasRole('teacher')) {
    echo '<section class="row">';
    echo '<a class="pdfLink btn btn-info" href="'.$page->url.'?pages2pdf=1">Get PDF ['.$page->title.']</a>';
    echo '</section>';
  }
?>
  
  <section class="row">
    <section class="col-sm-3 text-center">
      <div class="board panel panel-primary">
      <div class="panel-heading">
        <?php
          if ($user->isSuperuser()) {
            echo '<p class="pull-right btn btn-default">'.$page->feel(array("fields" => "title,summary,level,GC,photo,map")).'</p>';
          }
        ?>
        <h1 class="panel-title"><span class="lead"><?php echo $page->title; ?></span></h1>
      </div>
      <div class="panel-body text-center">
        <?php
          if ($imageLink != '') {
             echo '<a target="_blank" href="'.$imageLink.'"><img class="img-rounded img-responsive" src="'.$thumbImage.'" alt="'.$page->title.' photo" data-toggle="tooltip" data-placement="right" title="'.__("Click to enlarge and see attributions").'" /></a>';
          } else {
            echo '<img class="img-thumbnail img-responsive" src="'.$thumbImage.'" alt="'.$page->title.'" />';
          }
          if ($nationality) {
            echo '<h4 class="">'.__("Nationality").' : '.$nationality.'</h4>';
          } 
          if ($country) {
            echo '<h4 class="">'.__("Country").' : ';
            echo '<a href="'.$pages->get("name=people")->url.$country->name.'" data-toggle="tooltip" data-placement="right" title="'.__("See all people from ").$country->title.' data-placement="bottom">'.$country->title.' </a></h4>';
          } 
        ?>
        <hr />
        <h4 class="">
          <?php echo __('Level').' <span class="">'.$page->level; ?></span>
          &nbsp;&nbsp;
          <span class="badge"><?php echo $page->GC.__("GC"); ?></span>
        </h4>
      </div>
      <div class="panel-footer text-center">
        <a href="<?php echo $pages->get("name=people")->url; ?>"><?php echo __("See all people list"); ?></a>
      </div>
      </div>
    </section>
    <section class="col-sm-9">
      <?php
      echo '<p class="well lead text-justify">';
        echo $page->summary;
        echo '<span class="btn btn-info pull-right"><a href="'.$page->link.'">'.__("[Read more about this person]").'</a></span>';
        if ($user->language->name != 'french') {
          $page->of(false);
          if ($page->summary->getLanguageValue($french) != '') {
            echo ' <a class="frenchVersion" data-toggle="collapse" href="#collapseDiv" aria-expanded="false" aria-controls="collapseDiv">'.__("[French version]").'</a>';
            echo '<div class="collapse" id="collapseDiv">';
            echo '<div class="well">';
            echo $page->summary->getLanguageValue($french);
            echo '</div>';
            echo '</div>';
          }
        }
        echo '</p>';
        $map = $modules->get('MarkupLeafletMap');
        echo $map->getLeafletMapHeaderLines();
        if ($page->map->zoom > 5) {
          $page->map->zoom = 2;
        }
        $options = array(
          'markerIcon' => 'flag',
          'markerColour' => 'black',
          'popupFormatter' => function($page) {
            if ($page->city->id) {
              $out[] = '<strong>'.sprintf(__('Born in %1$s, in %2$s (%3$s)'), $page->city->title, $page->country->title, $page->nationality).'</strong>';
            } else {
              $out[] = '<strong>'.sprintf(__('From %1$s (%2$s)'), $page->country->title, $page->nationality).'</strong>';
            }
            $out[] = "<p class=\"text-center\"><img src=\"{$page->photo->eq(0)->getCrop('thumbnail')->url}\" /></p>";
            return implode('<br/>', $out);
          },
          'provider' => 'Esri.NatGeoWorldMap' // Other providers : Stamen.Toner, OpenTopoMap, OpenStreetMap.Mapnik, OpenStreetMap.HOT, Stamen.TonerLite
        );
        echo $map->render($page, 'map', $options); 
      ?>
    </section>
  </section>

<?php
  include("./foot.inc"); 
?>
