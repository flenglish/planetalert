<?php namespace ProcessWire;
  include("./head.inc"); 

  // Nav tabs
  $team = $selectedTeam;
  if ($user->isSuperuser()) {
    $headTeacher = $team->teacher->first();
  }
  include("./tabList.inc"); 

  $today = mktime(0,0,0, date("m"), date("d"), date("Y"));

  if ($user->hasRole('teacher') || $user->isSuperuser()) {
    $allCategories = $pages->find("parent.name=categories, name!=speaking|writing|revising|testing|listening|reading, template=category, adminOnly=1,sort=name");
    if ($user->isSuperuser() || ($user->name == 'flieutaud' && $team->name == 'no-team')) {
      // Limit to selected team headTeacher
      if ($team->name == 'no-team') {
        $allTasks = $pages->find("template=task, adminOnly=0");
        $allPlayers = getAllPlayers($user, true);
        $pagination = $allPlayers->renderPager();
      } else {
        $allTasks = $pages->find("parent.name=tasks, owner.singleTeacher=$headTeacher")->sort("category.name, HP, XP");
        $allPlayers = getAllPlayers($user, false);
      }
    } else { // Limit to logged in teacher
      $allTasks = $pages->find("parent.name=tasks, (owner.singleTeacher=$user), (name~=hq-attack)")->sort("category.name, HP, XP");
      $allPlayers->filter("team=$team, team.teacher=$user");
    }
  }

    if ($allTasks->count() > 0) {
      if (isset($pagination)) { echo $pagination;}
  ?>

    <ul class="list-inline text-center">
      <ul class="list-inline">
        <li><button class="btn btn-primary toggle-vis" data-category=""><?php echo __('All categories'); ?></button></li>
        <?php
          foreach ($allCategories as $cat) {
            echo '<li><button class="btn btn-primary toggle-vis" data-category="'.$cat->name.'">'.$cat->title.'</button></li>';
          }
        ?>
      </ul>
    </ul>

    <form id="adminTableForm" name="adminTableForm" action="<?php echo $pages->get('name=submitforms')->url; ?>" method="post" class="" role="form">

    <input type="hidden" name="adminTableSubmit" value="Save" />
    <input type="submit" name="adminTableSubmit" value="<?php echo __("Save"); ?>" class="btn btn-block btn-primary" disabled="disabled" />

    <table id="adminTable" class="adminTable">
      <thead>
      <tr class="dark">
        <th><?php echo __('Groups'); ?></th>
        <th style="min-width:80px"><?php echo __('Players'); ?></th>
        <?php
        $colIndex = 0;
        foreach ($allTasks as $task) { 
          $task = checkModTask($task, $headTeacher);
        ?>
        <th class="task" id="th_<?php echo $colIndex; ?>" data-category="<?php echo $task->category->name; ?>" data-order="<?php echo $task->name; ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo $task->summary; ?>" data-keepVisible="">
          <div class="vertical-text">
            <div class="vertical-text__inner">
              <?php if ($task->teacherTitle != '') { $task->title = $task->teacherTitle; }
                echo $task->title;
              ?>
            </div>
          </div>
        </th>
        <?php 
          $colIndex++;
        }
        ?>
      </tr>
      <tr>
        <td colspan="2"><?php echo __("Display comments"); ?></td>
        <?php 
        $colIndex = 0;
        foreach ($allTasks as $task) { ?>
        <td data-toggle="tooltip" title="<?php echo __("Display comments"); ?>">
        <input type="checkbox" id="cc_<?php echo $colIndex; ?>" class="commonComment" onclick="showComment(<?php echo $colIndex; ?>)" />
        <input style="display: none;" type="text" id="commonComment_<?php echo $colIndex; ?>" name="commonComment[<?php echo $colIndex; ?>]" value="" placeholder="<?php echo __("Common comment"); ?>" onKeyUp="setCommonComment(<?php echo $colIndex; ?>, $(this))" />
        </td>
        <?php 
          $colIndex++;
        }
        ?>
      </tr>
      <tr>
        <td colspan="2"><?php echo __("Select all"); ?></td>
        <?php
          $colIndex = 0;
          foreach ($allTasks as $task) {
        ?>
          <td data-toggle="tooltip" title="<?php echo __("Select all"); ?>"><input type="checkbox" id="csat_<?php echo $colIndex; ?>" class="selectAll" onclick="selectAll(<?php echo $colIndex; ?>)" /></td>
        <?php
          $colIndex++;
        } ?>
      </tr>
      </thead>
      <tbody>
      <?php
        foreach ($allPlayers as $player) { 
          $id = $player->id; 
          // See if absence already recorded (last event)
          $abs = $player->get("name=history")->children("date>$today")->get("task.name=abs|absent");
      ?>
      <tr class="<?php if ($abs) { $disabled = 'disabled'; echo 'negative'; } else { $disabled = ''; } ?>">
      <td class="nowrap transparent"><?php if (isset($player->group->id)) { echo $player->group->title; } ?></td>
      <td class="dark"><a class="negative" href="<?php echo $player->url; ?>" target="_blank"><?php echo $player->title; ?></a></td>
        <?php
          $colIndex = 0;
        foreach ($allTasks as $task) {
          if ($task->HP < 0) { $type = 'negative'; } else { $type=''; }
          $taskId = $task->id;
        ?>
        <td class="<?php echo $type; ?>" data-toggle="tooltip" title="<?php echo $player->title.' - '.$task->title; ?>">
          <input type="checkbox" <?php echo $disabled; ?> class="ctPlayer ct_<?php echo $colIndex; ?>" id="" data-customId="<?php echo $id.'_'.$taskId; ?>" name="player[<?php echo $id.'_'.$taskId; ?>]" onChange="onCheck(<?php echo $colIndex; ?>)" />
          <input style="display: none;" <?php echo $disabled; ?> type="text" data-customId="<?php echo $id.'_'.$taskId; ?>" class="cc_<?php echo $colIndex; ?>" name="comment_<?php echo $id.'_'.$taskId; ?>" value="" placeholder="<?php echo __("comment"); ?>" />
          <?php 
            if ($task->category->is("name=homework") && $task->HP < 0 && $player->hkcount > 0) { 
              echo '<small>'.$player->hkcount.'</small>';
            }
            if ($abs && $task->is("name=absent|abs")) { 
              echo "<a href='#' class='removeAbs' data-type='removeAbs' data-url='".$pages->get('name=submitforms')->url."?form=deleteForm&eventId=".$abs->id."'>[✗]</a>"; 
            } else {
              if ($disabled == 'disabled') { echo "<a href='#' class='toggleEnabled'>[◑]</a>"; } 
            }
          ?>
        </td>
        <?php
          $colIndex++;
        }
        ?>
      </tr>
      <?php } ?>
      </tbody>
    </table>
    <input type="submit" name="adminTableSubmit" value="<?php echo __("Save"); ?>" class="btn btn-block btn-primary" disabled="disabled" />
    <fieldset>
      <?php
        $today = date('Y-m-d');
        echo '<legend>';
        echo __("More options").' - ';
        echo '<span class="glyphicon glyphicon-warning-sign"></span> '.__("Applied to ALL selected players and tasks !");
        echo '</legend>';
        echo '<label for="customDate">'.__("Custom date").'</label>';
        echo '<input id="customDate" name="customDate" type="date" size="8" value="'.$today.'" />';
        echo '&nbsp;&nbsp;';
        echo '<select id="customRefPage" name="customRefPage">';
        echo '<option>';
        echo __('Select a refPage');
        echo '</option>';
        $allRefPages = $pages->get("name=headquarters")->children("template=megamonster, (exerciseOwner.singleTeacher=$headTeacher), (created_users_id=$headTeacher->id)")->sort("level");
        $allRefPages->add($pages->find("template=roadmap, team=$team"));
        /* $allRefPages = getAllMonsters($headTeacher); */
        $allRefPages->sort("title");
        foreach ($allRefPages as $p) { 
          echo '<option value="'.$p->id.'">';
          echo $p->title;
          echo '</option>';
        }
        echo '</select>';
        echo '&nbsp;&nbsp ;';
        echo '<input type="checkbox" id="stayOnPage" name="stayOnPage" data-url="'.$page->url.'" />';
        echo '&nbsp;';
        echo '<label for="stayOnPage">'.__("Stay on this page after saving").'</label>';
      ?>
    </fieldset>
    </form>

  <?php
      if (isset($pagination)) { echo $pagination;}
    } else { // No tasks
      echo '<p class="">'.__("You have no tasks set yet.").'</p>';
    }

  $pages->unCacheAll();

  include("./foot.inc"); 
?>
