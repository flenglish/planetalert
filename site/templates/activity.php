<?php namespace ProcessWire;
  if (!$config->ajax) {
    include("./head.inc"); 
  }

  $out = '';

  $playerId = $input->urlSegment2;
  if ($user->hasRole('player')) {
    $player = $pages->get("parent.name=players, template=player, login=$user->name");
  }
  if (isset($player) && $user->isLoggedin() && $playerId == $player->id || $user->isSuperuser() || $user->hasRole('teacher')) {
    $monster = $pages->get($input->urlSegment1);
    if ($playerId && $playerId != '') { // Teacher wants to see a player's fighting zone
      $player = $pages->get($playerId);
    }
    $allEvents = $player->find("template=event, refPage=$monster, name!=best-time-lost")->sort("date");
    $today = new \DateTime("today");
    $index = 0;
    $memorized = 0;
    $nbLines = count(preg_split('/$\r|\n/', wire('sanitizer')->entitiesMarkdown($monster->exData)));
    $colX = 0;
    $colY = 0;
    $points = '';
    $circles = [];
    // Calculate complete timespan and scale accordingly ?
    if ($allEvents->count() > 0) {
      $firstDate = new \DateTime(date("Y-m-d", $allEvents->first()->date()));
      $maxDaySpan = $today->diff($firstDate)->days;
      if ($maxDaySpan > 1095) {
        $labelSize = '2em';
        $size = 12;
      } else if ($maxDaySpan > 730) {
        $labelSize = '1.5em';
        $size = 8;
      } else {
        $labelSize = '0.9em';
        $size = 4;
      }
      foreach($allEvents as $e) {
        $date1 = new \DateTime(date("Y-m-d", $e->date));
        if ($index > 0) { // Not for first event
          $interval = $date1->diff($prevDate);
          if ($interval->days > 0) { // Forgetting curve depends on time interval and # of events
            $memorized = memorizedFromTime($memorized, $index, $nbLines, $interval->days);
            $colX += $interval->days;
            $colY = 200-($memorized*2);
            $points .= ' '.$colX.','.$colY;
          }
        }
        list($memorized, $result) = memorizedFromAction($memorized, $index, $nbLines, $e);
        $colY = 200-($memorized*2);
        $points .= ' '.$colX.','.$colY;
        $tooltip = date("d/m/y", $e->date);
        $tooltip .= ' ('.$e->title.$result.')';
        $tooltip2 = ' → '.sprintf(__('%s%% memorized'), $memorized);
        if ($e->task->is("name^=fight|trap")) {
          array_push($circles, '<circle class="circle" id="'.$e->id.'" cx="'.$colX.'" cy="'.$colY.'" r="'.$size.'" stroke="black" stroke-width="1" fill="grey" />');
        } else if ($e->task->is("name^=ut-action")){
          array_push($circles, '<circle class="circle" id="'.$e->id.'" cx="'.$colX.'" cy="'.$colY.'" r="'.$size.'" stroke="black" stroke-width="1" fill="black" />');
        } else {
          array_push($circles, '<circle class="circle" id="'.$e->id.'" cx="'.$colX.'" cy="'.$colY.'" r="'.$size.'" stroke="black" stroke-width="1" fill="lightgrey" />');
        }
        $textLabel = '<g id="label-'.$e->id.'" opacity="0">';
          $textLabel .= '<text x="20" y="150" font-size="'.$labelSize.'" fill="black" text-decoration="underline">'.$tooltip.'</text>';
          $textLabel .= '<text x="25" y="170" font-size="'.$labelSize.'" fill="black" text-decoration="underline">'.$tooltip2.'</text>';
        $textLabel .= '</g>';
        array_push($circles, $textLabel);
        $prevDate = $date1;
        $index++;
      }
      // Compare to today's date
      $interval = $today->diff($prevDate);
      if ($interval->days > 0) { // Forgetting curve depends on time interval and # of events
        $memorized = memorizedFromTime($memorized, $index, $nbLines, $interval->days);
      }
      $colX += $interval->days;
      $colY = 200-($memorized*2);
      $points .= ' '.$colX.','.$colY;
      $tooltip = date("d/m/y").' ';
      $tooltip .= sprintf(__('(Today\'s estimation) : %s%%'), $memorized);
      array_push($circles, '<circle class="circle" id="today" data-toggle="tooltip" title="'.$tooltip.'" cx="'.$colX.'" cy="'.$colY.'" r="'.($size-1).'" fill="white" stroke="black" />');
      $textLabel = '<g id="label-today" opacity="0">';
        $textLabel .= '<text x="20" y="150" font-size="'.$labelSize.'" fill="black" text-decoration="underline">'.$tooltip.'</text>';
      $textLabel .= '</g>';
      array_push($circles, $textLabel);

      if (!$config->ajax) {
        $out .= '<h2 class="text-center">';
          $out .= sprintf(__('Activity on %1$s by %2$s [%3$s]'), $monster->title, $player->title, $player->team->title);
          $out .= '<img class="absRight" src="'.$monster->image->getCrop("thumbnail")->url.'" alt="Photo" />';
        $out .= '</h2>';
      }
      $subtitle = '<img src="'.$monster->image->getCrop("small")->url.'" alt="photo" /> '.$monster->title.' → ';
      $subtitle .= __('Estimation').' : ';
      $subtitle .= '<span class="label label-primary">';
        $subtitle .= sprintf(__('%s%% memorized !'), $memorized);
      $subtitle .= '</span> ';
      $subtitle2 = '<h4 class="text-center">';
      $subtitle2 .= '<span class="label label-danger">'.sprintf(_n('%1$s event in %2$s days', '%1$s events in %2$s days', $allEvents->count(), $maxDaySpan), $allEvents->count(), $maxDaySpan).'</span>';
      $subtitle2 .= ' <span class="">'.sprintf(__("Last action : %d days ago."), $interval->days).'</span>';
      $subtitle2 .= '</h4>';

      $out .= '<section class="row text-center">';
        $out .= '<h1>';
          $out .= $subtitle;
        $out .= '</h1>';
        if ($maxDaySpan < 365) { $maxDaySpan = 365; }
        $out .= '<svg class="monsterActivity" viewbox="0 -5 '.($maxDaySpan+20).' 205">';
          $out .= '<line x1="0" y1="0" x2="'.$maxDaySpan.'" y2="0" stroke="grey" stroke-width="1" stroke-dasharray="1,1" />';
          $out .= '<line x1="0" y1="70" x2="'.$maxDaySpan.'" y2="70" stroke="red" stroke-width="2" stroke-dasharray="3,3" />';
          $out .= '<text x="2" y="85" fill="red" font-size="1em">';
            $out .= __("Recommended training level");
          $out .= '</text>';
          $out .= '<g class="x-grid">';
            $out .= '<line x1="0" y1="-5" x2="0" y2="205" stroke="black"></line>';
            $out .= '<text x="1" y="5" fill="grey" font-size="0.8em">100%</text>';
            $out .= '<text x="3" y="197" fill="grey" font-size="0.8em">0%</text>';
          $out .= '</g>';
          $out .= '<g class="y-grid">';
            $out .= '<line x1="0" y1="200" x2="'.$maxDaySpan.'" y2="200" stroke="black"></line>';
              $nbYears = round($maxDaySpan/365);
              for ($i=1; $i<=$nbYears; $i++) {
                $out .= '<text x="'.(($i*365)-15).'" y="193" fill="grey" font-size="0.8em">'.sprintf(_n('%syear', '%syears', $i), $i).'</text>';
                $out .= '<line x1="'.($i*365).'" y1="197" x2="'.($i*365).'" y2="203" stroke="black" strole-width="2"></line>';
              }
          $out .= '</g>';
          $out .= '<polyline points="'.$points.'" fill="none" stroke="#0074D9" stroke-width="2" />';
          foreach ($circles as $c) {
            $out .= $c;
          }
        $out .= '</svg>';
        $out .= $subtitle2;
      $out .= '</section>';
    } else {
      $out .= '<h3>'.__('You have no activity on this monster.').'</h3>';
    }

    echo $out;
  } else {
    echo $noAuthMessage;
  }

  if (!$config->ajax) {
    include("./foot.inc"); 
  }
?>
