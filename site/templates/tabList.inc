<?php namespace ProcessWire;

  $out = '';
  $team = $selectedTeam;
  $currentPeriod = $team->periods;

  $out .= '<ul class="tabList list-inline">';
  $team->freeActs == '' ? $team->freeActs = 0 : '';
  $team->freeworld == '' ? $team->freeworld = 0 : '';
  $team->yearlyKarma == '' ? $team->yearlyKarma = 0 : '';
  $out .= '<li class="label label-primary" data-toggle="tooltip" title="'.__("Team karma").' : '.$team->yearlyKarma.'">';
    $out .= '<span>'.$team->title.' : '.$team->freeActs;
    if ($team->skills->has("name=elite")) {
      $out .= ' <span class="glyphicon glyphicon-flag"></span> ';
      $nbHqVictories = $team->hqFights->find("result>=85")->count();
      $out .= sprintf(__('(HQ%d)'), $nbHqVictories);
    } else {
      $out .= ' ('.$team->freeworld.'%)</span>';
    }
  $out .= '</li>&nbsp;';
  $class = $page->is("template=main-office") ? "active" : "";
  $out .= '<li class="'.$class.'"><a href="'.$office->url.$team->name.'/teacher">'.$office->title.'</a></li>';
  $class = $page->is("name=players") ? "active" : "";
  $out .= '<li class="'.$class.'"><a href="'.$homepage->url.'players/'.$team->name.'">'.__("The team").'</a></li>';
  $class = $page->is("name=world") ? "active" : "";
  $out .= '<li class="'.$class.'"><a href="'.$homepage->url.'world/'.$team->name.'/completed">'.__("The world").'</a></li>';
  $class = $page->is("name=admintable") ? "active" : "";
  $out .= '<li class="'.$class.'"><a href="'.$homepage->url.'adminTable/'.$team->name.'">'.__("Admin table").'</a></li>';
  $anyRoadmap = $pages->find("parent.name=roadmaps,template=roadmap,team=$team");
  if ($anyRoadmap->count() > 0) {
    $class = $page->is("name=roadmaps") ? "active" : "";
    $out .= '<li class="'.$class.'"><a href="'.$homepage->url.'roadmaps/'.$team->name.'">'.__("The Roadmaps").'</a></li>';
  }
  $class = $page->is("name=shop") ? "active" : "";
  $out .= '<li class="'.$class.'"><a href="'.$shop->url.$team->name.'">'.$shop->title.'</a></li>';
  $class = $page->is("name=makedonation") ? "active" : "";
  $out .= '<li class="'.$class.'"><a href="'.$donation->url.$team->name.'">'.__("Donation").'</a></li>';
  $class = $page->is("name=quiz") ? "active" : "";
  $out .= '<li class="'.$class.'"><a href="'.$quiz->url.$team->name.'">'.$quiz->title.'</a></li>';
  $out .= '<li class="'.$class.'"><a href="'.$gallery->url.$team->name.'">'.$gallery->title.'</a></li>';

  if (($user->isSuperuser() || $user->name == 'flieutaud') && $team->name != 'no-team') { // TODO Depends on teacher 
    if ($currentPeriod != false) {
      $out .= '<li><a target="_blank" href="'.$pages->get("name=reports")->url.'participation/'.$team->id.'/'.$currentPeriod->id.'?reportSort=title">'.__("Current period participation").'</a></li>';
    }
    $out .= '<li><a target="_blank" href="'.$pages->get("name=headquarters")->url.$team->name.'">'.$pages->get("name=headquarters")->title.'</a></li>';
  }
  $out .= '</ul>';

  echo $out;
?>
