<?php

namespace ProcessWire;

include("./head.inc");

$places = $pages->find("template=place, map.lat!='', name!=places, sort=title");
$map = $modules->get('MarkupLeafletMap');
$totalPlacesCount = count($places);
?>
<div class="row">
  <div class="col-sm-12 text-center">
    <h4><?php echo $page->summary . ' ('.$totalPlacesCount.')'; ?></h4>
    <h4>
      See the map with numbers : <a href="'.$config->paths->assets.'paFiles/map/worldMap-numbers.png" target="_blank" data-toggle="tooltip" title="Write the corresponding numbers on your places in your copybook.">Low resolution (.png)</a> / <a href="'.$config->paths->assets.'paFiles/map/worldMap.svg" target="_blank" data-toggle="tooltip" title="Write the corresponding numbers on your places in your copybook.">High resolution (.svg)</a>
    </h4>
  </div>
  <div class="col-sm-12">
  <?php
    echo $map->getLeafletMapHeaderLines();
/* $options = array('height' => '600px', 'markerIcon' => 'flag', 'markerColour' => 'red'); */
$options = array(
  'markerIcon' => 'flag',
  'markerColour' => 'black',
  'popupFormatter' => function ($page) {
      if ($page->is("template=place")) {
          $out[] = '<strong>'.sprintf(__('In %1$s, in %2$s'), $page->city->title, $page->country->title).'</strong>';
      } else {
          if ($page->city->id) {
              $out[] = '<strong>'.sprintf(__('Born in %1$s, in %2$s (%3$s)'), $page->city->title, $page->country->title, $page->nationality).'</strong>';
          } else {
              $out[] = '<strong>'.sprintf(__('From %1$s (%2$s)'), $page->country->title, $page->nationality).'</strong>';
          }
      }
      $out[] = "<p class=\"text-center\"><img src=\"{$page->photo->eq(0)->getCrop('thumbnail')->url}\" /></p>";
      return implode('<br/>', $out);
  },
  'height' => '500px',
  'class' => 'mapBox',
  'provider' => 'Esri.NatGeoWorldMap'
);
echo $map->render($places, 'map', $options);
?>
  </div>
</div>

<?php
include("./foot.inc");
?>
