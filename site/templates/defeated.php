<?php namespace ProcessWire;
  include("./head.inc"); 

  if ((integer) $input->urlSegment1) {
    $playerId = $input->urlSegment1;
    $playerPage = $pages->get("id=$playerId");
    if ($input->get->design) {
      $playerPage->setAndSave('design', $input->get->design);
    } else {
      $playerPage->setAndSave('design', 1);
    }
    $bestWeapon = $playerPage->equipment->find("parent.name=weapons, sort=-xp")->first();
    $bestProtection = $playerPage->equipment->find("parent.name=protections, sort=-hp")->first();
    $tmpCache = $playerPage->children()->get("name=tmp");
    $medals = $tmpCache->tmpMonstersActivity->find('fightNb>=5, quality>0.3')->sort("-lastFightDate");
    if ($playerPage->is("design=3|4")) {
      $medals->sort("-monster.level, monster.name");
    } else {
      $medals->shuffle();
    }
    $classStyle = 'style'.$playerPage->design;

    $out = '<div class="row">';
      if ($user->hasRole("teacher") || $user->isSuperuser()) {
        $out .= ' <button id="makePrintable" class="btn btn-primary">Printable version <span class="glyphicon glyphicon-info-sign" data-toggle="tooltip" title="Click then use Firefox screenshot tool to capture page"></span></button>';
        $out .= ' <a class="btn btn-primary" href="'.$pages->get("name=defeated")->url.'?pages2pdf=1">Generate Fight Requests PDF (for copybook)</a>';
      }
      $out .= '<h2 class="text-center well banner">';
        $out .= sprintf(_n('%1$s medal for %2$s [%3$s] !', '%1$s medals for %2$s [%3$s] !', $medals->count(), $playerPage->title, $playerPage->team->title), $medals->count(), $playerPage->title, $playerPage->team->title);
        $out .= '<span class="pull-left avatarContainer">';
          if ($playerPage->avatar) {
            $out .= '<img class="avatar superpose" src="'.$playerPage->avatar->getCrop("thumbnail")->url.'" width="80" alt="'.$playerPage->title.'." />';
          } else {
            $out .= '<Avatar>';
          }
          if ($bestWeapon && $bestWeapon->image) {
            $out .= '<img class="weapon mini superpose" src="'.$bestWeapon->image->getCrop("mini")->url.'" alt="'.$bestWeapon->title.'." />';
          }
          if ($bestProtection && $bestProtection->image) {
            $out .= '<img class="protection mini superpose" src="'.$bestProtection->image->getCrop("mini")->url.'" alt="'.$bestProtection->title.'." />';
          }
        $out .= '</span>';
          if ($user->isSuperuser() || ($user->hasRole('teacher') && $headTeacher->name == $user->name) || ($user->isLoggedin() && $playerPage->login == $user->name)) {
            $out .= '<div class="styleMenu pull-right">';
            $out .= '<div class="btn-group">';
              $out .= '<button type="button" class="btn btn-default btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                $out .= '<span class="glyphicon glyphicon-cog"></span> <span class="caret"></span>';
              $out .= '</button>';
              $out .= '<ul class="dropdown-menu">';
                for ($i=1; $i<5; $i++) {
                  $out .= '<li>';
                  $out .= '<a href="'.$page->url.$input->urlSegment1.'?design='.$i.'" class="defeatedStyle">';
                  if ($playerPage->design == $i) {
                    $out .= '<span class="glyphicon glyphicon-ok"></span> ';
                  }
                  $out .= 'Design '.$i.'</a>';
                  $out .= '</li>';
                }
              $out .= '</ul>';
            $out .= '</div>';
            $buyPdf = $pages->get("name=buy-pdf");
            if ($playerPage->GC > abs($buyPdf->GC) && $medals->count() > 5) { $enabled = '';  } else { $enabled = 'disabled'; }
            $out .= '<button class="btn btn-primary btn-sm buyPdf" '.$enabled.' data-url="'.$pages->get('name=submitforms')->url.'?form=buyPdf" data-lessonId="'.$page->id.'" data-playerId="'.$playerPage->id.'">'.__("Buy PDF").'('.abs($buyPdf->GC).' GC) <span class="glyphicon glyphicon-info-sign" data-toggle="tooltip" title="At least 5 medals are required"></span></button>';
            $out .= '</div>';
          }
      $out .= '</h2>';
    $out .= '</div>';

    $out .= '<ul class="list list-inline defeated '.$classStyle.'">';
    foreach($medals as $p) {
      $out .= '<li>';
      $out .= '<div class="thumbnail">';
      $out .= '<img class="" data-toggle="tooltip" data-html="true" title="'.$p->monster->title.'<br />Level '.$p->monster->level.'<br />'.$p->monster->summary.'" src="'.$p->monster->image->getCrop('thumbnail')->url.'" alt="'.$p->monster->title.'." />';
      $out .= '<caption>';
      $out .= '<p class="myPanel level-'.$p->monster->level.'">';
      $out .= $p->monster->title;
      $out .= '<span class="detailled"><br />'.__('Level').' '.$p->monster->level.'</span>';
      $out .= '</p>';
      $out .= '</caption>';
      $out .= '</div>';
      $out .= '</li>';
    }
    $out .= '</ul>';
  } else {
    $out = $noAuthMessage;
  }


  echo $out;

  include("./foot.inc");
?>
