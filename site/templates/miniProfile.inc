<?php namespace ProcessWire;

  switch ($page->template->name) {
    case 'place': $miniProfileType = 'places'; break;
    case 'places': $miniProfileType = 'places'; break;
    case 'people': $miniProfileType = 'people'; break;
    case 'peoples': $miniProfileType = 'people'; break;
    case 'shop': $miniProfileType = 'equipment'; break;
    case 'shop_generator': $miniProfileType = 'equipment'; break;
    default : $miniProfileType = '';
  }

  if ($player->coma == 0) {
    $out .= '<p>';
      // Display scores
      $out .= '<span class="label label-success">'.__("Karma").' : '.$player->yearlyKarma.'</span>';
      $out .= ' <span class="label label-success">'.__("Reputation").' : '.$player->reputation.'</span>';
      $out .= ' <span class="label label-default" data-toggle="tooltip" title="'.__('Level').'"><span class="glyphicon glyphicon-signal"></span> '.$player->level.'</span>';
      $threshold = getLevelThreshold($player->level);
      $out .= ' <span class="label label-default" data-toggle="tooltip" title="XP"><img src="'.$config->urls->templates.'img/star.png" alt="star." /> '.$player->XP.'/'.$threshold.'</span>';
      $nbFreeEl = $player->places->count()+$player->people->count();
      $out .= ' <span class="label label-default" data-toggle="tooltip" title="'.__('Free Elements').'"><img src="'.$config->urls->templates.'img/globe.png" alt="globe." /> '.$nbFreeEl.'</span>';
      $out .= ' <span class="label label-default" data-toggle="tooltip" title="'.__('Equipment').'"><span class="glyphicon glyphicon-wrench"></span> '.$player->equipment->count().'</span>';
      $out .= '<span class="label label-default" data-toggle="tooltip" title="'.__('Underground Training').'">'.$player->underground_training.' '.__('UT').'</span>';
      $out .= ' <span class="label label-default" data-toggle="tooltip" title="'.__('Fighting Power').'">'.$player->fighting_power.' '.__('FP').'</span>';
      $out .= ' <span class="label label-default" data-toggle="tooltip" title="'.__('Health Points').'"><img src="'.$config->urls->templates.'img/heart.png" alt="heart." /> '.$player->HP.'</span>';
      $out .= ' <span id="playerGC" class="label label-default" data-toggle="tooltip" title="'.__('Gold Coins').'"><img src="'.$config->urls->templates.'img/gold_mini.png" alt="gold coins." /> '.$player->GC.'</span>';
    $out .= '</p>';

    // Display according to $miniProfileType
    switch ($miniProfileType) {
      case 'equipment' :
        if ($player->equipment->count() > 0) {
          $out .= '<ul class="list-unstyled list-inline">';
          $out .= __("Your equipment").' ('.$player->equipment->count().') → ';
          foreach($player->equipment as $p) {
            $out .= '<li><a href="'.$p->url.'"><img class="" src="'.$p->image->getCrop('small')->url.'" data-toggle="tooltip" title="'.$p->title.'" /></a></li>';
          }
          $out .= '</ul>';
        } else {
          $out .= '<p>'.__("You don't have any equipment for the moment.").'</p>';
        }
        if ($player->usabledItems->count() > 0) {
          $out .= '<ul class="list-unstyled list-inline">';
          $out .= __("Items waiting to be used ").' ('.$player->equipment->count().') → ';
          foreach($player->usabledItems as $p) {
              if ($p->is("name^=transformation")) {
                $out .= '<li data-toggle="tooltip" title="'.$p->title.' : '.__("Click to create a new avatar").'"><a target="_blank" href="'.$pages->get("name=avatar")->url.'"><img src="'.$p->image->getCrop("small")->url.'" alt="'.$p->title.'." /></a></li>';
              } else {
                $out .= '<li data-toggle="tooltip" title="'.$p->title.'"><img src="'.$p->image->getCrop("small")->url.'" alt="'.$p->title.'." /></li>';
              }
          }
          $out .= '</ul>';
        }
        break;
      case 'places' :
        if ($player->places->count() > 0) {
          $out .= '<ul class="list-unstyled list-inline">';
          $out .= __("Your places").' ('.$player->places->count().') → ';
          foreach($player->places as $p) {
            $out .= '<li><a href="'.$p->url.'"><img class="" src="'.$p->photo->eq(0)->getCrop('mini')->url.'" data-toggle="tooltip" title="'.$p->title.'" /></a></li>';
          }
          $out .= '</ul>';
        } else {
          $out .= '<p>'.__("You don't have any places for the moment.").'</p>';
        }
        break;
      case 'people' :
        if ($player->people->count() > 0) {
          $out .= '<ul class="list-unstyled list-inline">';
          $out .= __("Your people").' ('.$player->people->count().') → ';
          foreach($player->people as $p) {
            $out .= '<li><a href="'.$p->url.'"><img class="" src="'.$p->photo->eq(0)->getCrop('mini')->url.'" data-toggle="tooltip" title="'.$p->title.'" /></a></li>';
          }
          $out .= '</ul>';
        } else {
          $out .= '<p>'.__("You don't have any people for the moment.").'</p>';
        }
        break;
      default: break;
    }
  } else {
    $out .= '<p class="label label-danger">Coma !</p>';
  }
?>
