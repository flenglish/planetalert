<?php

namespace ProcessWire;

include("./head.inc");

$out = '';

$ajaxContentUrl = $pages->get("name=ajax-content")->url;
$out .= '<div id="showInfo" data-href="'.$ajaxContentUrl.'"></div>';

$playerId = $input->urlSegment1;
$playerPage = $pages->get("id=$playerId");
$footprint = $config->paths->assets."paFiles/img/footprint.png";

if (isset($player) && $user->isLoggedin() && $user->name == $playerPage->name || $user->isSuperuser() || $user->hasRole('teacher')) {
    $allMonsters = getAllMonsters($playerPage);
    $allMonsters->sort("level,name");
    // Get all fight requests during current school year
    $allFightRequests = $playerPage->get("name=history")->find("template=event, task.name~=fight, inClass=1")->sort("date");
    $allSuccessfulFightRequests = $allFightRequests->find("task.name=fight-vv");
    $out .= '<section class="lead">';
    $out .= '<h3 class="text-center">';
    /* $out .= sprintf(__('Fight requests of %1$s [%2$s] (%3$s successful out of %4$s request⋅s)'), $playerPage->title, $playerPage->team->title, count($allSuccessfulFightRequests), count($allFightRequests)); */
    $out .= sprintf(__('%3$s successful fight request⋅s for %1$s [%2$s]'), $playerPage->title, $playerPage->team->title, count($allSuccessfulFightRequests));
    $help = __("Click on monsters for details. Blinking elements are advice from your activity. You should be able to fight them with just a little revision.");
    $out .= '<span class="pull-right glyphicon glyphicon-question-sign" data-toggle="tooltip" data-html="true" title="'.$help.'"></span>';
    $out .= '</h3>';
    $out .= '</section>';
    $level = 0;
    $out .= '<ul class="list-inline">';
    foreach ($allMonsters as $m) {
        if ($m->level != $level) {
            $out .= '<br /><p class="label label-primary">'.sprintf(__('Level %s'), $m->level).'</p><br />';
            $level = $m->level;
        }
        $out .= '<li class="panel">';
        $out .= '<p class="panel-title">';
        $out .= $m->title;
        $out .= '</p>';
        $out .= '<div class="panel-body text-center">';
        if ($allSuccessfulFightRequests->get("refPage=$m")) {
            $out .= '<img src="'.$m->image->getCrop("small")->url.'" alt="no-img" />';
        } else {
            if (isRequestable($playerPage, $m)) {
                $class = 'blink';
            } else {
                $class = '';
            }
            $out .= '<img class="monsterInfo '.$class.'" data-href="'.$m->url.'?playerId='.$playerId.'" src="'.$footprint.'" width="50" height="50" />';
        }
        $out .= '</div>';
        $out .= '</li>';
    }
    $out .= '</ul>';
} else {
    $out .= $noAuthMessage;
}

echo $out;

include("./foot.inc");
?>

