<?php namespace ProcessWire;
  if ($selectedTeam && $selectedTeam != '-1') {
    $submitform = $pages->get("name=submitforms");
    $out .= '<h4 class="text-center">';
    $out .= sprintf(__('Team options for %1$s'), $selectedTeam->title);
    $out .= ' <a href="'.$config->urls->admin.'page/edit/?id='.$selectedTeam->id.'" target="blank">'.__("[Edit]").'</a>';
    $out .= '</h4>';
    $out .= '<ul>';
      // Team Official Period
      if ($user->hasRole('teacher')) {
        $allPeriods = $pages->find("template=period, periodOwner.singleTeacher=$user")->sort("title");
      } else {
        $allPeriods = $pages->find("template=period");
      }
      if ($selectedTeam->periods != false) {
        $officialPeriod = $selectedTeam->periods;
      } else {
        $officialPeriod = false;
      }
      $out .= '<li>';
        $out .=   '<label for="periodId">'.__("Official period").' :&nbsp;</label>';
        $out .=   '<select id="periodId">';
        $out .=     '<option value="-1">'.__("No official period (holidays ?)").'</option>';
        foreach($allPeriods as $p) {
          if ($officialPeriod != false) {
            if ($p->id == $officialPeriod->id) {
              $status = 'selected="selected"';
            } else {
              $status = '';
            }
          } else {
            $status = '';
          }
          $dateStart = date("d/m/Y", $p->dateStart);
          $dateEnd = date("d/m/Y", $p->dateEnd);
          $out .=   '<option value="'.$p->id.'" '.$status.'>'.$p->title.' ('.$dateStart.' → '.$dateEnd.')</option>';
        }
        $out .= ' </select>';
        $out .= '&nbsp;';
        $out .= '<button class="confirm btn btn-primary" data-href="'.$page->url.'savePeriod/'.$selectedTeam.'" disabled="disabled">'.__("Save").'</button>';
      $out .= '</li>';
      // Today's challenge
      if ($selectedTeam->name != 'no-team') {
        if ($user->hasRole('teacher')) {
          $teacherPage = $pages->get("parent.name=teachers, template=teacherProfile, name=$user->name");
        } else { // Superuser
          $teacherPage = $pages->get("parent.name=teachers, template=teacherProfile, name=flieutaud");
        }
        $teamPage = $teacherPage->teamChallenges->get("team=$selectedTeam");
        $teacherPage->of(false);
        if (!$teamPage) { // Team page isn't ready let's create it
          $teamPage = new Page();
          $teamPage->of(false);
          $teamPage = $teacherPage->teamChallenges->getNew();
          $teamPage->team = $selectedTeam;
          $teamPage->save();
          $teacherPage->teamChallenges->add($teamPage);
          $teacherPage->save();
        }
        // Build monsters' list
        if ($user->isSuperuser()) {
          $allMonsters = $pages->get("name=monsters")->children()->sort("name");
        } 
        if ($user->hasRole('teacher')) {
          $allMonsters = $pages->get("name=monsters")->children("(created_users_id=$user->id, exerciseOwner.publish=1), (exerciseOwner.singleTeacher=$user, exerciseOwner.publish=1)")->sort("level, name");
        }
        $out .= '<li>';
          $out .= __("Today's challenges").' : ';
          $out .= $teamPage->feel(array("fields" => "linkedMonsters"));
          if ($teamPage->linkedMonsters->count() > 0) {
            $out .= ' '.$teamPage->linkedMonsters->implode(', ', '{title} (lvl {level})');
          } else {
            $out .= '<span class="label label-danger">'.__("No challenges are set.").'</span>';
          }
          // TODO : Better possibility : click on the list to edit and list updates on save, but doesn't work (splut modal that don't want to close after saving)
          /* $out .= '<div edit="'.$teamPage->id.'.linkedMonsters">'; */
          /*   foreach($teamPage->linkedMonsters as $m) { */
          /*     $out .= $m->title.' (lvl '.$m->level.') '; */
          /*   } */
          /* $out .= '</div>'; */
          $out .= '<ul class="list list-unstyled">';
            $out .= '<li>';
              $out .=   '<label for="duplicateChallenges">'.__("Duplicate challenges").' :&nbsp;</label>';
              $out .=   '<select id="duplicateChallenges">';
              $out .=     '<option value="-1">'.__("No selection").'</option>';
              if ($user->hasRole('teacher')) {
                $allTeams = $pages->find("template=team, teacher=$user")->sort("title");
                $allPlayers = $pages->find("parent.name=players, template=player, team.teacher=$user");
              } else {
                $allTeams = $pages->find("template=team")->sort("title");
                $allPlayers = $pages->find("parent.name=players, template=player");
              }
              foreach($allTeams as $p) {
                if ($p->name != 'no-team') {
                  $out .= '<option value="'.$p->id.'">'.$p->title.'</option>';
                }
              }
              $out .= '</select>';
              $out .= '<button class="confirm btn btn-primary" data-original-href="'.$page->url.'duplicateChallenges/'.$selectedTeam.'/" data-href="" disabled="disabled">'.__("Save").'</button>';
              $out .= ' <span class="reloadRequired label label-danger blink hidden"><span class="glyphicon glyphicon-warning"></span> '.__("Reload required").'</span>';
            $out .= '</li>';
          $out .= '</ul>';
        $out .= '</li>';
      } else {
        $out .= '<li>'.__("No challenges for no team players.").'</li>';
      }
      // Force Memory Helmet
      $lock = $pages->get("$selectedTeam")->forceHelmet;
      $lock == 1 ? $status = 'checked="checked"' : $status = '';
      $out .= '<li>';
        $out .= '<label for="forceHelmet"><input type="checkbox" id="forceHelmet" data-href="'.$submitform->url.'?form=forceOption&pageId='.$selectedTeam.'&optionName=forceHelmet" class="simpleAjax" data-hide-feedback="true" '.$status.'> '.__("Force Memory Helmet").'</label>';
        $unEquippedPlayers = $pages->find("parent.name=players, team=$selectedTeam")->not("equipment.name=memory-helmet");
        if ($unEquippedPlayers->count() > 0) {
          $unEquippedPlayersList = $unEquippedPlayers->implode(', ', '{title}');
          $out .= ' ['.__('Not present for : ').$unEquippedPlayersList.']';
        } else {
          $out .= ' ['.__('Everybody is equipped').']';
        }
      $out .= '</li>';
      // Force Visualizer
      $lock = $pages->get("$selectedTeam")->forceVisualizer;
      $lock == 1 ? $status = 'checked="checked"' : $status = '';
      $out .= '<li>';
        $out .= '<label for="forceVisualizer"><input type="checkbox" id="forceVisualizer" data-href="'.$submitform->url.'?form=forceOption&pageId='.$selectedTeam.'&optionName=forceVisualizer" class="simpleAjax" data-hide-feedback="true" '.$status.'> '.__("Force Visualizer").'</label>';
        $unEquippedPlayers = $pages->find("parent.name=players, team=$selectedTeam")->not("equipment.name=visualizer");
        if ($unEquippedPlayers->count() > 0) {
          $unEquippedPlayersList = $unEquippedPlayers->implode(', ', '{title}');
          $out .= ' ['.__('Not present for : ').$unEquippedPlayersList.']';
        } else {
          $out .= ' ['.__('Everybody is equipped').']';
        }
      $out .= '</li>';
      // Force Avatar (transformation Potion)
      $lock = $pages->get("$selectedTeam")->forceAvatar;
      $lock == 1 ? $status = 'checked="checked"' : $status = '';
      $out .= '<li>';
        $out .= '<label for="forceAvatar"><input type="checkbox" id="forceAvatar" data-href="'.$submitform->url.'?form=forceOption&pageId='.$selectedTeam.'&optionName=forceAvatar" class="simpleAjax" data-hide-feedback="true" '.$status.'> '.__("Force Avatar (Free Transformation Potion)").'</label>';
      $out .= '</li>';
      // Force Book of Knowledge
      $lock = $pages->get("$selectedTeam")->forceKnowledge;
      $lock == 1 ? $status = 'checked="checked"' : $status = '';
      $out .= '<li>';
        $out .= '<label for="forceKnowledge"><input type="checkbox" id="forceKnowledge" data-href="'.$submitform->url.'?form=forceOption&pageId='.$selectedTeam.'&optionName=forceKnowledge" class="simpleAjax" data-hide-feedback="true" '.$status.'> '.__("Force the Book of Knowledge").'</label>';
        $unEquippedPlayers = $pages->find("parent.name=players, team=$selectedTeam")->not("equipment.name=book-knowledge");
        if ($unEquippedPlayers->count() > 0) {
          $unEquippedPlayersList = $unEquippedPlayers->implode(', ', '{title}');
          $out .= ' ['.__('Not present for : ').$unEquippedPlayersList.']';
        } else {
          $out .= ' ['.__('Everybody is equipped').']';
        }

      $out .= '</li>';
      // Lock Fights (Training but no 'tests')
      $lock = $pages->get("$selectedTeam")->lockFights;
      $lock == 1 ? $status = 'checked="checked"' : $status = '';
      $out .= '<li><label for="lockFights"><input type="checkbox" id="lockFights" data-href="'.$submitform->url.'?form=forceOption&pageId='.$selectedTeam.'&optionName=lockFights" class="simpleAjax" data-hide-feedback="true" '.$status.'> '.__("Lock fights").'</label></li>';
      // 'In Class' tag (ignored for 'motivation' statistic)
      $lock = $pages->get("$selectedTeam")->classActivity;
      $lock == 1 ? $status = 'checked="checked"' : $status = '';
      $out .= '<li><label for="classActivity"><input type="checkbox" id="classActivity" data-href="'.$submitform->url.'?form=forceOption&pageId='.$selectedTeam.'&optionName=classActivity" class="simpleAjax" data-hide-feedback="true" '.$status.'> '.__("'In class' tag").'</label></li>';
      // Reset streak
      $out .= '<li><label for="reset-streaks"><input type="checkbox" id="reset-streaks"> '.__("Recalculate streaks").'</label> ';
      $out .= '<button class="confirm btn btn-primary" data-href="'.$page->url.'reset-streaks/'.$selectedTeam.'/1">'.__("Save").'</button>';
      $out .= '</li>';
      // Recalculate tmp page
      $out .= '<li><label for="recalculate-tmp"><input type="checkbox" id="recalculate-tmp"> '.__("Recalculate tmpPage").'</label> ';
      $out .= '<button class="confirm btn btn-primary" data-href="'.$page->url.'recalculate-tmp/'.$selectedTeam.'/1">'.__("Save").'</button>';
      $out .= '</li>';
      // Clean MarkupCache
      $out .= '<li><label for="cleanMarkupCache"><input type="checkbox" id="cleanMarkupCache"> '.__("Clean markupCache").'</label> ';
      $out .= '<button class="confirm btn btn-primary" data-href="'.$page->url.'cleanMarkupCache/'.$selectedTeam.'/1">'.__("Save").'</button>';
      $out .= '</li>';
      // Set Team Rate
      $out .= '<li><span>'.__("Set team  rates").'</span> : ';
        $out .= '<label for="teamRatePlace">'.__("Place").' : </label> <input type="number" class="set-teamRate" id="teamRatePlace" size="4" value="'.$selectedTeam->teamRatePlace.'">';
        if ($selectedTeam->rank->is("index>=8")) {
          $out .= ' <label for="teamRatePeople">'.__("People").' : </label> <input type="number" class="set-teamRate" id="teamRatePeople" size="4" value="'.$selectedTeam->teamRatePeople.'">';
        }
        $out .= ' <button id="simulateScores" class="simpleAjax btn btn-primary" data-original-href="'.$page->url.'simulateScores/'.$selectedTeam->id.'" data-href="'.$page->url.'simulateScores/'.$selectedTeam->id.'" data-targetId="embedViewport" data-hide-feedback="true">'.__("Simulate scores").'</button>';
        $out .= '  [';
        $out .= sprintf(__('%1$s players in the team'), $allPlayers->find("team=$selectedTeam")->count());
        $out .= ', ';
        $out .= sprintf(__('15%% = %1$s players'), round(($allPlayers->find("team=$selectedTeam")->count())*15/100));
        $out .= ', ';
        $out .= sprintf(__('complete world = %1$s elements'), teamPossibleElements($selectedTeam)->count());
        $out .= '] ';
        $out .= sprintf(__('[Current score : %1$s%%]'), $selectedTeam->freeworld);
        if ($selectedTeam->rank->is("index>=8")) {
          $out .= ' <button class="confirm btn btn-primary" id="saveRates" data-original-href="'.$page->url.'setTeamRate/'.$selectedTeam.'/'.$selectedTeam->teamRatePlace.'-'.$selectedTeam->teamRatePeople.'" data-href="">'.__("Save").'</button> ';
        } else {
          $out .= ' <button class="confirm btn btn-primary" id="saveRates" data-original-href="'.$page->url.'setTeamRate/'.$selectedTeam.'/'.$selectedTeam->teamRatePlace.'" data-href="">'.__("Save").'</button> ';

        }
        $out .= '<section id="embedViewport" class="well"></section>';
      $out .= '</li>';
      if ($user->isSuperuser()) {
        // Archive team
        $out .= '<li><label for="archiveTeam"><input type="checkbox" id="archiveTeam"> '.__("Archive").'</label> ';
        $out .= '<button class="confirm btn btn-primary" data-href="'.$page->url.'archive/'.$selectedTeam.'/1">'.__("Save").'</button>';
        $out .= '</li>';
      };
    $out .= '</ul>';
  } else {
    $out .= '<p>'.__("You need to select a team for more options.").'</p>';
  }
?>
