<?php namespace ProcessWire;
  include("./head.inc"); 
  
if ($user->hasRole('teacher') || $user->isSuperuser()) {

  if ($user->is("name=flieutaud") || $user->isSuperuser()) { 
    $noTeamPlayers = $pages->find("parent.name=players, team.name=no-team");
    $allPlayers->add($noTeamPlayers);
    $testTeamPlayers = $pages->find("parent.name=players, team.name=test-team");
    $allPlayers->add($testTeamPlayers);
  }
  $submitforms = $pages->get("name=submitforms");
  $padsToShow = $input->urlSegment1;
  $teamName = $input->urlSegment2;
  // Preparing PW find selectors
  if ($user->isSuperuser()) {
    $padsCallRequest = "parent.name=players, pads.alertBox=1";
    $nonEmptyPadsRequest = "parent.name=players, pads.pad!=''";
    $editedPadsRequest = "parent.name=players, pads.editedBox=1";
  } else {
    $headTeacherName = $user->name;
    $padsCallRequest = "parent.name=players, team.teacher=$headTeacherName, pads.alertBox=1";
    $nonEmptyPadsRequest = "parent.name=players, team.teacher=$headTeacherName, pads.pad!=''";
    $editedPadsRequest = "parent.name=players, team.teacher=$headTeacherName, pads.editedBox=1";
  }

  $padsCallsNb = count($pages->findIds($padsCallRequest));
  $nonEmptyPadsNb = count($pages->findIds($nonEmptyPadsRequest));
  $editedPadsNb = count($pages->findIds($editedPadsRequest));

  $out = '';
  $ajaxContentUrl = $pages->get("name=ajax-content")->url;
  $out .= '<div id="showInfo" data-href="'.$ajaxContentUrl.'"></div>';

  $out .= '<p><a href="'.$page->url.'"><span class="glyphicon glyphicon-refresh"></span> '.__("Reload page after editing pads to update the counters.").'</a></p>';
  $out .= '<div class="btn-group pull-right">';
    $out .= '<a href="'.$page->url.'?sort=date" class="btn btn-xs btn-default">'.__("Sort by date").'</a>';
    $out .= '<a href="'.$page->url.'?sort=team" class="btn btn-xs btn-default">'.__("Sort by team").'</a>';
    $out .= '<a href="'.$page->url.'?sort=name" class="btn btn-xs btn-default">'.__("Sort by name").'</a>';
  $out .= '</div>';

  $out .= '<ul class="list list-unstyled list-inline">';
  if ($padsToShow == '' || $padsToShow == 'padsCalls') {
    $class = 'btn btn-success btn-sm';
    if ($input->get('sort') && $input->get('sort') == 'team') {
      $allConcerned = $pages->find($padsCallRequest)->sort('team.title, title');
    } else if ($input->get('sort') && $input->get('sort') == 'name') {
      $allConcerned = $pages->find($padsCallRequest)->sort('title');
    } else {
      $allConcerned = $pages->find($padsCallRequest)->sort('-pads.modified, team.title, title');
      bd(t('02'));
    }
  } else { $class = 'btn btn-danger btn-sm'; }
  $out .= '<li><a class="'.$class.'" href="'.$page->url.'padsCalls'.'">'.__('Pads calls').' ['.$padsCallsNb.']</a></li>';

  if ($padsToShow == 'editedPads') {
    $class = 'btn btn-success btn-sm';
    $allConcerned = $pages->find($editedPadsRequest)->sort('title');
  } else { $class = 'btn btn-danger btn-sm'; }
  $out .= '<li><a class="'.$class.'" href="'.$page->url.'editedPads'.'">'.__('Edited pads').' ['.$editedPadsNb.']</a></li>';

  if ($padsToShow == 'hasContent') {
    $class = 'btn btn-success btn-sm';
    if (!$teamName) {
      $allConcerned = $pages->find($nonEmptyPadsRequest)->sort('title');
    } else {
      $allConcerned = $pages->find($nonEmptyPadsRequest.", team.name=$teamName")->sort('title');
    }
  } else { $class = 'btn btn-danger btn-sm'; }
  if (!$teamName) {
    $out .= '<li><a class="'.$class.'" href="'.$page->url.'hasContent'.'">'.__('Non empty pads').' ['.$nonEmptyPadsNb.']</a></li>';
  } else {
    $out .= '<li><a class="'.$class.'" href="'.$page->url.'hasContent'.'">'.sprintf(__('Non empty pads for %s'), strtoupper($teamName)).'</a></li>';
  }

  if ($padsToShow == 'allPads') {
    $class = 'btn btn-success btn-sm';
  } else { $class = 'btn btn-danger btn-sm'; }
  $out .= '<li><a class="'.$class.'" href="'.$page->url.'allPads'.'">'.__('All pads').' ['.$allPlayers->count().']</a></li>';
  $out .= '</ul>';

  switch ($padsToShow) {
    case 'allPads' :
      $out .= '<ul class="list list-unstyled col4">';
      $allPlayers->sort('title');
      foreach ($allPlayers as $p) {
        $editLink = $p->feel(array(
          'mode' => 'page-edit',
          'fields' => 'pads',
        ));
        $out .= '<li>';
          $out .= '<a href="'.$p->url.'" target="_blank">'.$p->title.' ['.$p->team->title.']</a> ';
          if ($p->pad != '') { $out .= '*'; }
          $out .= $editLink;
          $out .= ' <a href="#" class="ajaxBtn" data-type="quickValidation" data-playerId="'.$p->id.'">['.__("Quick validation").'] </a>';
        $out .= '</li>';
      }
      $out .= '</ul>';
      break;
    default :
      foreach ($allConcerned as $p) {
        foreach ($p->pads as $pad) {
          $editPadLink = $pad->feel(array(
            'mode' => 'page-edit',
            'fields' => 'pad',
            'data-ajax-target' => '#myPad-'.$pad->id
          ));
          if ($pad == '') { $pad = '<br /><br /><br />'; }
          $out .= '<section class="frame" id="pad-'.$p->id.'">';
            $out .= '<button class="close pull-right" data-id="#pad-'.$p->id.'"><span class="glyphicon glyphicon-remove"></span></button>';
            $out .= '<a href="#" class="btn btn-primary btn-sm toggleNext">'.$p->title. '['.$p->team->title.'] <span class="glyphicon glyphicon-eye-open"></span></a>';
            $out .= ' <a href="'.$p->url.'" target="_blank">['.__('Profile page').']</a> ';
            $out .= $editPadLink;
            $out .= '<label for="editedBox_'.$pad->id.'" class="btn btn-sm">';
              $pad->editedBox == 1 ? $status = 'checked="checked"' : $status = '';
              $out .= '<input type="checkbox" id="editedBox_'.$pad->id.'" name="editedBox_'.$pad->id.'" data-href="'.$submitforms->url.'?form=forceOption&pageId='.$pad->id.'&optionName=editedBox" class="simpleAjax" data-hide-feedback="true" '.$status.' /> ';
              $out .= __("Edited");
            $out .= '</label> ';
            $out .= '<label for="alertBox_'.$pad->id.'" class="btn btn-sm">';
              $pad->alertBox == 1 ? $status = 'checked="checked"' : $status = '';
              $out .= '<input type="checkbox" id="alertBox_'.$pad->id.'" name="alertBox_'.$pad->id.'" data-href="'.$submitforms->url.'?form=forceOption&pageId='.$pad->id.'&optionName=alertBox" class="simpleAjax" data-hide-feedback="true" '.$status.' /> ';
              $out .= __("Published on Newsboard");
            $out .= '</label> ';
            $out .= ' <a href="#" class="btn btn-danger btn-sm ajaxBtn" data-type="quickValidation" data-playerId="'.$p->id.'">['.__("Quick validation").']</a>';
            $lastModDate = date("D d M Y, H:i:s", $pad->modified);
            $out .= ' <span>'.sprintf(__("Last modified on %s"), $lastModDate).'</span>';
            $out .= '<div id="myPad-'.$pad->id.'" class="pad hidden">';
              if ($pad->pad == '') {$pad->pad = '<br /><br />'; }
              $out .= $pad->edit('pad');
            $out .= '</div>';
          $out .= '</section>';
        }
      }
  }

  echo $out;
} else {
  echo $noAuthMessage;
}
  
include("./foot.inc");
?>
