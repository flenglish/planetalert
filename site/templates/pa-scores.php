<?php

namespace ProcessWire;

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>All scores</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="generator" content="ProcessWire <?php echo $config->version; ?>" />
  <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates; ?>node_modules/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/main.css" />
</head>

  <body>
<div class="row text-center">
<?php echo getScoresSummaries($page->teacher->first()); ?>
</div>
</body>
</html>
