<?php namespace ProcessWire;

    include("./head.inc"); 

    $out = '';

    if ($input->urlSegment1) {
      $selectedTeamName = $input->urlSegment1;
      $allPlayers = $pages->find("parent.name=players, team.name=$selectedTeamName")->shuffle();
      $noAvatar = '';
      $out .= '<section class="row">';
        $out .= '<h3 class="well text-center">';
          $out .= sprintf(__('Welcome to %s gallery'), $allPlayers->eq(0)->team->title);
        $out .= '</h3>';

        $out .= '<ul class="list list-unstyled list-inline">';
        foreach ($allPlayers as $p) {
          if ($p->avatar) {
            $out .= '<li>';
              $out .= '<img class="img-thumbnail" data-toggle="tooltip" width="130" data-html="true" title="'.$p->title.'<br />'.__("Level").' '.$p->level.'" src="'.$p->avatar->url.'" alt="'.$p->title.'" />';
            $out .= '</li>';
          } else {
            $noAvatar .= $p->title.' ';
          }
        }
        $out .= '</ul>';
      $out .= '</section>';
      $out .= '<hr />';
      $out .= '<section class="row">';
        $out .= '<p>'.__("Players with no avatar").' : '.$noAvatar.'</p>';
      $out .= '</section>';
      echo $out;
    } else {
      echo $noAuthMessage;
    }

  include("./foot.inc");
?>
