<?php namespace ProcessWire; /* tmp template */
  include("./head.inc"); 

  if ($user->isSuperuser() || $user->hasRole('teacher')) {
    $tmpPages = $page->tmpMonstersActivity;
    $tmpPages->sort("monster.name");

    $recalculate = $input->get("recalculate");
    $confirm = $input->get("confirm");
    $init = $input->get("init");
    $out = '';

    $player = $page->parent();
    $out .= '<div class="row text-center">';
      $out .= '<h3>tmpCache for <a href="'.$player->url.'">'.$player->title.'</a></h3>';
      // Action buttons
      $out .= '<p>';
      $out .= '<a class="btn btn-danger" href="'.$page->url.'?recalculate=1&confirm=1&init=1">Initialize</a>';
      $nbCached = count($page->tmpMonstersActivity->children());
      if ($nbCached > 0) {
        $out .= '<a class="btn btn-info" href="'.$page->url.'?recalculate=1&confirm=0">'.__('Check').'</a>';
      }
      $out .= '</p>';
      $out .= '<p>';
      if ($recalculate) {
        if ($confirm) {
          if ($init) { initTmpMonstersActivity($player); }
          $out .= '<p>';
          $out .= 'New values have been saved.';
          $out .= ' <a class="btn btn-success" href="'.$page->url.'">Reload tmp page</a>';
          $out .= '</p>';
        } else {
          $out .= '<a class="btn btn-primary" href="'.$page->url.'?recalculate=1&confirm=1">Save new values ?</a>';
        }
      }
      $out .= '</p>';

      if ($nbCached > 0) {
        $out .= '<p>'.sprintf(__('Never trained on %d monsters'), $page->index).' ';
      } else {
        $out .= '<p>'.__("No tmp activity. You should initialize tmp page.").'</p>';
      }
      if ($recalculate) {
        $headTeacher = getHeadTeacher($player);
        $allMonsters = $pages->get("name=monsters")->children("(created_users_id=$headTeacher->id, publish=1), (exerciseOwner.singleTeacher=$headTeacher, exerciseOwner.publish=1)");
        $neverTrained = 0;
        foreach ($allMonsters as $m) {
          if ($player->find("task.name^=ut-action, refPage=$m")->count() == 0) {
            $neverTrained++;
          }
        }
        if ($neverTrained != $page->index) {
          $out .= '<span class="label label-danger">⇒ '.$neverTrained.' / '.$allMonsters->count().'</span>';
          if ($neverTrained < 0) {
            $out .= '<p class="label label-danger"><i class="glyphicon glyphicon-warning-sign"></i> Error detected ! Page should be initialized !</p>';
          }
          $page->index = $neverTrained;
        } else {
          $out .= '<span class="label label-success">OK</span>';
        }
        $absentMonstersNb = (($allMonsters->count()-$neverTrained)-$page->tmpMonstersActivity->count());
        $out .= '<p>';
        if ($absentMonstersNb > 0) {
          $out .= sprintf(_n('%d monster is missing.', '%d monsters are missing.', $absentMonstersNb), $absentMonstersNb);
          $out .= ' '.__("You should initialize tmpPage");
          $out .= ' <span class="label label-danger">⇒ Not OK</span></span>';
        } else {
          $out .= __('All monsters are present.').' <span class="label label-success">OK</span>';
        }
        $out .= '</p>';
      }
    $out .= '</div>';

    if (!$init && $page->tmpMonstersActivity->count() > 0) {
      $out .= '<table class="table table-condensed">';
        $out .= '<tr>';
        $out .= '<th>Monster Activity</th>';
        $out .= '</tr>';
        $out .= '<tr>';
        foreach($page->tmpMonstersActivity->eq(0)->fields as $f) {
          $out .= '<td>';
          if ($f->name == 'date') {
            $out .= $f->label;
          } else {
            $out .= $f->name;
          }
          $out .= '</td>';
        }
        $out .= '</tr>';
        $globalUt = 0;
        foreach($tmpPages as $p) {
          $out .= '<tr>';
            foreach ($p->fields as $f) {
              if ($f->name == 'monster') {
                $out .= '<td>'.$p->$f->title.'</td>';
                if ($recalculate) {
                  $m = $p->$f;
                  $m = recalculateMonster($player, $m); // Recalculate monster's state from history
                  $globalUt += $m->inUt+$m->outUt;
                }
              } else {
                $out .= '<td>';
                if (stripos($f->name, 'date') !== false && $p->$f != '') { 
                  if ($p->$f != '') {
                    $out .= date("d/m/Y", $p->$f);
                  }
                } else {
                  $out .= $p->$f;
                }
                if ($recalculate) {
                  $out .= '<br />';
                  if ($m->$f == $p->$f) {
                    $out .= '<span class="label label-success">OK</span>';
                  } else {
                    $out .= '<span class="label label-danger">⇒ ';
                    if (stripos($f->name, 'date') !== false && $m->$f != '') { 
                      $out .= date("d/m/Y", $m->$f);
                    } else {
                      $out .= $m->$f;
                    }
                    $p->$f = $m->$f; // Set new value
                    $out .= '</span>';
                  }
                }
                $out .= '</td>';
              }
            }
            if ($confirm) { // Save new monster cache
              if ($m->trainNb == 0 && $m->fightNb == 0) {
                $page->tmpMonstersActivity->remove($p);
              }
              $p->of(false);
              $p->save();
            }
          $out .= '</tr>';
        }
        $out .= '<tr>';
          $out .= '<td colspan="3">';
            $out .= __("Global UT for player :");
            $out .= ' '.$player->underground_training;
            if ($recalculate) {
              if ($player->underground_training != $globalUt) {
                $out .= ' <span class="label label-danger">⇒ ';
                  $out .= $globalUt;
                $out .= '</span>';
              } else {
                $out .= ' <span class="label label-success">OK</span>';
              }
            }
            $out .= '</td>';
        $out .= '</tr>';
      $out .= '</table>';
    }

    if (!$init && $confirm == 1) { // Save new cache
      $page->of(false);
      $page->save();
      if ($player->underground_training != $globalUt) {
        $player->setAndSave('underground_training', $globalUt);
      }
    }
  } else {
    $out = 'This page is for admin only.';
  }

  echo $out;

  include("./foot.inc"); 
?>
