<?php namespace ProcessWire; ?>
<?php 
  if (!$config->ajax) {
          echo '</div>'; // #content
        echo '</div>'; // .row
      echo '<div class="push"><!--//--!></div>';
      echo '</div>'; // .container
    echo '</div>'; // wrap

    if ($user->isSuperuser() || $user->hasRole('teacher') || $user->name == 'test') {
      echo '<section class="navigate">';
        echo '<p id="foldAll">▄</p>';
        echo '<a href="#" id="scrollUp"><span class="glyphicon glyphicon-triangle-top"></span></a>';
        echo '<a href="#" id="scrollDown"><span class="glyphicon glyphicon-triangle-bottom"></span></a>';
      echo '</section>';
    }

    echo '<footer class="text-center">';
      echo '<p>';
      echo 'Planet Alert v24.04';
      echo ' &copy; '.date("Y").' FL';
      echo ' - ';
      echo 'Powered by <a href="http://processwire.com">ProcessWire</a> CMS (v'.$config->version.')';
      echo '&nbsp;';
      $about = $pages->get("name=about");
      echo '<a href="'.$about->url.'">['.$about->title.']</a>';
      echo ' <a href="https://framagit.org/celfred/planetAlert">'.__("[Framagit repository]").'</a>';
      echo '</p>';
    echo '</footer>';

    if ($user->isSuperuser()) {
      echo '<span class="backendLink">';
      echo '<a href="'.$config->urls->admin.'">[Backend]</a>';
      echo '</span>';
    } else if ($user->hasRole('teacher')) {
      echo '<span class="backendLink">'.__("Teacher").'</span>';
    }
  }

  $jsVersion = md5(filemtime($config->paths->templates.'foot.inc'));
?>
  <script src="<?php echo $config->urls->templates?>node_modules/jquery/dist/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo $config->urls->templates; ?>node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="<?php echo $config->urls->templates?>node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
  <script type='text/javascript' src='<?php echo $config->urls->templates; ?>node_modules/sweetalert2/dist/sweetalert2.min.js'></script>
  <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>node_modules/sweetalert2/dist/sweetalert2.min.css">
  <?php if ($page->is("template=hall-of-fame|visualizer")) { ?>
    <script src="<?php echo $config->urls->templates?>node_modules/masonry/index.js"></script>
  <?php } ?>
  <?php if ($page->is("template=quiz|main-office")) { ?>
    <script src="<?php echo $config->urls->templates?>node_modules/svg-pan-zoom/dist/svg-pan-zoom.min.js"></script>
    <script type='text/javascript' src='<?php echo $config->urls->templates?>node_modules/chance/dist/chance.min.js'></script>

  <?php } ?>
  <?php if ($page->is("template=exercise|speedQuiz|megamonster")) { ?>
    <script type='text/javascript' src='<?php echo $config->urls->templates?>node_modules/angular/angular.min.js'></script>
    <script type='text/javascript' src='<?php echo $config->urls->templates?>node_modules/angular-animate/angular-animate.min.js'></script>
    <script type='text/javascript' src='<?php echo $config->urls->templates?>node_modules/angular-sanitize/angular-sanitize.min.js'></script>
    <script type='text/javascript' src='<?php echo $config->urls->templates?>node_modules/chance/dist/chance.min.js'></script>
    <!-- Set the game logic !-->
    <script type='text/javascript' src='<?php echo $config->urls->templates?>scripts/exercise.js?v=<?php echo $jsVersion; ?>'></script>
  <?php }
    if ($user->language->name == 'french') {
      echo '<script src="'.$config->urls->templates.'scripts/dataTables.french.lang.js?v='.$jsVersion.'"></script>';
      echo '<script src="'.$config->urls->templates.'scripts/lang.fr.js?v='.$jsVersion.'"></script>';
    } else {
      echo '<script src="'.$config->urls->templates.'scripts/lang.en.js?v='.$jsVersion.'"></script>';
    }
    echo '<script src="'.$config->urls->templates.'scripts/main.js?v='.$jsVersion.'"></script>';

  if (!$config->ajax) {
    echo '</body>';
    echo '</html>';
  }
?>
